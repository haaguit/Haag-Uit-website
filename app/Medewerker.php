<?php

namespace HUplicatie;

use function collect;
use HUplicatie\Http\Requests\Stafplicatie\MedewerkerInschrijvingRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use function range;

class Medewerker extends Model
{
    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($medewerker) {
            collect(range(0, 4))->each(function ($value) use ($medewerker) {
                Aanwezigheid::create([
                    'medewerker_id' => $medewerker->id,
                    'meeting' => $value,
                ]);
            });
        });

        static::deleted(function ($medewerker) {
            $medewerker->eerste_keus()->detach();
            $medewerker->tweede_keus()->detach();
            $medewerker->aanwezigheid->each->delete();
            $medewerker->persoonsgegevens->delete();
        });
    }

    /**
     * Belongs to Persoon.
     *
     * @return BelongsTo
     */
    public function persoonsgegevens(): BelongsTo
    {
        return $this->belongsTo(Persoon::class, 'persoon_id');
    }

    /**
     * Belongs to Kampjaar.
     *
     * @return BelongsTo
     */
    public function kampjaar(): BelongsTo
    {
        return $this->belongsTo(Kampjaar::class, 'jaar', 'jaar');
    }

    public function eerste_keus(): BelongsToMany
    {
        return $this->belongsToMany(Functie::class, 'eerste_keus');
    }

    public function tweede_keus(): BelongsToMany
    {
        return $this->belongsToMany(Functie::class, 'tweede_keus');
    }

    public function aanwezigheid(): HasMany
    {
        return $this->hasMany(Aanwezigheid::class);
    }

    public static function createInschrijving(MedewerkerInschrijvingRequest $request): Medewerker
    {
        $persoon = Persoon::create($request->only([
            'voorletters',
            'voornaam',
            'achternaam',
            'roepnaam',
            'straat',
            'huisnummer',
            'postcode',
            'woonplaats',
            'mobiel',
            'geboortedatum',
            'email',
            'geslacht',
        ]));
        $persoon = $persoon->studie()->associate($request->get('studie_id'));
        $persoon->save();
        $medewerker = new self($request->only([
            'telefoon_nood',
            'studentnummer',
            'opmerking',
            'ervaring',
            'motivatie',
            'eigenschappen',
            'ehbo',
            'rijbewijs',
        ]));
        $medewerker->persoonsgegevens()->associate($persoon);
        $medewerker->kampjaar()->associate(Kampjaar::getActiveKampjaar());
        $medewerker->save();

        return $medewerker;
    }
}
