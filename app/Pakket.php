<?php

namespace HUplicatie;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Pakket extends Model
{
    protected $table = 'pakket';

    protected $fillable = ['pakket', 'inschrijving_id'];

    public $timestamps = false;

    /**
     * Belongs to inschrijving.
     *
     * @return BelongsTo
     */
    public function inschrijving(): BelongsTo
    {
        return $this->belongsTo(KampInschrijving::class);
    }
}
