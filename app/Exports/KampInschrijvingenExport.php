<?php

namespace HUplicatie\Exports;

use HUplicatie\KampInschrijving;
use HUplicatie\Kampjaar;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class KampInschrijvingenExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * Collection of all the inschrijvingen.
     *
     * @return Collection
     */
    public function collection(): Collection
    {
        return KampInschrijving::with(['persoon', 'latestBetaling'])
            ->where('jaar', Kampjaar::getActiveKampjaar()->jaar)
            ->where('presentie', '!=', 'afgemeld')
            ->get();
    }

    /**
     * Map Kamp Inschrijvingen to an array.
     *
     * @param  KampInschrijving  $inschrijving
     * @return array
     */
    public function map($inschrijving): array
    {
        $data = [
            $inschrijving->id,
            $inschrijving->persoon->voornaam,
            $inschrijving->persoon->voorletters,
            $inschrijving->persoon->roepnaam,
            $inschrijving->persoon->achternaam,
            $inschrijving->persoon->straat,
            $inschrijving->persoon->huisnummer,
            $inschrijving->persoon->postcode,
            $inschrijving->persoon->woonplaats,
            $inschrijving->persoon->geboortedatum,
            $inschrijving->persoon->studie->naam,
            $inschrijving->persoon->geslacht,
            $inschrijving->persoon->email,
            $inschrijving->persoon->mobiel,
            $inschrijving->kledingmaat,
            $inschrijving->telefoon_nood,
            $inschrijving->opmerking,
            $inschrijving->presentie,
            $inschrijving->annuleringsverzekering ? 'Ja' : 'Nee',
        ];

        if ($inschrijving->latestBetaling) {
            $data = array_merge($data, [
                $inschrijving->latestBetaling->type,
                $inschrijving->latestBetaling->transactie_id,
                $inschrijving->latestBetaling->bedrag,
                $inschrijving->latestBetaling->status,
                $inschrijving->latestBetaling->rekeninghouder_naam,
                $inschrijving->latestBetaling->rekeninghouder_nummer,
                $inschrijving->latestBetaling->rekeninghouder_woonplaats,
            ]);
        }

        return $data;
    }

    /**
     * Define all the column headings.
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            'ID',
            'Voornaam',
            'Voorletters',
            'Roepnaam',
            'Achternaam',
            'Straat',
            'Huisnummer',
            'Postcode',
            'Woonplaats',
            'Geboortedatum',
            'Opleiding',
            'Geslacht',
            'Email',
            'Mobiel',
            'Kledingmaat',
            'Telefoon Nood',
            'Opmerking',
            'Presentie',
            'Annuleringsverzekering',
            'Betalingsvorm',
            'Transactie ID',
            'Bedrag',
            'Status',
            'Rekeninghouder Naam',
            'Rekeninghouder Nummer',
            'Rekeninghouder Woonplaats',
        ];
    }
}
