<?php

namespace HUplicatie\Console\Commands;

use HUplicatie\Events\InternetConnection\Heartbeat;
use Illuminate\Console\Command;
use Log;

class SendHeartBeat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dashboard:send-heartbeat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a heartbeat to the internet connection tile';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        event(new Heartbeat());
        Log::info('Send heartbeat');
    }
}
