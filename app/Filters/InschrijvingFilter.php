<?php

namespace HUplicatie\Filters;

class InschrijvingFilter extends Filters
{
    protected $filters = ['paymentStatus', 'paymentType', 'presentie'];

    /**
     * Filter the query to payment status.
     *
     * @param  string  $status
     * @return mixed
     */
    protected function paymentStatus(string $status)
    {
        return $this->builder->whereHas('betalingen', function ($query) use ($status) {
            $query->where('status', $status);
        });
    }

    /**
     * Filter the query to payment type.
     *
     * @param  string  $type
     * @return mixed
     */
    protected function paymentType(string $type)
    {
        return $this->builder->whereHas('betalingen', function ($query) use ($type) {
            $query->where('type', $type);
        });
    }

    protected function contains($criteria)
    {
        return $this->builder->whereHas('persoon', function ($query) use ($criteria) {
            $query->where('roepnaam', 'LIKE', '%'.$criteria.'%')
                ->orWhere('voornaam', 'LIKE', '%'.$criteria.'%')
                ->orWhere('achternaam', 'LIKE', '%'.$criteria.'%')
                ->orWhere('email', 'LIKE', '%'.$criteria.'%');
        });
    }

    public function presentie($presentie)
    {
        return $this->builder->where('presentie', $presentie);
    }
}
