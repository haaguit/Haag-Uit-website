<?php

namespace HUplicatie;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Kampjaar extends Model
{
    protected $table = 'kampjaren';

    protected $primaryKey = 'jaar';
    protected $keyType = 'string';
    public $incrementing = false;

    protected $guarded = ['actief', 'open'];
    protected $casts = ['actief' => 'boolean', 'open' => 'boolean'];

    public static function getOpenKampjaar()
    {
        return self::where('actief', 1)->where('open', 1)->first();
    }

    public static function getActiveKampjaar()
    {
        return self::where('actief', 1)->first();
    }

    public function scopeActive($query)
    {
        return $query->where('actief', 1);
    }

    /**
     * Returns the open kampjaar.
     *
     * @return int
     */
    public static function getOpenKampjaarYear(): int
    {
        return self::getOpenKampjaar()->jaar;
    }

    /**
     * Get the amount to pay with the insurance.
     *
     * @return string
     */
    public function getAmountWithInsurance(): string
    {
        return number_format((float) $this->inschrijfbedrag + (float) $this->verzekeringbedrag, 2, '.', '');
    }

    /**
     * Get the amount to pay without the insurance.
     *
     * @return string
     */
    public function getAmountWithoutInsurance(): string
    {
        return number_format($this->inschrijfbedrag, 2, '.', '');
    }

    public function getLogoAttribute($logo): string
    {
        return asset($logo ? '/storage/'.$logo : '/assets/website/img/hu_logo_default.png');
    }

    public function activate(): void
    {
        if ($active = self::getActiveKampjaar()) {
            $active->actief = false;
            $active->open = false;
            $active->save();
        }
        $this->actief = true;
        $this->save();
    }

    public function open(): void
    {
        if ($this->actief) {
            $this->open = true;
            $this->save();
        }
    }

    public function close(): void
    {
        $this->open = false;
        $this->save();
    }

    /**
     * Has many kamp inschrijvingen.
     *
     * @return HasMany
     */
    public function kampInschrijvingen(): HasMany
    {
        return $this->hasMany(KampInschrijving::class, 'jaar');
    }

    /**
     * Has many actiepunten.
     *
     * @return HasMany
     */
    public function actiepunten(): HasMany
    {
        return $this->hasMany(Actiepunt::class, 'jaar');
    }

    /**
     * Has many medewerkers.
     *
     * @return HasMany
     */
    public function medewerkers(): HasMany
    {
        return $this->hasMany(Medewerker::class, 'jaar');
    }
}
