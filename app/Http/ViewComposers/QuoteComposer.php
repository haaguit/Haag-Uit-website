<?php

namespace HUplicatie\Http\ViewComposers;

use HUplicatie\Quote;
use Illuminate\View\View;
use Log;

class QuoteComposer
{
    public function compose(View $view): void
    {
        $quote = Quote::inRandomOrder()->first();
        if (! $quote) {
            Log::debug('Could not find a quote in the database. Returning null');
        }
        $view->with('quote', $quote);
    }
}
