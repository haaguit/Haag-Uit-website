<?php

namespace HUplicatie\Http\ViewComposers;

use Carbon\Carbon;
use HUplicatie\Kampjaar;
use Illuminate\View\View;
use Log;

class KampjaarComposer
{
    public function compose(View $view): void
    {
        $kampjaar = Kampjaar::active()->first();
        if (! $kampjaar) {
            Log::warning('No active kampjaar! Using current year.');
            $kampjaar = new Kampjaar();
            $kampjaar->jaar = Carbon::now()->year;
        }
        $view->with('kampjaar', $kampjaar);
    }
}
