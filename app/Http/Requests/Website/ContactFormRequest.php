<?php

namespace HUplicatie\Http\Requests\Website;

use Illuminate\Foundation\Http\FormRequest;
use TimeHunter\LaravelGoogleReCaptchaV3\Validations\GoogleReCaptchaV3ValidationRule;

class ContactFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'naam'                 => 'required|max:20',
            'email'                => 'required|email',
            'tekst'                => 'required|max:600',
            'g-recaptcha-response' => [new GoogleReCaptchaV3ValidationRule('contact')],
        ];
    }

    public function messages()
    {
        return [
            'naam.required'  => 'Een naam is verplicht',
            'naam.max'       => 'Je naam mag niet langer zijn dan 20 karakters',
            'email.required' => 'Zonder email kunnen we niet antwoorden!',
            'email.email'    => 'Dit is geen geldig email adres.',
            'tekst.required' => 'Je moet ons wel wat te vertellen hebben!',
            'tekst.max'      => 'Het bericht mag niet meer dan 600 karakters bevatten.',
        ];
    }
}
