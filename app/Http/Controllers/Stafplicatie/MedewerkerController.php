<?php

namespace HUplicatie\Http\Controllers\Stafplicatie;

use function compact;
use HUplicatie\Functie;
use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Http\Requests\Stafplicatie\MedewerkerInschrijvingRequest;
use HUplicatie\Kampjaar;
use HUplicatie\Medewerker;
use HUplicatie\Studie;
use HUplicatie\User;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use function redirect;

class MedewerkerController extends Controller
{
    private $kampjaar;

    public function __construct()
    {
        $this->middleware('permission:View Medewerkers')->only('index');
        $this->middleware('permission:Create Medewerker')->only(['create', 'store']);
        $this->middleware('permission:Edit Medewerker')->only(['edit', 'update']);
        $this->middleware('permission:Delete Medewerker')->only('destroy');
        $this->kampjaar = Kampjaar::getActiveKampjaar();
    }

    public function index()
    {
        return view('stafplicatie.medewerker.index');
    }

    public function create()
    {
        return view('stafplicatie.medewerker.create', [
            'jaar'    => $this->kampjaar->jaar,
            'studies' => Studie::orderBy('naam')->get(),
        ]);
    }

    public function store(MedewerkerInschrijvingRequest $request)
    {
        $medewerker = Medewerker::createInschrijving($request);
        $medewerker->eerste_keus()->attach(Functie::select('id')->whereIn('naam', $request->eerste_keus)->get());
        $medewerker->tweede_keus()->attach(Functie::select('id')->whereIn('naam', $request->tweede_keus)->get());

        return redirect('/stafplicatie/medewerker')->with('flash', 'Medewerker aangemaakt');
    }

    public function edit(Medewerker $medewerker)
    {
        $studies = Studie::orderBy('naam')->get();

        return view('stafplicatie.medewerker.edit')
            ->with('medewerker', $medewerker)
            ->with('studies', $studies);
    }

    public function update(Medewerker $medewerker, MedewerkerInschrijvingRequest $request)
    {
        $medewerker->persoonsgegevens()->update($request->only([
            'voorletters',
            'voornaam',
            'achternaam',
            'roepnaam',
            'straat',
            'huisnummer',
            'postcode',
            'woonplaats',
            'mobiel',
            'geboortedatum',
            'email',
            'geslacht',
        ]));
        $medewerker->persoonsgegevens->studie()->associate($request->studie_id);
        $medewerker->update($request->only([
            'telefoon_nood',
            'studentnummer',
            'opmerking',
            'ervaring',
            'motivatie',
            'eigenschappen',
            'ehbo',
            'rijbewijs',
        ]));
        $medewerker->eerste_keus()->sync(Functie::select('id')->whereIn('naam', $request->eerste_keus)->get());
        $medewerker->tweede_keus()->sync(Functie::select('id')->whereIn('naam', $request->tweede_keus)->get());

        return redirect("/stafplicatie/medewerker/{$medewerker->id}/edit")->with('flash', 'Medewerker aangepast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Medewerker  $medewerker
     * @return ResponseFactory|RedirectResponse|Response|Redirector
     *
     * @throws \Exception
     */
    public function destroy(Medewerker $medewerker)
    {
        $medewerker->delete();
        if (\request()->wantsJson()) {
            return response([], 204);
        }

        return redirect('/stafplicatie/medewerker');
    }
}
