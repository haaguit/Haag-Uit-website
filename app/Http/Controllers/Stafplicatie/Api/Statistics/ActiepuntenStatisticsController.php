<?php

namespace HUplicatie\Http\Controllers\Stafplicatie\Api\Statistics;

use HUplicatie\Actiepunt;
use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Kampjaar;

class ActiepuntenStatisticsController extends Controller
{
    public function actiepuntenStatistics(): array
    {
        $result = [];
        $actiepunten = Actiepunt::withTrashed()->activeYear()->get();
        $result['total'] = $actiepunten->count();
        $result['done'] = $actiepunten->filter(function ($ap) {
            return $ap->trashed();
        })->count();

        $result['users'] = $actiepunten->filter(function ($ap) {
            return ! $ap->trashed() && $ap->users->contains(auth()->id());
        })->pluck('beschrijving');

        return $result;
    }
}
