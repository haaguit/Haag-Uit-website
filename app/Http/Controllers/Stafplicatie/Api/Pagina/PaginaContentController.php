<?php

namespace HUplicatie\Http\Controllers\Stafplicatie\Api\Pagina;

use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Pagina;
use Illuminate\Http\Request;

class PaginaContentController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:Edit Pagina')->only('index', 'update');
    }

    public function index(Pagina $pagina): Pagina
    {
        return $pagina;
    }

    public function update(Pagina $pagina, Request $request)
    {
        $pagina->update($request->all());

        return response(null, 201);
    }
}
