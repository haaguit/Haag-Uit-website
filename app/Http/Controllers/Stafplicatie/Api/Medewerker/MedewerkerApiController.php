<?php

namespace HUplicatie\Http\Controllers\Stafplicatie\Api\Medewerker;

use HUplicatie\Filters\InschrijvingFilter;
use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Kampjaar;

class MedewerkerApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:View Medewerkers')->only('index');
    }

    public function index()
    {
        return Kampjaar::getActiveKampjaar()->medewerkers()
            ->with('persoonsgegevens', 'eerste_keus', 'tweede_keus')->get();
    }
}
