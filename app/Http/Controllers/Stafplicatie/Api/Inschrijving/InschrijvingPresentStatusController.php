<?php

namespace HUplicatie\Http\Controllers\Stafplicatie\Api\Inschrijving;

use HUplicatie\Http\Controllers\Controller;
use HUplicatie\KampInschrijving;

class InschrijvingPresentStatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:Edit Inschrijving')->only(['open', 'unregister']);
    }

    public function open(KampInschrijving $inschrijving)
    {
        $inschrijving->update(['presentie' => 'open']);

        return response('', 200);
    }

    public function unregister(KampInschrijving $inschrijving)
    {
        $inschrijving->update(['presentie' => 'afgemeld']);

        return response('', 200);
    }
}
