<?php

namespace HUplicatie\Http\Controllers\Stafplicatie\Api\Inschrijving;

use Carbon\Carbon;
use HUplicatie\Exports\KampInschrijvingenExport;
use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Kampjaar;
use Maatwebsite\Excel\Excel;

class InschrijvingExportController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:View Inschrijvingen');
    }

    public function export(Excel $excel): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        $kampjaarJaar = Kampjaar::getActiveKampjaar()->jaar;
        $date = Carbon::now()->format('Y-m-d');

        return $excel->download(new KampInschrijvingenExport(), "Inschrijvingen-Haag-Uit-{$kampjaarJaar}-{$date}.xlsx");
    }
}
