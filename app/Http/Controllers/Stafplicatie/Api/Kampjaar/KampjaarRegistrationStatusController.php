<?php

namespace HUplicatie\Http\Controllers\Stafplicatie\Api\Kampjaar;

use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Kampjaar;

class KampjaarRegistrationStatusController extends Controller
{
    /**
     * KampjaarRegistrationStatusController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:Edit Kampjaar')->only(['store', 'destroy']);
    }

    public function store(Kampjaar $kampjaar)
    {
        if (! $kampjaar->actief) {
            return response('Kampjaar is niet actief', 409);
        }
        $kampjaar->open();
    }

    public function destroy(Kampjaar $kampjaar): void
    {
        $kampjaar->close();
    }
}
