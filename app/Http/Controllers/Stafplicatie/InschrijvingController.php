<?php

namespace HUplicatie\Http\Controllers\Stafplicatie;

use HUplicatie\Http\Controllers\Controller;
use HUplicatie\KampInschrijving;
use HUplicatie\Kampjaar;
use Illuminate\Http\Request;

class InschrijvingController extends Controller
{
    /**
     * InschrijvingController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:View Inschrijvingen')->only('index');
        $this->middleware('permission:View Inschrijving')->only('show');
        $this->middleware('permission:Edit Inschrijving')->only(['edit', 'update']);
    }

    public function index()
    {
        $kampjaar = Kampjaar::active()->first();

        return view('stafplicatie.inschrijving.index', compact('kampjaar'));
    }

    public function show(KampInschrijving $inschrijving)
    {
        $inschrijving->load('persoon', 'latestBetaling', 'kampjaar');

        return view('stafplicatie.inschrijving.show', compact('inschrijving'));
    }

    public function update(KampInschrijving $inschrijving, Request $request): KampInschrijving
    {
        $inschrijving->update($request->all());

        return $inschrijving;
    }
}
