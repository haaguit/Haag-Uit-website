<?php

namespace HUplicatie\Http\Controllers\Website;

use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Nieuwsbericht;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class NieuwsberichtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $newsMessages = Nieuwsbericht::latest()->paginate(5);

        return view('website.nieuws.index', compact('newsMessages'));
    }

    /**
     * Shows a nieuwsbericht.
     *
     * @param  Nieuwsbericht  $nieuwsbericht
     * @return Factory|View
     */
    public function show(Nieuwsbericht $nieuwsbericht)
    {
        return view('website.nieuws.show')
            ->with('nieuwsbericht', $nieuwsbericht);
    }
}
