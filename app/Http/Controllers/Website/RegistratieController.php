<?php

namespace HUplicatie\Http\Controllers\Website;

use HUplicatie\Events\RegistrationSuccessEvent;
use HUplicatie\Exceptions\IdealBetalingCreationException;
use HUplicatie\Exceptions\InschrijvingCreationException;
use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Http\Requests\Website\RegistratieRequest;
use HUplicatie\Services\Betalingen\IdealBetalingService;
use HUplicatie\Services\Betalingen\IncassoBetalingService;
use HUplicatie\Services\InschrijvingService;
use Illuminate\Support\Facades\Log;

class RegistratieController extends Controller
{
    /**
     * Inschrijving Service.
     *
     * @var InschrijvingService
     */
    private $inschrijvingService;

    /**
     * RegistratieController constructor.
     *
     * @param  InschrijvingService  $inschrijvingService
     */
    public function __construct(InschrijvingService $inschrijvingService)
    {
        $this->middleware('registrationOpen')->except('closed');
        $this->inschrijvingService = $inschrijvingService;
    }

    public function index()
    {
        return view('website.inschrijven.index');
    }

    public function store(RegistratieRequest $request)
    {
        $inschrijving = null;

        try {
            $inschrijving = $this->inschrijvingService->processInschrijving($request);
        } catch (InschrijvingCreationException $exception) {
            Log::error('Het is niet gelukt de inschrijving aan te maken.', ['exception' => $exception]);

            return view('website.errors.500');
        }

        try {
            $amount = $inschrijving->kampjaar->getAmountWithoutInsurance();

            if ($request->input('annuleringsverzekering')) {
                $amount = $inschrijving->kampjaar->getAmountWithInsurance();
            }

            $betalingService = null;
            if ($request->input('type') === 'ideal') {
                $betalingService = new IdealBetalingService($inschrijving);
            } else {
                $betalingService = new IncassoBetalingService(
                    $inschrijving,
                    $request->only(['rekeninghouder_naam', 'rekeninghouder_nummer', 'rekeninghouder_woonplaats'])
                );
            }
            $betaling = $betalingService->processBetaling($amount);
            event(new RegistrationSuccessEvent($inschrijving, $betaling->paymentUrl));

            if ($request->input('type') === 'ideal') {
                return redirect($betaling->paymentUrl);
            }

            return view(
                'website.inschrijven.success',
                ['paymentUrl' => $betaling->paymentUrl, 'email' => $inschrijving->persoon->email]
            );
        } catch (IdealBetalingCreationException $exception) {
            Log::error("Error creating payment for KampInschrijving {$inschrijving->id}", ['exception' => $exception]);

            return view('website.errors.500');
        }
    }

    public function closed()
    {
        return view('website.inschrijven.closed');
    }
}
