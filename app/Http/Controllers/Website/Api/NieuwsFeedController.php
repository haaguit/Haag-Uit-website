<?php

namespace HUplicatie\Http\Controllers\Website\Api;

use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Nieuwsbericht;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class NieuwsFeedController extends Controller
{
    /**
     * Returns a feed of nieuwsberichten limited by an given amount.
     *
     * @param  Request  $request
     * @return Collection
     */
    public function index(Request $request): Collection
    {
        $count = $request->input('count', '5');

        return Nieuwsbericht::latest()->take($count)->get();
    }
}
