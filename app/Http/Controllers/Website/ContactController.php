<?php

namespace HUplicatie\Http\Controllers\Website;

use HUplicatie\Http\Controllers\Controller;
use HUplicatie\Http\Requests\Website\ContactFormRequest;
use HUplicatie\Mail\Website\ContactRequestMailable;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class ContactController extends Controller
{
    /**
     * Mailer.
     *
     * @var Mailer
     */
    private $mailer;

    /**
     * ContactController constructor.
     *
     * @param  Mailer  $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function index()
    {
        return view('website.contact.index');
    }

    /**
     * Store contact request.
     *
     * @param  ContactFormRequest  $request
     * @return Factory|View
     */
    public function store(ContactFormRequest $request)
    {
        $name = $request->get('naam');
        $email = $request->get('email');
        $text = $request->get('tekst');
        $this->mailer->queue(new ContactRequestMailable($name, $email, $text));

        return view('website.contact.done', compact('name'));
    }
}
