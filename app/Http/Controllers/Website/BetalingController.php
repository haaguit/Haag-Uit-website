<?php

namespace HUplicatie\Http\Controllers\Website;

use HUplicatie\Betaling;
use HUplicatie\Exceptions\IdealBetalingCreationException;
use HUplicatie\Http\Controllers\Controller;
use HUplicatie\KampInschrijving;
use HUplicatie\Services\Betalingen\IdealBetalingService;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Log;
use Mollie\Api\Exceptions\ApiException;
use Mollie\Laravel\Facades\Mollie;

class BetalingController extends Controller
{
    /**
     * Retry function for ideal payments.
     *
     * @param  Request  $request
     * @return Factory|RedirectResponse|Redirector|View
     *
     * @throws ApiException
     */
    public function retry(Request $request)
    {
        if (! $request->has('id')) {
            return redirect('/');
        }

        $inschrijving = KampInschrijving::findOrFail($request->get('id'));
        $betaling = $inschrijving->betalingen()->latest()->first();

        if (! $betaling) {
            return view('website.errors.500', [
                'message' => 'Er is iets mis gegaan met het aanmaken van je betaling. Neem contact op met de staf!',
            ]);
        }

        if ($betaling->isIncasso()) {
            return redirect('/');
        }

        if ($betaling->isOpen()) {
            $payment = Mollie::api()->payments()->get($betaling->transactie_id);

            return redirect($payment->getCheckoutUrl(), 303);
        }

        if ($betaling->isPaid()) {
            return view('website.inschrijven.betaald', [
                'email'           => $inschrijving->persoon->email,
                'inschrijving_id' => $inschrijving->id,
                'betaling'        => $betaling,
            ]);
        }

        try {
            $newBetaling = (new IdealBetalingService($inschrijving))->processBetaling($betaling->bedrag);

            return redirect($newBetaling->paymentUrl, 303);
        } catch (IdealBetalingCreationException $exception) {
            Log::error(
                "Error creating retry payment for KampInschrijving {$inschrijving->id}",
                ['exception' => $exception]
            );

            return view('website.errors.500');
        }
    }

    /**
     * Return url after payment in Mollie Terminal.
     *
     * @param  Request  $request
     * @return Factory|RedirectResponse|Redirector|View
     *
     * @throws ApiException
     */
    public function returnUrl(Request $request)
    {
        if (! $request->has('id')) {
            return redirect('/');
        }

        $inschrijving = KampInschrijving::findOrFail($request->get('id'));
        $betaling = $inschrijving->betalingen()->latest()->first();
        if ($betaling->type === 'incasso') {
            return redirect('/');
        }

        $data = [
            'email'           => $inschrijving->persoon->email,
            'inschrijving_id' => $inschrijving->id,
            'betaling'        => $betaling,
        ];

        if ($betaling->isOpen()) {
            $data['paymentUrl'] = Mollie::api()->payments()->get($betaling->transactie_id)->getCheckoutUrl();
        } else {
            $data['paymentUrl'] = '';
        }

        return view('website.inschrijven.betaald', $data);
    }

    /**
     * Webhook function to receive status updates from Mollie.
     *
     * @param  Request  $request
     * @return ResponseFactory|Response
     */
    public function webhook(Request $request)
    {
        if (! $request->has('id')) {
            return response('', 404);
        }

        try {
            $payment = Mollie::api()->payments()->get($request->id);
            $betaling = Betaling::where('transactie_id', $payment->id)->first();
            Log::debug('Betaling info: ', ['id' => $betaling->id, 'details' => $payment->details]);
            if ($payment->isPaid()) {
                $betaling->update([
                    'status'                => $payment->status,
                    'rekeninghouder_naam'   => $payment->details->consumerName,
                    'rekeninghouder_nummer' => $payment->details->consumerAccount,
                ]);
            } else {
                Log::info("Betaling {$payment->id} is niet betaald: ", ['status' => $payment->status]);
                $betaling->update(['status' => $payment->status]);
            }

            return response('', 200);
        } catch (ApiException $exception) {
            Log::error('Some error occurred on the webhook', ['exception' => $exception]);

            return response('', 400);
        }
    }
}
