<?php

namespace HUplicatie;

use HUplicatie\Transformer\ActiveKampjaarVariable;
use HUplicatie\Transformer\VariableTransformer;
use Illuminate\Database\Eloquent\Model;

class Pagina extends Model
{
    protected $guarded = ['id'];

    public function withVariableReplacements(): self
    {
        if ($this->attributes['tekst']) {
            $this->attributes['tekst'] = (new VariableTransformer($this->attributes['tekst']))
                ->addSubstitute(new ActiveKampjaarVariable())
                ->replace();
        }

        return $this;
    }
}
