<?php

namespace HUplicatie;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Functie extends Model
{
    protected $fillable = ['naam'];

    /**
     * Belongs to many medewerkers.
     *
     * @return BelongsToMany
     */
    public function medewerker(): BelongsToMany
    {
        return $this->belongsToMany(Medewerker::class);
    }
}
