<?php

namespace HUplicatie\Mail\Website;

use HUplicatie\KampInschrijving;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InschrijvingPersoonMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Kamp Inschrijving.
     *
     * @var KampInschrijving
     */
    public $inschrijving;

    /**
     * Payment URL.
     *
     * @var string|null
     */
    public $paymentUrl;

    /**
     * Create a new message instance.
     *
     * @param  KampInschrijving  $inschrijving
     * @param  string|null  $paymentUrl
     */
    public function __construct(KampInschrijving $inschrijving, string $paymentUrl = null)
    {
        $this->inschrijving = $inschrijving;
        $this->paymentUrl = $paymentUrl;

        $this->to(
            $inschrijving->persoon->email,
            $inschrijving->persoon->voornaam.' '.$inschrijving->persoon->achternaam
        )
            ->subject("Inschrijving Haag Uit {$inschrijving->kampjaar->jaar}")
            ->from('staf@haaguit.com', 'Staf Haag Uit');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->markdown('email.inschrijving.persoon');
    }
}
