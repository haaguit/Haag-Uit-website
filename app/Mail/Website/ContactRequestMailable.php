<?php

namespace HUplicatie\Mail\Website;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactRequestMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Name.
     *
     * @var string
     */
    public $name;

    /**
     * Email.
     *
     * @var string
     */
    public $email;

    /**
     * Text.
     *
     * @var string
     */
    public $text;

    /**
     * Create a new message instance.
     *
     * @param  string  $name
     * @param  string  $email
     * @param  string  $text
     */
    public function __construct(string $name, string $email, string $text)
    {
        $this->name = $name;
        $this->email = $email;
        $this->text = $text;

        $this->to('staf@haaguit.com', 'Staf Haag Uit')
            ->from($this->email, $this->name)
            ->subject("Contact via website door {$this->name}");
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->markdown('email.contact');
    }
}
