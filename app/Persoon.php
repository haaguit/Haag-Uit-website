<?php

namespace HUplicatie;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;

/**
 * Entity voor Persoon.
 *
 * @property string $voornaam
 * @property string $voorletters
 * @property string $roepnaam
 * @property string $achternaam
 * @property string $straat
 * @property string $huisnummer
 * @property string $postcode
 * @property string $woonplaats
 * @property Carbon $geboortedatum
 * @property Studie $studie
 * @property string $geslacht
 * @property string $email
 * @property string $mobiel
 */
class Persoon extends Model
{
    protected $table = 'personen';

    protected $guarded = ['id'];

    protected $dates = [
        'created_at',
        'updated_at',
        'geboortedatum',
    ];

    /**
     * Has one Kamp Inschrijving.
     *
     * @return HasOne
     */
    public function kampInschrijving(): HasOne
    {
        return $this->hasOne(KampInschrijving::class);
    }

    public function setGeboortedatumAttribute($value): void
    {
        $this->attributes['geboortedatum'] = Carbon::parse($value);
    }

    /**
     * Has many medewerker inschrijvingen.
     *
     * @return HasMany
     */
    public function medewerkerInschrijvingen(): HasMany
    {
        return $this->hasMany(Medewerker::class);
    }

    /**
     * Belongs to a Studie.
     *
     * @return BelongsTo
     */
    public function studie(): BelongsTo
    {
        return $this->belongsTo(Studie::class);
    }

    public function registrations(): Collection
    {
        $inschrijvingen = collect();
        $inschrijvingen = $inschrijvingen->union(
            $this->kampInschrijving()->select('jaar')->get()->mapWithKeys(function ($item) {
                return [$item->jaar => 'Feut'];
            })
        );
        $inschrijvingen = $inschrijvingen->union(
            $this->medewerkerInschrijvingen()->select('jaar')->get()
                ->mapWithKeys(function ($item) {
                    return [$item->jaar => 'Medewerker Inschrijving'];
                })->all()
        );

        return $inschrijvingen;
    }
}
