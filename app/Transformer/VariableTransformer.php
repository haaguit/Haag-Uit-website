<?php

namespace HUplicatie\Transformer;

use Illuminate\Support\Collection;
use InvalidArgumentException;

class VariableTransformer
{
    private $substitutes;
    private $text;

    /**
     * VariableTransformer constructor.
     *
     * @param  string  $text
     */
    public function __construct(string $text)
    {
        $this->substitutes = new Collection();
        $this->text = $text;
    }

    /**
     * Adds a substitute to the list of substitutes.
     *
     * @param  SubstituteVariable  $variable
     * @return $this
     */
    public function addSubstitute(SubstituteVariable $variable): self
    {
        $this->substitutes->push($variable);

        return $this;
    }

    public function replace(): string
    {
        if ($this->substitutes->isEmpty()) {
            throw new InvalidArgumentException('No substitute variables defined.');
        }
        $this->substitutes->each(function (SubstituteVariable $variable) {
            $this->text = str_replace($variable->getVariable(), $variable->getReplacement(), $this->text);
        });

        return $this->text;
    }
}
