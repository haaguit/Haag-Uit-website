<?php

namespace HUplicatie;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Studie extends Model
{
    protected $fillable = ['naam'];

    /**
     * Has many personen.
     *
     * @return HasMany
     */
    public function personen(): HasMany
    {
        return $this->hasMany(Persoon::class);
    }
}
