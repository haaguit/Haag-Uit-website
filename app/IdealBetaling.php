<?php

namespace HUplicatie;

use Mollie\Api\Resources\Payment;

class IdealBetaling extends Betaling
{
    /**
     * IncassoBetaling constructor.
     *
     * @param  Payment  $payment
     */
    public function __construct(Payment $payment)
    {
        parent::__construct();
        $this->attributes['type'] = 'ideal';
        $this->transactie_id = $payment->id;
        $this->bedrag = $payment->amount->value;
    }
}
