# Haag Uit Website

Hallo welkom bij de source code van HUplicatie!
Dit is zowel de front end als de back end van de Haag Uit website.

## Installatie van project

Benodigdheden: 
- Ontwikkelomgeving (PhpStorm/VisualStudioCode/Atom/VIM)
- Platform om applicatie lokaal te draaien. Zie kopje Ontwikkelplatform.
- Source code op je PC door middel van een `git clone`
- Kennis van Laravel/Google. [Laravel](https://laravel.com/docs/6.x) heeft goede documentatie. Op dit moment draait het nog op Laravel 6, 
check of je de juiste documentatie bekijkt!

### Ontwikkelplatform
Het project is gebaseerd op Laravel. 
Dit draait niet zomaar op een vers geïnstalleerde XAMP/LAMP stack, het is echter wel mogelijk dit te gebruiken maar 
andere methodes worden geadviseerd.
Op dit moment is er geen duidelijke beschrijving voor het gebruiken van een XAMP/LAMP stack voor dit project. Mocht je 
deze route willen gaan, google is uw vriend.

De Laravel documentatie beveelt verschillende manieren aan voor verschillende platformen. Voor Windows is 
[Vagrant](https://laravel.com/docs/8.x/homestead) de makkelijkste optie. Voor Mac OSX is dat [Valet](https://laravel.com/docs/8.x/valet).
Voor Linux zijn er vele mogelijkheden, een van de manieren is [Valet for Linux](https://cpriego.github.io/valet-linux/).

### Draaien van het project

Navigeer met de command line naar de root directory van het project.
Installeer de dependencies door `composer install` en `npm install` uit te voeren. Check of het bestand `.env` bestaat.
Zo niet, kopieer dan `.env.example` en noem die `.env`. Voer vervolgens `php artisan key:generate` uit. Open het `.env`
bestand en pas de variabelen aan die aanngepast moeten worden. Zoals die van de Database beginnende met `DB_`.
Als laatste voer je `php artisan migrate` uit om de database structuur aan te maken. Voila!

## Code Kwaliteit

Er zijn meerde code kwaliteit tools voor dit project. Alles moet in orde zijn voordat de pipeline succesvol draait.
Draai de volgende commando's vanuit de project root om eventuele errors te vinden die je vervolgens kan fixen. 

### PHPCodeSniffer
Een PHP Style checker. Deze moet globaal geïnstalleerd staan. Doe dit met het volgende commando:

`composer global require "squizlabs/php_codesniffer=*"`

Linux/Mac:

`phpcs`

Windows:

`?`

Om eventuele errors automatisch te fixen, draai het volgende commando:

Linux/Mac: 

`phpcbf`

Windows:

`?`

Dit repareert zo veel als het kan aan fouten. 
Draai PHPCodeSniffer opnieuw om te verifiëren dat alle problemen zijn opgelost!

### LaraStan

Static code analysis tool.

Linux/Mac:

`vendor/bin/phpstan analyse`

Windows:

`?`