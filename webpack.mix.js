let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/website/js/website.js', 'public/assets/website/js').vue();

mix.sass('resources/website/sass/website.scss', 'public/assets/website/css');

mix.js('resources/website/js/lekkerbellen.js', 'public/assets/website/js');
mix.sass('resources/website/sass/lekkerbellen.scss', 'public/assets/website/css');

mix.copyDirectory('resources/website/sound', 'public/assets/website/sound')
    .copyDirectory('resources/website/img', 'public/assets/website/img')
    .copyDirectory('resources/stafplicatie/img', 'public/assets/stafplicatie/img');

mix.js('resources/stafplicatie/js/stafplicatie.js', 'public/assets/stafplicatie/js')
    .extract(['vue', 'lodash', 'popper.js', 'jquery', 'bootstrap', '@coreui/coreui', 'toastr', 'axios']);

mix.sass('resources/stafplicatie/sass/stafplicatie.scss', 'public/assets/stafplicatie/css');
mix.copy('node_modules/@coreui/icons/css/free.min.css', 'public/assets/stafplicatie/css');
mix.copy('node_modules/@coreui/icons/css/brand.min.css', 'public/assets/stafplicatie/css');
mix.copy('node_modules/@coreui/icons/css/flag.min.css', 'public/assets/stafplicatie/css');
mix.copy('node_modules/@coreui/icons/sprites', 'public/assets/stafplicatie/svg');

if (mix.inProduction()) {
    mix.version();
}

mix.browserSync(
    {
        proxy: 'https://stafplicatie.test'
    }
);
