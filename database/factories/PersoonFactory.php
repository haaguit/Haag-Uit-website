<?php

use Faker\Generator as Faker;
use HUplicatie\Studie;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(HUplicatie\Persoon::class, function (Faker $faker) {
    return [
        'voornaam'      => $faker->firstName,
        'roepnaam'      => $faker->firstName,
        'voorletters'   => $faker->randomLetter,
        'achternaam'    => $faker->lastName,
        'straat'        => $faker->streetName,
        'huisnummer'    => $faker->buildingNumber,
        'postcode'      => $faker->postcode,
        'woonplaats'    => $faker->city,
        'geboortedatum' => $faker->date('Y-m-d', '-16 years'),
        'studie_id'     => Studie::inRandomOrder()->first()->id,
        'geslacht'      => $faker->randomElement(['m', 'v', 'o']),
        'email'         => $faker->email,
        'mobiel'        => $faker->phoneNumber,
    ];
});
