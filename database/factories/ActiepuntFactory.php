<?php

use Faker\Generator as Faker;

$factory->define(HUplicatie\Actiepunt::class, function (Faker $faker) {
    return [
        'beschrijving' => $faker->paragraph(),
        'deadline'     => \Carbon\Carbon::now()->addWeek(2),
        'jaar'         => function () {
            return factory(\HUplicatie\Kampjaar::class)->create()->jaar;
        },
    ];
});
