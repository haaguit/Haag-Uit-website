<?php

use Faker\Generator as Faker;
use HUplicatie\Pagina;

$factory->define(Pagina::class, function (Faker $faker) {
    return [
        'naam'  => $faker->word,
        'titel' => $faker->sentence,
        'tekst' => $faker->paragraph,
    ];
});
