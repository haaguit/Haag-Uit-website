<?php

use Faker\Generator as Faker;

$factory->define(HUplicatie\Sponsor::class, function (Faker $faker) {
    return [
        'naam' => $faker->company,
        'url'  => $faker->url,
        'logo' => $faker->imageUrl(),
    ];
});
