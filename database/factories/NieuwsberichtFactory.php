<?php

use Faker\Generator as Faker;

$factory->define(HUplicatie\Nieuwsbericht::class, function (Faker $faker) {
    return [
        'titel' => $faker->sentence,
        'tekst' => $faker->paragraphs(10, true),
    ];
});
