<?php

use HUplicatie\Authorization\Roles;
use HUplicatie\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $staffer = factory(User::class)->create([
            'name'     => 'Staffer',
            'email'    => 'staffer@haaguit.com',
            'password' => Hash::make('test'),
        ]);
        $staffer->assignRole(Roles::STAFFER);
    }
}
