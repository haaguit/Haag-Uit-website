<?php

use HUplicatie\Authorization\Roles;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddPaginaManagementPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => 'View Paginas']);
        Permission::create(['name' => 'View Pagina']);
        Permission::create(['name' => 'Create Pagina']);
        Permission::create(['name' => 'Edit Pagina']);
        Permission::create(['name' => 'Delete Pagina']);

        Role::findByName(Roles::STAFFER)->givePermissionTo('View Paginas', 'View Pagina', 'Create Pagina', 'Edit Pagina', 'Delete Pagina');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereIn('name', ['View Paginas', 'View Pagina', 'Create Pagina', 'Edit Pagina', 'Delete Pagina'])->delete();
    }
}
