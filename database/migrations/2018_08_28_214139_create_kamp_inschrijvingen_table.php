<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKampInschrijvingenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kamp_inschrijvingen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kledingmaat');
            $table->string('telefoon_nood');
            $table->text('opmerking')->nullable();
            $table->enum('presentie', ['open', 'aanwezig', 'afgemeld', 'afwezig'])->default('open');
            $table->boolean('annuleringsverzekering');
            $table->year('jaar');
            $table->integer('persoon_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kamp_inschrijvingen');
    }
}
