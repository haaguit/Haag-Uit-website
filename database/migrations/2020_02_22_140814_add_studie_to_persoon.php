<?php

use HUplicatie\Persoon;
use HUplicatie\Studie;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStudieToPersoon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personen', function (Blueprint $table) {
            $table->unsignedBigInteger('studie_id')->nullable();
        });
        Persoon::all()->each(function ($value) {
            $studie = Studie::where(['naam' => $value->opleiding])->first();
            $value->studie()->associate($studie);
            $value->save();
        });
        Schema::table('personen', function (Blueprint $table) {
            $table->unsignedBigInteger('studie_id')->change();
            $table->dropColumn('opleiding');
            $table->foreign('studie_id')->references('id')->on('studies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personen', function (Blueprint $table) {
            $table->dropForeign(['studie_id']);
            $table->string('opleiding')->nullable();
            $table->unsignedBigInteger('studie_id')->nullable()->change();
        });

        Persoon::all()->each(function ($value) {
            $value->opleiding = $value->studie()->naam;
            $value->save();
        });

        Schema::table('personen', function (Blueprint $table) {
            $table->string('opleiding')->change();
            $table->dropColumn('studie_id');
        });
    }
}
