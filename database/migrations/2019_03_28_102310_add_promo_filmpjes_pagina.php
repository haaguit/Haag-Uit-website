<?php

use HUplicatie\Pagina;
use Illuminate\Database\Migrations\Migration;

class AddPromoFilmpjesPagina extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Pagina::create([
            'naam'  => 'promo',
            'titel' => 'Promo video\'s!',
            'tekst' => '
    <h3 class="box-sub-title"> Promo 2018 </h3>
    <iframe width="616" height="347" style="background: #ebebeb; border-radius:  6px; padding: 16px;" src="https://www.youtube.com/embed/JaWtJ008QG4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
    <br> <br>
    <h3 class="box-sub-title"> Promo 2014 </h3>
    <iframe width="616" height="347" style="background: #ebebeb; border-radius:  6px; padding: 16px;" src="https://www.youtube.com/embed/MsRNUb6CHQI" frameborder="0" allowfullscreen=""></iframe>
    <br> <br>
    <h3 class="box-sub-title"> Promo 2013 </h3>
    <iframe width="616" height="347" style="background: #ebebeb; border-radius:  6px; padding: 16px; " src="https://www.youtube.com/embed/ihBCyVDhxsU?rel=0&amp;vq=hd1080" frameborder="0" allowfullscreen=""></iframe>',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Pagina::where('naam', 'promo')->delete();
    }
}
