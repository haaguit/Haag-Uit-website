<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMedewerkersFunctieKeuzes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eerste_keus', function (Blueprint $table) {
            $table->bigInteger('medewerker_id');
            $table->bigInteger('functie_id');
            $table->timestamps();
        });
        Schema::create('tweede_keus', function (Blueprint $table) {
            $table->bigInteger('medewerker_id');
            $table->bigInteger('functie_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tweede_keus');
        Schema::dropIfExists('eerste_keus');
    }
}
