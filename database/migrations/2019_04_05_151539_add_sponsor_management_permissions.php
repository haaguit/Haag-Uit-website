<?php

use HUplicatie\Authorization\Roles;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddSponsorManagementPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => 'View Sponsoren']);
        Permission::create(['name' => 'View Sponsor']);
        Permission::create(['name' => 'Create Sponsor']);
        Permission::create(['name' => 'Edit Sponsor']);
        Permission::create(['name' => 'Delete Sponsor']);

        Role::findByName(Roles::STAFFER)->givePermissionTo([
            'View Sponsoren',
            'View Sponsor',
            'Create Sponsor',
            'Edit Sponsor',
            'Delete Sponsor',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereIn('name', [
            'View Sponsoren',
            'View Sponsor',
            'Create Sponsor',
            'Edit Sponsor',
            'Delete Sponsor',
        ])->delete();
    }
}
