<?php

namespace Tests\Setup;

use HUplicatie\Betaling;
use HUplicatie\KampInschrijving;
use Illuminate\Support\Collection;

class KampInschrijvingFactory
{
    private $betalingen = [];

    public function withIdealBetaling($status = 'open'): KampInschrijvingFactory
    {
        array_push($this->betalingen, ['type' => 'ideal', 'status' => $status]);

        return $this;
    }

    public function withIncassoBetaling($status = 'open'): KampInschrijvingFactory
    {
        array_push($this->betalingen, ['type' => 'incasso', 'status' => $status]);

        return $this;
    }

    public function create()
    {
        $inschrijving = factory(KampInschrijving::class)->create();

        Collection::make($this->betalingen)->each(function ($value) use ($inschrijving) {
            factory(Betaling::class)->create([
                'type'                 => $value['type'],
                'status'               => $value['status'],
                'kamp_inschrijving_id' => $inschrijving->id,
            ]);
        });

        return $inschrijving;
    }
}
