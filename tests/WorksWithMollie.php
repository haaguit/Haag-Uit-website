<?php

namespace Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Utils;
use Mollie\Api\MollieApiClient;
use Tests\utilities\MollieMockResponseFactory;

trait WorksWithMollie
{
    public $mollieRequestHistory = [];

    /**
     * Make Mollie's Guzzle instance use a mock handler.
     *
     * @see http://docs.guzzlephp.org/en/stable/testing.html
     *
     * @return MockHandler
     *
     * @throws \Mollie\Api\Exceptions\IncompatiblePlatform
     * @throws \Mollie\Api\Exceptions\IncompatiblePlatform
     */
    public function mockMollie(): MockHandler
    {
        $handler = HandlerStack::create(
            $mockHandler = new MockHandler()
        );

        $handler->push(
            Middleware::history($this->mollieRequestHistory)
        );

        $guzzle = new Client(['handler' => $handler]);

        $this->app->singleton('mollie.api.client', function () use ($guzzle) {
            return new MollieApiClient($guzzle);
        });

        return $mockHandler;
    }

    public function createStandardMollieResponse(): \GuzzleHttp\Psr7\Response
    {
        return (new MollieMockResponseFactory())
            ->transactieId('tr_123131')
            ->amount('100.00')
            ->paymentUrl('https://lekkerbetalen.com')
            ->build();
    }

    public function getRequestBodyFromHistory(int $number)
    {
        if (array_key_exists(--$number, $this->mollieRequestHistory)) {
            return json_decode(Utils::copyToString($this->mollieRequestHistory[$number]['request']->getBody()), false);
        }

        return '';
    }
}
