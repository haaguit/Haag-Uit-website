<?php

namespace Tests\Stafplicatie\Feature\Users;

use HUplicatie\Authorization\Roles;
use HUplicatie\Medewerker;
use HUplicatie\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class DeleteMedewerkerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if a medewerker can be deleted.
     *
     * @test
     *
     * @return void
     */
    public function a_medewerker_can_be_deleted(): void
    {
        $this->withExceptionHandling();
        $medewerker = create(Medewerker::class, ['id' => 2]);

        $this->delete("/stafplicatie/medewerker/{$medewerker->id}")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->delete("/stafplicatie/medewerker/{$medewerker->id}")
            ->assertSee('403');

        $this->signInWithPermissions('Delete Medewerker');
        $this->json('DELETE', "/stafplicatie/medewerker/{$medewerker->id}")
            ->assertStatus(204);

        $medewerker = create(Medewerker::class, ['id' => 2]);
        $this->delete("/stafplicatie/medewerker/{$medewerker->id}")
            ->assertRedirect('/stafplicatie/medewerker');
    }
}
