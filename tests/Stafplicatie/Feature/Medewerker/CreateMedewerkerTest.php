<?php

namespace Tests\Stafplicatie\Feature\Medewerker;

use function app;
use Carbon\Carbon;
use HUplicatie\Aanwezigheid;
use HUplicatie\Actiepunt;
use HUplicatie\Authorization\Roles;
use HUplicatie\Functie;
use HUplicatie\Medewerker;
use HUplicatie\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\Setup\KampjaarFactory;
use Tests\TestCase;

class CreateMedewerkerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if the right permissions are enforced.
     *
     * @test
     *
     * @return void
     */
    public function the_right_permissions_are_enforced(): void
    {
        $this->withExceptionHandling();
        $this->post('/stafplicatie/medewerker')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->post('/stafplicatie/medewerker')
            ->assertStatus(403);

        $this->signInWithPermissions('Create Medewerker');
        $this->post('/stafplicatie/medewerker', [])
            ->assertStatus(302);
    }

    /**
     * Test if a medewerker can be created.
     *
     * @test
     *
     * @return void
     */
    public function a_medewerker_can_be_created(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);

        $this->assertEquals(0, Medewerker::count());
        $this->signInWithPermissions('Create Medewerker');
        $response = $this->post('/stafplicatie/medewerker', [
            'voorletters' => '',
            'voornaam' => 'Snoert',
            'achternaam' => 'Snabbelstaart',
            'roepnaam' => '',
            'straat' => 'Straatje',
            'huisnummer' => '10',
            'postcode' => '2172XR',
            'woonplaats' => 'Malmö',
            'mobiel' => '0612312312',
            'telefoon_nood' => '0612312321',
            'studentnummer' => '11058687',
            'geboortedatum' => '1990-02-10',
            'email' => 'test@email.com',
            'geslacht' => 'm',
            'rijbewijs' => 'B',
            'ehbo' => 'Geen',
            'studie_id' => 1,
            'eerste_keus' => ['BAR'],
            'tweede_keus' => ['Leider', 'Keuken'],
            'ervaring' => 'Joh denk je?',
            'motivatie' => 'Kan toch wel',
            'eigenschappen' => 'Ik maak websites',
        ]);

        $response->assertSessionDoesntHaveErrors();

        $response->assertStatus(302);
        $this->assertEquals(1, Medewerker::count());
        $this->assertDatabaseHas('medewerkers', [
            'studentnummer' => '11058687',
            'rijbewijs' => 'B',
        ]);

        $this->assertDatabaseHas('personen', [
            'voornaam' => 'Snoert',
            'email' => 'test@email.com',
        ]);

        $medewerker = Medewerker::where('studentnummer', '11058687')->first();
        $this->assertNotNull($medewerker);
        $this->assertNotNull($medewerker->persoonsgegevens);
        $this->assertNotNull($medewerker->persoonsgegevens->studie);
        $this->assertCount(1, $medewerker->eerste_keus);
        $this->assertCount(2, $medewerker->tweede_keus);
    }

    /**
     * Test if medewerker creations are validated.
     *
     * @test
     *
     * @return void
     */
    public function medewerker_creations_are_validated(): void
    {
        $this->withExceptionHandling();
        $this->assertEquals(0, Actiepunt::count());
        $this->signInWithPermissions('Create Medewerker');
        $this->postJson('/stafplicatie/medewerker', [])
            ->assertStatus(422);

        $this->assertEquals(0, Actiepunt::count());
    }

    /**
     * Test if a medewerker cannot be created with invalid functie choices.
     *
     * @test
     *
     * @return void
     */
    public function an_medewerker_can_not_be_created_with_invalid_keuzes(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);

        $this->assertEquals(0, Medewerker::count());
        $this->signInWithPermissions('Create Medewerker');
        $response = $this->withExceptionHandling()->post('/stafplicatie/medewerker', [
            'voorletters' => '',
            'voornaam' => 'Snoert',
            'achternaam' => 'Snabbelstaart',
            'roepnaam' => '',
            'straat' => 'Straatje',
            'huisnummer' => '10',
            'postcode' => '2172XR',
            'woonplaats' => 'Malmö',
            'mobiel' => '0612312312',
            'telefoon_nood' => '0612312321',
            'studentnummer' => '11058687',
            'geboortedatum' => '1990-02-10',
            'email' => 'test@email.com',
            'geslacht' => 'm',
            'rijbewijs' => 'B',
            'ehbo' => 'Geen',
            'studie_id' => 1,
            'eerste_keus' => ['Tasje'],
            'tweede_keus' => ['Mascotte', 'Bierenbeul'],
            'ervaring' => 'Joh denk je?',
            'motivatie' => 'Kan toch wel',
            'eigenschappen' => 'Ik maak websites',
        ]);

        $response->assertStatus(302)
            ->assertSessionHasErrors([
                'eerste_keus.0', 'tweede_keus.0', 'tweede_keus.1',
            ]);
    }

    /**
     * Test if creating a medewerker also creates aanwezigheid.
     *
     * @test
     *
     * @return void
     */
    public function creating_medewerker_creates_aanwezigheid(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);

        $this->assertEquals(0, Medewerker::count());
        $this->assertEquals(0, Aanwezigheid::count());

        $this->signInWithPermissions('Create Medewerker');
        $response = $this->post('/stafplicatie/medewerker', [
            'voorletters' => '',
            'voornaam' => 'Snoert',
            'achternaam' => 'Snabbelstaart',
            'roepnaam' => '',
            'straat' => 'Straatje',
            'huisnummer' => '10',
            'postcode' => '2172XR',
            'woonplaats' => 'Malmö',
            'mobiel' => '0612312312',
            'telefoon_nood' => '0612312321',
            'studentnummer' => '11058687',
            'geboortedatum' => '1990-02-10',
            'email' => 'test@email.com',
            'geslacht' => 'm',
            'rijbewijs' => 'B',
            'ehbo' => 'Geen',
            'studie_id' => 1,
            'eerste_keus' => ['BAR'],
            'tweede_keus' => ['Leider', 'Keuken'],
            'ervaring' => 'Joh denk je?',
            'motivatie' => 'Kan toch wel',
            'eigenschappen' => 'Ik maak websites',
        ]);

        $response->assertSessionDoesntHaveErrors();

        $response->assertStatus(302);
        $this->assertEquals(1, Medewerker::count());

        $this->assertCount(5, Medewerker::find(1)->aanwezigheid);
    }
}
