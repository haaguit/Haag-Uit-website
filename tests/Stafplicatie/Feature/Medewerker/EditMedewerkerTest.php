<?php

namespace Tests\Stafplicatie\Feature\Medewerker;

use function create;
use HUplicatie\Medewerker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\Setup\KampjaarFactory;
use Tests\TestCase;

class EditMedewerkerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if the right permissions are enforced.
     *
     * @test
     *
     * @return void
     */
    public function the_right_permissions_are_enforced(): void
    {
        create(Medewerker::class);

        $this->withExceptionHandling();
        $this->put('/stafplicatie/medewerker/1')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->put('/stafplicatie/medewerker/1')
            ->assertStatus(403);

        $this->signInWithPermissions('Edit Medewerker');
        $this->put('/stafplicatie/medewerker/1')
            ->assertStatus(302);
    }

    /**
     * Test if an edit form can be displayed for a medewerker.
     *
     * @test
     *
     * @return void
     */
    public function a_medewerker_edit_form_can_be_displayed(): void
    {
        $this->signInWithPermissions('Edit Medewerker');
        $medewerker = create(Medewerker::class, ['jaar' => 2019]);

        $response = $this->get("/stafplicatie/medewerker/{$medewerker->id}/edit");

        $response->assertStatus(200)
            ->assertSee($medewerker->persoonsgegevens->email);
    }

    /**
     * Test if a medewerker can be edited.
     *
     * @test
     *
     * @return void
     */
    public function a_medewerker_can_be_edited(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);
        $medewerker = create(Medewerker::class, ['jaar' => 2019]);

        $this->assertDatabaseHas('medewerkers', [
            'studentnummer' => $medewerker->studentnummer,
            'rijbewijs' => $medewerker->rijbewijs,
        ]);

        $this->assertDatabaseHas('personen', [
            'voornaam' => $medewerker->persoonsgegevens->voornaam,
            'email' => $medewerker->persoonsgegevens->email,
        ]);
        $this->signInWithPermissions('Edit Medewerker');
        $response = $this->put("/stafplicatie/medewerker/{$medewerker->id}", [
            'voorletters' => '',
            'voornaam' => 'Snoert',
            'achternaam' => 'Snabbelstaart',
            'roepnaam' => '',
            'straat' => 'Straatje',
            'huisnummer' => '10',
            'postcode' => '2172XR',
            'woonplaats' => 'Malmö',
            'mobiel' => '0612312312',
            'telefoon_nood' => '0612312321',
            'studentnummer' => '11058687',
            'geboortedatum' => '1990-02-10',
            'email' => 'test@email.com',
            'geslacht' => 'm',
            'rijbewijs' => 'B',
            'ehbo' => 'Geen',
            'studie_id' => 1,
            'eerste_keus' => ['BAR'],
            'tweede_keus' => ['Leider', 'Keuken'],
            'ervaring' => 'Joh denk je?',
            'motivatie' => 'Kan toch wel',
            'eigenschappen' => 'Ik maak websites',
        ]);

        $response->assertSessionDoesntHaveErrors();

        $response->assertStatus(302);
        $this->assertDatabaseHas('medewerkers', [
            'studentnummer' => '11058687',
            'rijbewijs' => 'B',
        ]);

        $this->assertDatabaseHas('personen', [
            'voornaam' => 'Snoert',
            'email' => 'test@email.com',
        ]);

        $medewerker = Medewerker::where('studentnummer', '11058687')->first();
        $this->assertNotNull($medewerker);
        $this->assertNotNull($medewerker->persoonsgegevens);
        $this->assertNotNull($medewerker->persoonsgegevens->studie);
        $this->assertCount(1, $medewerker->eerste_keus);
        $this->assertCount(2, $medewerker->tweede_keus);
    }
}
