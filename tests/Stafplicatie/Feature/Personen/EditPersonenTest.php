<?php

namespace Tests\Stafplicatie\Feature\Personen;

use Hash;
use HUplicatie\Authorization\Roles;
use HUplicatie\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class EditPersonenTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if a user can be edited.
     *
     * @test
     *
     * @return void
     */
    public function a_user_can_be_edited(): void
    {
        $this->withExceptionHandling();
        $user = create(User::class, ['id' => 2]);

        $this->get('/stafplicatie/user/2/edit')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get('/stafplicatie/user/2/edit')
            ->assertSee('403');

        $this->signInWithPermissions('Edit User');
        $this->get('/stafplicatie/user/2/edit')
            ->assertOk()
            ->assertSee(e($user->name))
            ->assertDontSee(Roles::WEBMEESTER);
    }

    /**
     * Test if a user can be updated.
     *
     * @test
     *
     * @return void
     */
    public function a_user_can_be_updated(): void
    {
        $this->withExceptionHandling();
        $user = create(User::class, ['id' => 2, 'password' => Hash::make('password')]);
        $user->assignRole(Roles::MEDEWERKER);

        $this->put('/stafplicatie/user/2')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->put('/stafplicatie/user/2')
            ->assertSee('403');

        $this->signInWithPermissions('Edit User');
        $this->withoutExceptionHandling()->json('PUT', '/stafplicatie/user/2', [
            'name'                  => 'Test name',
            'email'                 => 'test@example.com',
            'password'              => 'testerino',
            'password_confirmation' => 'testerino',
            'role'                  => 'Staffer',
        ])
            ->assertSessionHasNoErrors()
            ->assertRedirect('/stafplicatie/user')
            ->assertSessionHas('flash', 'Gebruiker gewijzigd');

        $user->refresh();
        $this->assertEquals('Test name', $user->name);
        $this->assertEquals('test@example.com', $user->email);
        $this->assertTrue(Hash::check('testerino', $user->password));
        $this->assertTrue($user->hasRole(Roles::STAFFER));
        $this->assertFalse($user->hasRole(Roles::MEDEWERKER));
    }

    /**
     * Test if a password remains unchanged when not submitted.
     *
     * @test
     *
     * @return void
     */
    public function a_password_remains_unchanged_when_not_submitted(): void
    {
        $this->withExceptionHandling();
        $user = create(User::class, ['id' => 2, 'password' => Hash::make('password')]);
        $user->assignRole(Roles::MEDEWERKER);

        $this->signInWithPermissions('Edit User');
        $this->withoutExceptionHandling()->json('PUT', '/stafplicatie/user/2', [
            'name'  => 'Test name',
            'email' => 'test@example.com',
            'role'  => 'Staffer',
        ])
            ->assertSessionHasNoErrors()
            ->assertRedirect('/stafplicatie/user')
            ->assertSessionHas('flash', 'Gebruiker gewijzigd');

        $user->refresh();
        $this->assertEquals('Test name', $user->name);
        $this->assertEquals('test@example.com', $user->email);
        $this->assertTrue(Hash::check('password', $user->password));
        $this->assertTrue($user->hasRole(Roles::STAFFER));
        $this->assertFalse($user->hasRole(Roles::MEDEWERKER));
    }

    /**
     * Test if a user edit is validated.
     *
     * @test
     *
     * @return void
     */
    public function updates_are_validated(): void
    {
        $this->withExceptionHandling();
        $user = create(User::class, ['id' => 2]);
        $user->assignRole(Roles::MEDEWERKER);

        $this->signInWithPermissions('Edit User');
        $this->put('/stafplicatie/user/2', [])
            ->assertSessionHasErrors(['name', 'email', 'role']);
    }

    /**
     * Test if a user with id 1 cannot be updated by a non webmeester.
     *
     * @test
     *
     * @return void
     */
    public function user_id_1_cannot_be_updated_by_non_webmeester_users(): void
    {
        $this->withExceptionHandling();

        $this->signInAsRole(Roles::STAFFER);
        $this->put(
            '/stafplicatie/user/1',
            ['name' => 'Test', 'email' => 'hacker@example.com', 'role' => 'Inschrijving']
        )->assertStatus(403);
    }

    /**
     * Test if a user cannot be updated to webmeester.
     *
     * @test
     *
     * @return void
     */
    public function a_user_cannot_be_updated_to_webmeester(): void
    {
        $this->withExceptionHandling();

        $user = create(User::class, ['id' => 2]);
        $user->assignRole(Roles::MEDEWERKER);

        $this->signInAsRole(Roles::STAFFER);
        $this->put('/stafplicatie/user/2', ['name' => 'Test', 'email' => 'hacker@example.com', 'role' => 'Webmeester'])
            ->assertSessionHasErrors('role');

        $user->refresh();
        $this->assertFalse($user->hasRole(Roles::WEBMEESTER));
        $this->assertTrue($user->hasRole(Roles::MEDEWERKER));
    }
}
