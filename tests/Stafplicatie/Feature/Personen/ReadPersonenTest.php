<?php

namespace Tests\Stafplicatie\Feature\Personen;

use HUplicatie\Authorization\Roles;
use HUplicatie\KampInschrijving;
use HUplicatie\Kampjaar;
use HUplicatie\Medewerker;
use HUplicatie\Persoon;
use HUplicatie\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class ReadPersonenTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if all Persons can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function all_personen_can_be_viewed(): void
    {
        $this->withExceptionHandling();
        $this->get('/stafplicatie/persoon')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get('/stafplicatie/persoon')
            ->assertSee('403');

        $this->signInWithPermissions('View Personen');
        $person = create(Persoon::class);

        $this->withoutExceptionHandling()->get('/stafplicatie/persoon')
            ->assertOk()
            ->assertSee(e($person->voornaam))
            ->assertSee($person->achternaam);
    }

    /**
     * Test if a Persoon can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function a_persoon_can_be_viewed(): void
    {
        $this->withExceptionHandling();
        $person = create(Persoon::class);
        create(KampInschrijving::class, ['persoon_id' => $person->id]);

        $this->get("/stafplicatie/persoon/{$person->id}")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get("/stafplicatie/persoon/{$person->id}")
            ->assertSee('403');

        $this->signInWithPermissions('View Persoon');
        $this->withoutExceptionHandling()->get("/stafplicatie/persoon/{$person->id}")
            ->assertOk()
            ->assertSee(e($person->voornaam))
            ->assertSee($person->email)
            ->assertSee($person->kampInschrijving->jaar);
    }

    /**
     * Test if a Persoon can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function a_persoon_without_kampinschrijving_can_be_viewed(): void
    {
        $this->withExceptionHandling();
        $person = create(Persoon::class);

        $this->signInWithPermissions('View Persoon');
        $this->withoutExceptionHandling()->get("/stafplicatie/persoon/{$person->id}")
            ->assertOk()
            ->assertSee(e($person->voornaam))
            ->assertSee($person->email)
            ->assertSeeText('Geen feut');
    }

    /**
     * Test if a Persoon can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function a_persoon_can_be_viewed_with_all_registrations(): void
    {
        $this->withExceptionHandling();
        $person = create(Persoon::class);
        $kampInschrijving = create(KampInschrijving::class, [
            'persoon_id' => $person->id,
            'jaar' => create(Kampjaar::class, ['jaar' => 2011]),
        ]);
        $medewerkerInschrijving1 = create(Medewerker::class, [
            'persoon_id' => $person->id,
            'jaar' => create(Kampjaar::class, ['jaar' => 2012]),
        ]);
        $medewerkerInschrijving2 = create(Medewerker::class, [
            'persoon_id' => $person->id,
            'jaar' => create(Kampjaar::class, ['jaar' => 2013]),
        ]);

        $this->signInWithPermissions('View Persoon');
        $this->withoutExceptionHandling()->get("/stafplicatie/persoon/{$person->id}")
            ->assertOk()
            ->assertSee(e($person->voornaam))
            ->assertSee($person->email)
            ->assertSee($kampInschrijving->jaar)
            ->assertSee($medewerkerInschrijving1->jaar)
            ->assertSee($medewerkerInschrijving2->jaar);
    }
}
