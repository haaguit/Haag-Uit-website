<?php

namespace Tests\Stafplicatie\Feature\Actiepunt;

use Carbon\Carbon;
use Exception;
use HUplicatie\Actiepunt;
use HUplicatie\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\Setup\KampjaarFactory;
use Tests\TestCase;

class ReadActiepuntTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if all actiepunten can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function all_actiepunten_can_be_viewed(): void
    {
        $this->withExceptionHandling();
        $this->get('/stafplicatie/actiepunt')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get('/stafplicatie/actiepunt')
            ->assertSee('403');

        $this->signInWithPermissions('View Actiepunten');
        $users = create(User::class, [], 2);
        $kampjaar = app(KampjaarFactory::class)->isActief()->create(2019);
        $actiepunt = create(Actiepunt::class, ['jaar' => $kampjaar->jaar]);
        $actiepuntDone = create(Actiepunt::class, ['jaar' => $kampjaar->jaar, 'deleted_at' => Carbon::now()]);
        $actiepunt->users()->sync($users);

        $response = $this->get('/stafplicatie/actiepunt');

        $response->assertSee($actiepunt->beschrijving)
            ->assertSee($actiepunt->deadline->format('Y-m-d H:i:s'))
            ->assertSee(e(json_encode($actiepunt->users[0]->only('name', 'id', 'pivot'))))
            ->assertSee(e(json_encode($actiepunt->users[1]->only('name', 'id', 'pivot'))));

        $response->assertSee($actiepuntDone->beschrijving)
            ->assertSee($actiepuntDone->deadline->format('Y-m-d H:i:s'))
            ->assertSee($actiepuntDone->deleted_at->format('Y-m-d H:i:s'));
    }

    /**
     * Test if an Actiepunt can be requested as JSON.
     *
     * @test
     *
     * @return void
     *
     * @throws Exception
     */
    public function an_actiepunt_can_be_requested_as_json(): void
    {
        $ap = create(Actiepunt::class);
        $users = create(User::class, [], 2);
        $ap->users()->sync($users);

        $this->withExceptionHandling();
        $this->getJson("/stafplicatie/actiepunt/{$ap->id}")
            ->assertStatus(401);

        $this->signIn();
        $this->getJson("/stafplicatie/actiepunt/{$ap->id}")
            ->assertStatus(403);

        $this->signInWithPermissions('View Actiepunt');
        $this->getJson("/stafplicatie/actiepunt/{$ap->id}")
            ->assertJson([
                'id'            => $ap->id,
                'assignedUsers' => $users->only('id', 'name')->toArray(),
            ]);
        $ap->delete();
        $this->getJson("/stafplicatie/actiepunt/{$ap->id}")
            ->assertJson([
                'id'            => $ap->id,
                'assignedUsers' => $users->only('id', 'name')->toArray(),
            ]);
    }
}
