<?php

namespace Tests\Stafplicatie\Feature\Actiepunt;

use Carbon\Carbon;
use HUplicatie\Actiepunt;
use HUplicatie\Authorization\Roles;
use HUplicatie\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\Setup\KampjaarFactory;
use Tests\TestCase;

class CreateActiepuntTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if the right permissions are enforced.
     *
     * @test
     *
     * @return void
     */
    public function the_right_permissions_are_enforced(): void
    {
        $this->withExceptionHandling();
        $this->postJson('/stafplicatie/actiepunt')
            ->assertStatus(401);

        $this->signIn();
        $this->postJson('/stafplicatie/actiepunt')
            ->assertStatus(403);

        $this->signInWithPermissions('Create Actiepunt');
        $this->postJson('/stafplicatie/actiepunt', [])
            ->assertStatus(422);
    }

    /**
     * Test if an actiepunt can be created.
     *
     * @test
     *
     * @return void
     */
    public function an_actiepunt_can_be_created(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);
        $users = create(User::class, [], 3);
        $users->each->assignRole(Roles::STAFFER);

        $this->assertEquals(0, Actiepunt::count());
        $this->signInWithPermissions('Create Actiepunt');
        $response = $this->postJson('/stafplicatie/actiepunt', [
            'beschrijving'  => 'Ff tenten regelen tochhhh',
            'deadline'      => Carbon::now()->addWeek()->format('Y-m-d'),
            'assignedUsers' => $users->map(function ($user) {
                return $user->only(['id', 'name']);
            })->take(2),
        ]);

        $response->assertStatus(201)
            ->assertJson([
                'beschrijving'  => 'Ff tenten regelen tochhhh',
                'deadline'      => Carbon::now()->addWeek()->format('Y-m-d'),
                'assignedUsers' => $users->only('id', 'name')->toArray(),
            ]);

        $this->assertEquals(1, Actiepunt::count());
        $this->assertDatabaseHas('Actiepunten', [
            'beschrijving' => 'Ff tenten regelen tochhhh',
            'deadline'     => Carbon::now()->addWeek()->format('Y-m-d'),
        ]);

        $ap = Actiepunt::where('beschrijving', 'Ff tenten regelen tochhhh')->first();
        $this->assertNotNull($ap);
        $this->assertCount(2, $ap->users);

        $this->assertDatabaseHas('actiepunt_user', [
            'actiepunt_id' => $ap->id,
            'user_id'      => $users->pluck('id')[0],
        ]);
        $this->assertDatabaseHas('actiepunt_user', [
            'actiepunt_id' => $ap->id,
            'user_id'      => $users->pluck('id')[1],
        ]);

        $this->assertDatabaseMissing('actiepunt_user', [
            'actiepunt_id' => $ap->id,
            'user_id'      => $users->pluck('id')[2],
        ]);
    }

    /**
     * Test if the creation of an actiepunt is validated.
     *
     * @test
     *
     * @return void
     */
    public function actiepunt_creations_are_validated(): void
    {
        $this->withExceptionHandling();
        $this->assertEquals(0, Actiepunt::count());
        $this->signInWithPermissions('Create Actiepunt');
        $this->postJson('/stafplicatie/actiepunt', [])
            ->assertStatus(422);

        $this->assertEquals(0, Actiepunt::count());
    }

    /**
     * Test if an actiepunt can be created without a deadline.
     *
     * @test
     *
     * @return void
     */
    public function an_ap_can_be_created_without_a_deadline(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);
        $users = create(User::class, [], 3);
        $users->each->assignRole(Roles::STAFFER);

        $this->assertEquals(0, Actiepunt::count());
        $this->signInWithPermissions('Create Actiepunt');
        $response = $this->postJson('/stafplicatie/actiepunt', [
            'beschrijving'  => 'Ff tenten regelen tochhhh',
            'assignedUsers' => $users->map(function ($user) {
                return $user->only(['id', 'name']);
            })->take(2),
        ]);

        $response->assertStatus(201)
            ->assertJson([
                'beschrijving'  => 'Ff tenten regelen tochhhh',
                'assignedUsers' => $users->only('id', 'name')->toArray(),
            ]);
    }

    /**
     * Test if an actiepunt requires one or multiple assigned users.
     *
     * @test
     *
     * @return void
     */
    public function an_actiepunt_requires_users(): void
    {
        $this->withExceptionHandling();
        $this->assertEquals(0, Actiepunt::count());
        $this->signInWithPermissions('Create Actiepunt');

        $this->postJson('/stafplicatie/actiepunt', [
            'beschrijving'  => 'Ff tenten regelen tochhhh',
            'deadline'      => Carbon::now()->addWeek()->format('Y-m-d'),
            'assignedUsers' => [],
        ])->assertStatus(422);

        $this->assertEquals(0, Actiepunt::count());
    }

    /**
     * Test if an actiepunt can not be assigned to a non existent user.
     *
     * @test
     *
     * @return void
     */
    public function an_actiepunt_can_not_be_assigned_to_a_non_existent_user(): void
    {
        $this->withExceptionHandling();
        $this->assertEquals(0, Actiepunt::count());
        $this->signInWithPermissions('Create Actiepunt');
        $this->assertNull(User::find(99));
        $response = $this->postJson('/stafplicatie/actiepunt', [
            'beschrijving' => 'Ff tenten regelen tochhhh',
            'deadline'     => Carbon::now()->addWeek()->format('Y-m-d'),
            'users'        => [99],
        ]);

        $response->assertStatus(422);

        $this->assertEquals(0, Actiepunt::count());
    }

    /**
     * Test if an actiepunt can not be assigned to a non staffer.
     *
     * @test
     *
     * @return void
     */
    public function an_actiepunt_can_not_be_assigned_to_a_non_staffer(): void
    {
        $this->withExceptionHandling();
        $this->assertEquals(0, Actiepunt::count());
        $this->signInWithPermissions('Create Actiepunt');
        $user = create(User::class);
        $this->assertFalse($user->hasRole(Roles::STAFFER));

        $this->postJson('/stafplicatie/actiepunt', [
            'beschrijving' => 'Ff tenten regelen tochhhh',
            'deadline'     => Carbon::now()->addWeek()->format('Y-m-d'),
            'users'        => [$user->id],
        ])->assertStatus(422);

        $this->assertEquals(0, Actiepunt::count());
    }
}
