<?php

namespace Tests\Stafplicatie\Feature\Actiepunt;

use Carbon\Carbon;
use HUplicatie\Actiepunt;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class EditActiepuntStatusTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if the right permissions are enforced.
     *
     * @test
     *
     * @return void
     */
    public function the_right_permission_are_enforced(): void
    {
        $ap = create(Actiepunt::class);
        $this->withExceptionHandling();
        $this->postJson("/stafplicatie/api/actiepunt/{$ap->id}/status")
            ->assertStatus(401);
        $this->deleteJson("/stafplicatie/api/actiepunt/{$ap->id}/status")
            ->assertStatus(401);

        $this->signIn();
        $this->postJson("/stafplicatie/api/actiepunt/{$ap->id}/status")
            ->assertStatus(403);
        $this->deleteJson("/stafplicatie/api/actiepunt/{$ap->id}/status")
            ->assertStatus(403);

        $this->signInWithPermissions('Edit Actiepunt');

        $this->postJson("/stafplicatie/api/actiepunt/{$ap->id}/status")
            ->assertStatus(200);
        $this->deleteJson("/stafplicatie/api/actiepunt/{$ap->id}/status")
            ->assertStatus(200);
    }

    /**
     * Test if an actiepunt can be marked as done.
     *
     * @test
     *
     * @return void
     */
    public function an_actiepunt_can_be_marked_as_done(): void
    {
        $ap = create(Actiepunt::class);
        $this->assertFalse($ap->trashed());

        $this->signInWithPermissions('Edit Actiepunt');

        $this->deleteJson("/stafplicatie/api/actiepunt/{$ap->id}/status")
            ->assertStatus(200);

        $this->assertTrue($ap->fresh()->trashed());
    }

    /**
     * Test if an actiepunt can be marked as not done.
     *
     * @test
     *
     * @return void
     */
    public function an_actiepunt_can_be_marked_as_not_done(): void
    {
        $ap = create(Actiepunt::class, ['deleted_at' => Carbon::now()]);
        $this->assertTrue($ap->trashed());

        $this->signInWithPermissions('Edit Actiepunt');

        $this->postJson("/stafplicatie/api/actiepunt/{$ap->id}/status")
            ->assertStatus(200);

        $this->assertFalse($ap->fresh()->trashed());
    }
}
