<?php

namespace Tests\Stafplicatie\Feature\Actiepunt;

use HUplicatie\Authorization\Roles;
use HUplicatie\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class ReadActiepuntUsersTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if all users for an actiepunt can be retrieved.
     *
     * @test
     *
     * @return void
     */
    public function all_users_for_actiepunten_can_be_retrieved(): void
    {
        $this->withExceptionHandling();
        $this->getJson('/stafplicatie/api/actiepunt/users')
            ->assertStatus(401);

        $this->signIn();
        $this->getJson('/stafplicatie/api/actiepunt/users')
            ->assertStatus(403);

        $this->signInWithPermissions('Create Actiepunt');
        $users = create(User::class, [], 2);
        $users->each->assignRole(Roles::STAFFER);

        $response = $this->getJson('/stafplicatie/api/actiepunt/users');

        $response->assertStatus(200);
        $response->assertExactJson([
            0 => [
                'id'   => $users[0]->id,
                'name' => $users[0]->name,
            ],
            1 => [
                'id'   => $users[1]->id,
                'name' => $users[1]->name,
            ],
        ]);
    }
}
