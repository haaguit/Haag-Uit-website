<?php

namespace Tests\Stafplicatie\Feature\Pagina;

use HUplicatie\Pagina;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\Setup\KampjaarFactory;
use Tests\TestCase;

class ReadPaginaTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if all Pagina's can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function all_paginas_can_be_viewed(): void
    {
        $this->withExceptionHandling();
        $this->get('/stafplicatie/pagina')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get('/stafplicatie/pagina')
            ->assertSee('403');

        $this->signInWithPermissions('View Paginas');
        $response = $this->get('/stafplicatie/pagina');
        $paginas = Pagina::all();
        $response->assertSee($paginas->first()->titel)
            ->assertSee(e($paginas->last()->titel));
    }

    /**
     * Test if a Pagina can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function a_pagina_can_be_viewed(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);
        $this->withExceptionHandling();
        $pagina = Pagina::where('naam', 'home')->first();

        $this->get("/stafplicatie/pagina/{$pagina->id}")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get("/stafplicatie/pagina/{$pagina->id}")
            ->assertSee('403');

        $this->signInWithPermissions('View Pagina');
        $this->get("/stafplicatie/pagina/{$pagina->id}")
            ->assertSee($pagina->titel)
            ->assertSee($pagina->tekst);
    }
}
