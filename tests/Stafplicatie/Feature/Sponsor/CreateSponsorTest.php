<?php

namespace Tests\Stafplicatie\Feature\Sponsor;

use HUplicatie\Sponsor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class CreateSponsorTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Storage::fake('public');
        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if a form can be shown to create a sponsor.
     *
     * @test
     *
     * @return void
     */
    public function a_create_sponsor_form_can_be_shown(): void
    {
        $this->withExceptionHandling();

        $this->get('/stafplicatie/sponsor/create')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get('/stafplicatie/sponsor/create')
            ->assertSee('403');

        $this->signInWithPermissions('Create Sponsor');
        $this->get('/stafplicatie/sponsor/create')
            ->assertOk()
            ->assertSee('Sponsor aanmaken');
    }

    /**
     * Test if a sponsor can be created.
     *
     * @test
     *
     * @return void
     */
    public function a_sponsor_can_be_created(): void
    {
        $this->withoutExceptionHandling();
        $this->assertEquals(0, Sponsor::count());
        $this->signInWithPermissions('Create Sponsor');
        $response = $this->post('/stafplicatie/sponsor', [
            'naam' => 'sv SIM',
            'url'  => 'https://www.svsim.nl',
            'logo' => $logo = UploadedFile::fake()->image('svsim.png'),
        ]);

        $response->assertSessionHasNoErrors()
            ->assertRedirect('/stafplicatie/sponsor')
            ->assertSessionHas('flash', 'Sponsor aangemaakt');

        $this->assertDatabaseHas('Sponsoren', [
            'naam' => 'sv SIM',
            'url'  => 'https://www.svsim.nl',
            'logo' => 'sponsoren/'.$logo->hashName(),
        ]);

        $this->assertEquals(asset('storage/sponsoren/'.$logo->hashName()), Sponsor::find(1)->logo);

        Storage::disk('public')->assertExists('sponsoren/'.$logo->hashName());
        $this->assertEquals(1, Sponsor::count());
    }

    /**
     * Test if sponsor creations are validated.
     *
     * @test
     *
     * @return void
     */
    public function sponsor_creations_are_validated(): void
    {
        $this->withExceptionHandling();
        $this->assertEquals(0, Sponsor::count());
        $this->signInWithPermissions('Create Sponsor');
        $this->post('/stafplicatie/sponsor', [])
            ->assertSessionHasErrors(['naam', 'url', 'logo']);

        $this->assertEquals(0, Sponsor::count());
    }
}
