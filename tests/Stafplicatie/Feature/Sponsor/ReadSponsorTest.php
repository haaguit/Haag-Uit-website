<?php

namespace Tests\Stafplicatie\Feature\Sponsor;

use HUplicatie\Sponsor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class ReadSponsorTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if all sponsors can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function all_sponsoren_can_be_viewed(): void
    {
        $this->withExceptionHandling();
        $this->get('/stafplicatie/sponsor')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get('/stafplicatie/sponsor')
            ->assertSee('403');

        $this->signInWithPermissions('View Sponsoren');
        $sponsor = create(Sponsor::class);
        $sponsor2 = create(Sponsor::class);

        $response = $this->get('/stafplicatie/sponsor');

        $response->assertSee(e(json_encode($sponsor->naam)))
            ->assertSee(e(json_encode($sponsor->url)))
            ->assertSee(e(json_encode($sponsor->logo)));

        $response->assertSee(e(json_encode($sponsor2->naam)))
            ->assertSee(e(json_encode($sponsor2->url)))
            ->assertSee(e(json_encode($sponsor2->logo)));
    }
}
