<?php

namespace Tests\Stafplicatie\Feature\Sponsor;

use Exception;
use HUplicatie\Sponsor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class DeleteSponsorTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if a sponsor can be completely deleted.
     *
     * @test
     *
     * @return void
     *
     * @throws Exception
     * @throws Exception
     */
    public function a_sponsor_can_be_completely_deleted(): void
    {
        $this->withExceptionHandling();
        $sponsor = create(Sponsor::class);
        $sponsor->delete();
        $this->assertCount(1, Sponsor::withTrashed()->get());

        $this->delete("/stafplicatie/sponsor/{$sponsor->id}")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->delete("/stafplicatie/sponsor/{$sponsor->id}")
            ->assertSee('403');

        $this->signInWithPermissions('Delete Sponsor');

        $this->delete("/stafplicatie/sponsor/{$sponsor->id}")
            ->assertStatus(200);
        $this->assertCount(0, Sponsor::withTrashed()->get());
    }

    /**
     * Test if a sponsor can be completely removed if it is soft deleted.
     *
     * @test
     *
     * @return void
     *
     * @throws Exception
     * @throws Exception
     */
    public function a_sponsor_can_only_be_force_deleted_when_soft_deleted(): void
    {
        $this->withExceptionHandling();
        $sponsor = create(Sponsor::class);
        $sponsor->delete();
        $this->signInWithPermissions('Delete Sponsor');

        $this->delete("/stafplicatie/sponsor/{$sponsor->id}")
            ->assertStatus(200);
        $this->assertCount(0, Sponsor::withTrashed()->get());

        $sponsor = create(Sponsor::class);
        $this->delete("/stafplicatie/sponsor/{$sponsor->id}")
            ->assertStatus(404);
        $this->assertCount(1, Sponsor::withTrashed()->get());
    }
}
