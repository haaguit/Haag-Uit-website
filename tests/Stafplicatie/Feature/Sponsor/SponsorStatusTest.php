<?php

namespace Tests\Stafplicatie\Feature\Sponsor;

use Carbon\Carbon;
use HUplicatie\Sponsor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class SponsorStatusTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if a sponsor can be activated.
     *
     * @test
     *
     * @return void
     */
    public function a_sponsor_can_be_activated(): void
    {
        $sponsor = create(Sponsor::class, ['deleted_at' => Carbon::now()]);
        $this->assertTrue($sponsor->trashed());

        $this->withExceptionHandling();
        $this->post("/stafplicatie/api/sponsor/{$sponsor->id}/activate")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->post("/stafplicatie/api/sponsor/{$sponsor->id}/activate")
            ->assertSee('403');

        $this->signInWithPermissions('Edit Sponsor');
        $this->post("/stafplicatie/api/sponsor/{$sponsor->id}/activate")
            ->assertStatus(200);

        $sponsor->refresh();
        $this->assertFalse($sponsor->trashed());
    }

    /**
     * Test if a sponsor can be deactivated.
     *
     * @test
     *
     * @return void
     */
    public function a_sponsor_can_be_deactivated(): void
    {
        $sponsor = create(Sponsor::class);
        $this->assertFalse($sponsor->trashed());

        $this->withExceptionHandling();
        $this->delete("/stafplicatie/api/sponsor/{$sponsor->id}/activate")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->delete("/stafplicatie/api/sponsor/{$sponsor->id}/activate")
            ->assertSee('403');

        $this->signInWithPermissions('Edit Sponsor');
        $this->delete("/stafplicatie/api/sponsor/{$sponsor->id}/activate")
            ->assertStatus(200);

        $sponsor->refresh();
        $this->assertTrue($sponsor->trashed());
    }
}
