<?php

namespace Tests\Stafplicatie\Feature\Sponsor;

use HUplicatie\Sponsor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class EditSponsorTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Storage::fake('public');
        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if a sponsor can be edited.
     *
     * @test
     *
     * @return void
     */
    public function a_sponsor_can_be_edited(): void
    {
        $this->withExceptionHandling();
        $sponsor = create(Sponsor::class);

        $this->get("/stafplicatie/sponsor/{$sponsor->id}/edit")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get("/stafplicatie/sponsor/{$sponsor->id}/edit")
            ->assertSee('403');

        $this->signInWithPermissions('Edit Sponsor');
        $this->get("/stafplicatie/sponsor/{$sponsor->id}/edit")
            ->assertSee(e($sponsor->naam))
            ->assertSee($sponsor->logo);
    }

    /**
     * Test if a sponsor can be updated without a new logo.
     *
     * @test
     *
     * @return void
     */
    public function a_sponsor_can_be_updated_without_a_new_logo(): void
    {
        $this->withExceptionHandling();
        $sponsor = create(Sponsor::class, ['logo' => 'sponsoren/svsim.png']);

        $this->put("/stafplicatie/sponsor/{$sponsor->id}")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->put("/stafplicatie/sponsor/{$sponsor->id}")
            ->assertSee('403');

        $this->signInWithPermissions('Edit Sponsor');
        $this->withExceptionHandling()->json('PUT', "/stafplicatie/sponsor/{$sponsor->id}", [
            'naam' => 'Sv SIM',
            'url'  => 'https://svsim.nl',
        ])->assertSessionDoesntHaveErrors()
            ->assertRedirect('/stafplicatie/sponsor')
            ->assertSessionHas('flash', 'Sponsor gewijzigd');

        $sponsor->refresh();
        $this->assertEquals('Sv SIM', $sponsor->naam);
        $this->assertEquals('https://svsim.nl', $sponsor->url);
        $this->assertEquals(asset('storage/sponsoren/svsim.png'), $sponsor->logo);
    }

    /**
     * Test if a sponsor can be updated with a new logo.
     *
     * @test
     *
     * @return void
     */
    public function a_sponsor_can_be_updated_with_a_new_logo(): void
    {
        $this->withExceptionHandling();
        $sponsor = create(Sponsor::class);

        $this->put("/stafplicatie/sponsor/{$sponsor->id}")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->put("/stafplicatie/sponsor/{$sponsor->id}")
            ->assertSee('403');

        $this->signInWithPermissions('Edit Sponsor');
        $this->withExceptionHandling()->json('PUT', "/stafplicatie/sponsor/{$sponsor->id}", [
            'naam' => 'Sv SIM',
            'url'  => 'https://svsim.nl',
            'logo' => $logo = UploadedFile::fake()->image('sim2019logo.png'),
        ])->assertSessionDoesntHaveErrors()
            ->assertRedirect('/stafplicatie/sponsor')
            ->assertSessionHas('flash', 'Sponsor gewijzigd');

        $sponsor->refresh();
        $this->assertEquals('Sv SIM', $sponsor->naam);
        $this->assertEquals('https://svsim.nl', $sponsor->url);
        $this->assertEquals(asset('storage/sponsoren/'.$logo->hashName()), $sponsor->logo);
    }

    /**
     * Test if sponsor updates are validated.
     *
     * @test
     *
     * @return void
     */
    public function updates_are_validated(): void
    {
        $this->withExceptionHandling();
        $sponsor = create(Sponsor::class);

        $this->signInWithPermissions('Edit Sponsor');
        $this->put("/stafplicatie/sponsor/{$sponsor->id}", [])
            ->assertStatus(302)
            ->assertSessionHasErrors(['naam', 'url']);
    }
}
