<?php

namespace Tests\Stafplicatie\Feature\Nieuwsbericht;

use HUplicatie\Nieuwsbericht;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class DeleteNieuwsberichtTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if a nieuwsbericht can be deleted.
     *
     * @test
     *
     * @return void
     */
    public function a_nieuwsbericht_can_be_deleted(): void
    {
        $this->withExceptionHandling();
        $nieuwsbericht = create(Nieuwsbericht::class);

        $this->delete("/stafplicatie/nieuwsbericht/{$nieuwsbericht->id}")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->delete("/stafplicatie/nieuwsbericht/{$nieuwsbericht->id}")
            ->assertSee('403');

        $this->signInWithPermissions('Delete Nieuwsbericht');
        $this->json('DELETE', "/stafplicatie/nieuwsbericht/{$nieuwsbericht->id}")
            ->assertStatus(204);

        $nieuwsbericht2 = create(Nieuwsbericht::class);
        $this->delete("/stafplicatie/nieuwsbericht/{$nieuwsbericht2->id}")
            ->assertRedirect('/stafplicatie/nieuwsbericht');
    }
}
