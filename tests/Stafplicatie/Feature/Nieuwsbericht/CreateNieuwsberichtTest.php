<?php

namespace Tests\Stafplicatie\Feature\Nieuwsbericht;

use HUplicatie\Nieuwsbericht;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class CreateNieuwsberichtTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if a form can be shown to create a nieuwsbericht.
     *
     * @test
     *
     * @return void
     */
    public function a_create_nieuwsbericht_form_can_be_shown(): void
    {
        $this->withExceptionHandling();

        $this->get('/stafplicatie/nieuwsbericht/create')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get('/stafplicatie/nieuwsbericht/create')
            ->assertSee('403');

        $this->signInWithPermissions('Create Nieuwsbericht');
        $this->get('/stafplicatie/nieuwsbericht/create')
            ->assertOk()
            ->assertSee('Nieuwsbericht publiceren');
    }

    /**
     * Test if a nieuwsbericht can be created.
     *
     * @test
     *
     * @return void
     */
    public function a_nieuwsbericht_can_be_created(): void
    {
        $this->withoutExceptionHandling();
        $this->assertEquals(0, Nieuwsbericht::count());
        $this->signInWithPermissions('Create Nieuwsbericht');
        $response = $this->post('/stafplicatie/nieuwsbericht', [
            'titel'              => 'Op naar Haag Uit 2250',
            'tekst'              => 'We zijn druk bezig met de voorbereidingen voor Haag Uit 2250',
        ]);

        $response->assertSessionHasNoErrors()
            ->assertRedirect('/stafplicatie/nieuwsbericht')
            ->assertSessionHas('flash', 'Nieuwsbericht gepubliceerd');

        $this->assertDatabaseHas('Nieuwsberichten', [
            'titel'              => 'Op naar Haag Uit 2250',
            'tekst'              => 'We zijn druk bezig met de voorbereidingen voor Haag Uit 2250',
        ]);
        $this->assertEquals(1, Nieuwsbericht::count());
    }

    /**
     * Test is creation of nieuwsberichten are validated.
     *
     * @test
     *
     * @return void
     */
    public function nieuwsbericht_creations_are_validated(): void
    {
        $this->withExceptionHandling();
        $this->assertEquals(0, Nieuwsbericht::count());
        $this->signInWithPermissions('Create Nieuwsbericht');
        $this->post('/stafplicatie/nieuwsbericht', [])
            ->assertSessionHasErrors(['titel', 'tekst']);

        $this->assertEquals(0, Nieuwsbericht::count());
    }
}
