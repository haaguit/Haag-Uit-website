<?php

namespace Tests\Stafplicatie\Feature\Nieuwsbericht;

use HUplicatie\Nieuwsbericht;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class ReadNieuwsberichtTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if all Nieuwsberichten can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function all_nieuwsberichten_can_be_viewed(): void
    {
        $this->withExceptionHandling();
        $this->get('/stafplicatie/nieuwsbericht')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get('/stafplicatie/nieuwsbericht')
            ->assertSee('403');

        $this->signInWithPermissions('View Nieuwsberichten');

        $nieuwsbericht = create(Nieuwsbericht::class);
        $nieuwsbericht2 = create(Nieuwsbericht::class);
        $response = $this->get('/stafplicatie/nieuwsbericht');

        $response->assertSee($nieuwsbericht->titel)
            ->assertSee($nieuwsbericht->created_at->format('Y-m-d H:i:s'));

        $response->assertSee($nieuwsbericht2->titel)
            ->assertSee($nieuwsbericht2->created_at->format('Y-m-d H:i:s'));
    }

    /**
     * Test if a Nieuwsbericht can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function a_nieuwsbericht_can_be_viewed(): void
    {
        $this->withExceptionHandling();
        $nieuwsbericht = create(Nieuwsbericht::class);
        $this->get("/stafplicatie/nieuwsbericht/{$nieuwsbericht->id}")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get("/stafplicatie/nieuwsbericht/{$nieuwsbericht->id}")
            ->assertSee('403');

        $this->signInWithPermissions('View Nieuwsbericht');
        $this->get("/stafplicatie/nieuwsbericht/{$nieuwsbericht->id}")
            ->assertSee($nieuwsbericht->titel);
    }
}
