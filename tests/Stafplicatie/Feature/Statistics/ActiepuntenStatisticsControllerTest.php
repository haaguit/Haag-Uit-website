<?php

namespace Tests\Stafplicatie\Feature\Statistics;

use Carbon\Carbon;
use HUplicatie\Actiepunt;
use HUplicatie\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\Setup\KampjaarFactory;
use Tests\TestCase;

class ActiepuntenStatisticsControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test actiepunten statistics can be requested.
     *
     * @test
     *
     * @return void
     */
    public function actiepunten_statistics_can_be_requested(): void
    {
        $user = create(User::class);
        $this->signInWithPermissions('View Dashboard', $user);

        app(KampjaarFactory::class)->isActief()->create(2019);
        create(Actiepunt::class, ['jaar' => 2019], 5);
        create(Actiepunt::class, ['jaar' => 2019, 'deleted_at' => Carbon::now()], 3);
        $assigned = create(Actiepunt::class, ['jaar' => 2019], 3);
        $assigned->each(function ($ap) use ($user) {
            $ap->users()->sync($user->id);
        });
        $response = $this->get('/stafplicatie/api/statistics/2019/actiepunten');
        $response->assertJsonFragment(['total' => 11]);
        $response->assertJsonFragment(['done' => 3]);
        $this->assertEquals($assigned->pluck('beschrijving')->toArray(), $response->json('users'));
    }
}
