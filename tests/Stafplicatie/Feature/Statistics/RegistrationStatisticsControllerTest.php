<?php

namespace Tests\Stafplicatie\Feature\Statistics;

use HUplicatie\KampInschrijving;
use HUplicatie\Kampjaar;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class RegistrationStatisticsControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if registration statistics can be requested?
     *
     * @test
     *
     * @return void
     */
    public function registration_statistics_can_be_requested(): void
    {
        $this->signInWithPermissions('View Dashboard');

        create(Kampjaar::class, ['actief' => 1, 'jaar' => 2019, 'eind' => '2019-08-25']);
        create(KampInschrijving::class, ['jaar' => 2019, 'created_at' => '2019-05-10 10:00:00'], 12);
        create(KampInschrijving::class, ['jaar' => 2019, 'created_at' => '2019-06-10 10:00:00'], 5);
        create(KampInschrijving::class, ['jaar' => 2019, 'created_at' => '2019-07-10 10:00:00'], 8);
        create(KampInschrijving::class, ['jaar' => 2019, 'created_at' => '2019-08-10 10:00:00'], 6);
        $response = $this->get('/stafplicatie/api/statistics/2019/registrations');
        $response->assertJsonFragment(['total' => 31]);
        $response->assertJsonFragment(['year' => '2019']);
        $response->assertJsonFragment([
            'monthly' => [
                '01' => 0,
                '02' => 0,
                '03' => 0,
                '04' => 0,
                '05' => 12,
                '06' => 17,
                '07' => 25,
                '08' => 31,
            ],
        ]);
    }
}
