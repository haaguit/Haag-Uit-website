<?php

namespace Tests\Stafplicatie\Feature\Kampjaar;

use HUplicatie\Kampjaar;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class DeleteKampjaarTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if a kampjaar can be deleted.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_be_deleted(): void
    {
        $this->withExceptionHandling();
        $kampjaar = create(Kampjaar::class, ['jaar' => 2018]);

        $this->delete("/stafplicatie/kampjaar/{$kampjaar->jaar}")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->delete("/stafplicatie/kampjaar/{$kampjaar->jaar}")
            ->assertSee('403');

        $this->signInWithPermissions('Delete Kampjaar');
        $this->json('DELETE', "/stafplicatie/kampjaar/{$kampjaar->jaar}")
            ->assertStatus(204);

        $kampjaar = create(Kampjaar::class, ['jaar' => 2018]);
        $this->delete("/stafplicatie/kampjaar/{$kampjaar->jaar}")
            ->assertRedirect('/stafplicatie/kampjaar');
    }

    /**
     * Test if an active kampjaar can not be deleted.
     *
     * @test
     *
     * @return void
     */
    public function an_active_kampjaar_can_not_be_deleted(): void
    {
        $this->withExceptionHandling();
        $kampjaar = create(Kampjaar::class, ['jaar' => 2018, 'actief' => 1]);

        $this->signInWithPermissions('Delete Kampjaar');

        $this->json('DELETE', "/stafplicatie/kampjaar/{$kampjaar->jaar}")
            ->assertStatus(400)
            ->assertSeeText('Een actief kampjaar mag niet verwijderd worden');
    }
}
