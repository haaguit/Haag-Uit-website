<?php

namespace Tests\Stafplicatie\Feature\Users;

use HUplicatie\Authorization\Roles;
use HUplicatie\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if a form can be shown to create a user.
     *
     * @test
     *
     * @return void
     */
    public function a_create_user_form_can_be_shown(): void
    {
        $this->withExceptionHandling();

        $this->get('/stafplicatie/user/create')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get('/stafplicatie/user/create')
            ->assertSee('403');

        $this->signInWithPermissions('Create User');
        $this->get('/stafplicatie/user/create')
            ->assertOk()
            ->assertSee('Gebruiker aanmaken')
            ->assertDontSee('Webmeester');
    }

    /**
     * Test if a user can be created.
     *
     * @test
     *
     * @return void
     */
    public function a_user_can_be_created(): void
    {
        $this->withExceptionHandling();
        $this->assertEquals(0, User::role(Roles::STAFFER)->count());
        $this->signInWithPermissions('Create User');
        $response = $this->post('/stafplicatie/user', [
            'name'                  => 'Test name',
            'email'                 => 'test@example.com',
            'password'              => 'testtest',
            'password_confirmation' => 'testtest',
            'role'                  => 'Staffer',
        ]);
        $response->assertSessionHasNoErrors()
            ->assertRedirect('/stafplicatie/user')
            ->assertSessionHas('flash', 'Gebruiker aangemaakt');

        $this->assertDatabaseHas('Users', ['name' => 'Test name', 'email' => 'test@example.com']);
        $this->assertDatabaseMissing('Users', ['password' => 'testtest']);
        $this->assertEquals(1, User::role(Roles::STAFFER)->count());
    }

    /**
     * Test if creations are validated.
     *
     * @test
     *
     * @return void
     */
    public function creations_are_validated(): void
    {
        $this->withExceptionHandling();
        $this->assertEquals(0, User::role(Roles::STAFFER)->count());
        $this->signInWithPermissions('Create User');
        $this->post('/stafplicatie/user', [])
            ->assertSessionHasErrors(['name', 'email', 'password', 'role']);

        $this->assertDatabaseMissing('Users', ['name' => 'Test name', 'email' => 'test@example.com']);
        $this->assertEquals(0, User::role(Roles::STAFFER)->count());
    }

    /**
     * Test if users cannot be created with the role webmeester.
     *
     * @test
     *
     * @return void
     */
    public function users_cannot_be_created_with_role_webmeester(): void
    {
        $this->withExceptionHandling();
        $this->assertEquals(1, User::role(Roles::WEBMEESTER)->count());
        $this->signInWithPermissions('Create User');
        $this->post(
            '/stafplicatie/user',
            ['name' => 'Test name', 'email' => 'test@example.com', 'password' => 'test', 'role' => Roles::WEBMEESTER]
        )
            ->assertSessionHasErrors('role');

        $this->assertDatabaseMissing('Users', ['name' => 'Test name', 'email' => 'test@example.com']);
        $this->assertEquals(1, User::role(Roles::WEBMEESTER)->count());
    }
}
