<?php

namespace Tests\Stafplicatie\Feature\Inschrijving;

use HUplicatie\KampInschrijving;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\Setup\KampjaarFactory;
use Tests\TestCase;

class ReadInschrijvingControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if all inschrijvingen can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function all_inschrijvingen_can_be_viewed(): void
    {
        $this->withExceptionHandling();
        $this->get('/stafplicatie/inschrijving')
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get('/stafplicatie/inschrijving')
            ->assertSee('403');

        $this->signInWithPermissions('View Inschrijvingen');
        $kampjaar = app(KampjaarFactory::class)->withInschrijvingen(40)->isActief()->create(2019);
        $kampjaarOud = app(KampjaarFactory::class)->withInschrijvingen(40)->create(2018);

        $response = $this->get('/stafplicatie/inschrijving');

        $response->assertSee($kampjaar->jaar)
            ->assertDontSee($kampjaarOud->jaar);
    }

    /**
     * Test if an inschrijving can be viewed.
     *
     * @test
     *
     * @return void
     */
    public function an_inschrijving_can_be_viewed(): void
    {
        $kampjaar = app(KampjaarFactory::class)->withInschrijvingen(1)->isActief()->create(2019);
        $inschrijving = $kampjaar->kampInschrijvingen()->first();

        $this->withExceptionHandling();
        $this->get("/stafplicatie/inschrijving/{$inschrijving->id}")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->get("/stafplicatie/inschrijving/{$inschrijving->id}")
            ->assertSee('403');

        $this->signInWithPermissions('View Inschrijving');
        $this->get("/stafplicatie/inschrijving/{$inschrijving->id}")
            ->assertSee(e($inschrijving->persoon->voornaam))
            ->assertSee($inschrijving->persoon->email);
    }

    /**
     * Test if an inschrijving can be edited.
     *
     * @test
     *
     * @return void
     */
    public function an_inschrijving_can_be_edited(): void
    {
        $kampjaar = app(KampjaarFactory::class)->isActief()->create(2019);
        $inschrijving = create(KampInschrijving::class, ['kledingmaat' => 'S']);

        $this->withExceptionHandling();
        $this->put("/stafplicatie/inschrijving/{$inschrijving->id}")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->put("/stafplicatie/inschrijving/{$inschrijving->id}")
            ->assertSee('403');

        $this->signInWithPermissions('Edit Inschrijving');
        $this->put("/stafplicatie/inschrijving/{$inschrijving->id}", [
            'kledingmaat' => 'L',
        ]);

        $this->assertDatabaseHas('kamp_inschrijvingen', ['id' => $inschrijving->id, 'kledingmaat' => 'L']);
    }
}
