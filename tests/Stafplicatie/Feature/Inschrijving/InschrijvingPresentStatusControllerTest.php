<?php

namespace Tests\Stafplicatie\Feature\Inschrijving;

use HUplicatie\KampInschrijving;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\TestCase;

class InschrijvingPresentStatusControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if an unauthenticated user cannot change the present status of an inschrijving.
     *
     * @test
     *
     * @return void
     */
    public function an_unauthenticated_user_can_not_change_present_status(): void
    {
        $inschrijving = create(KampInschrijving::class);
        $this->withExceptionHandling();
        $this->postJson("/stafplicatie/api/inschrijving/{$inschrijving->id}/open")
            ->assertStatus(401);

        $this->postJson("/stafplicatie/api/inschrijving/{$inschrijving->id}/unregister")
            ->assertStatus(401);
    }

    /**
     * Test if a user cannot change the present status without the right permission.
     *
     * @test
     *
     * @return void
     */
    public function an_user_can_not_change_present_status_without_the_right_permission(): void
    {
        $inschrijving = create(KampInschrijving::class);
        $this->withExceptionHandling();
        $this->signIn();
        $this->postJson("/stafplicatie/api/inschrijving/{$inschrijving->id}/open")
            ->assertStatus(403);
        $this->postJson("/stafplicatie/api/inschrijving/{$inschrijving->id}/unregister")
            ->assertStatus(403);
    }

    /**
     * Test if an inschrijving can be marked as unregistered.
     *
     * @test
     *
     * @return void
     */
    public function an_inschrijving_can_be_marked_as_unregistered(): void
    {
        $inschrijving = create(KampInschrijving::class, ['presentie' => 'open']);
        $this->signInWithPermissions('Edit Inschrijving');

        $response = $this->postJson("/stafplicatie/api/inschrijving/{$inschrijving->id}/unregister");
        $response->assertStatus(200);

        $this->assertEquals('afgemeld', $inschrijving->fresh()->presentie);
    }

    /**
     * Test if an inschrijving van be marked as open.
     *
     * @test
     *
     * @return void
     */
    public function an_inschrijving_can_be_marked_as_open(): void
    {
        $inschrijving = create(KampInschrijving::class, ['presentie' => 'afwezig']);
        $this->signInWithPermissions('Edit Inschrijving');

        $response = $this->postJson("/stafplicatie/api/inschrijving/{$inschrijving->id}/open");
        $response->assertStatus(200);

        $this->assertEquals('open', $inschrijving->fresh()->presentie);
    }
}
