<?php

namespace Tests\Stafplicatie\Feature\Inschrijving;

use HUplicatie\KampInschrijving;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\PermissionRegistrar;
use Tests\Setup\KampjaarFactory;
use Tests\TestCase;

class EditInschrijvingControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->app->make(PermissionRegistrar::class)->registerPermissions();
    }

    /**
     * Test if an inschrijving can be edited.
     *
     * @test
     *
     * @return void
     */
    public function an_inschrijving_can_be_edited(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);
        $inschrijving = create(KampInschrijving::class, ['kledingmaat' => 'S']);

        $this->withExceptionHandling();
        $this->put("/stafplicatie/inschrijving/{$inschrijving->id}")
            ->assertRedirect('/stafplicatie/login');

        $this->signIn();
        $this->put("/stafplicatie/inschrijving/{$inschrijving->id}")
            ->assertSee('403');

        $this->signInWithPermissions('Edit Inschrijving');
        $response = $this->put("/stafplicatie/inschrijving/{$inschrijving->id}", [
            'kledingmaat' => 'L',
        ]);

        $response->assertStatus(200)
            ->assertJsonFragment(['id' => $inschrijving->id, 'kledingmaat' => 'L']);

        $this->assertDatabaseHas('kamp_inschrijvingen', ['id' => $inschrijving->id, 'kledingmaat' => 'L']);
    }
}
