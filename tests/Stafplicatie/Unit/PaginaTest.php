<?php

namespace Tests\Stafplicatie\Unit;

use HUplicatie\Pagina;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Setup\KampjaarFactory;
use Tests\TestCase;

class PaginaTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if a pagina can replace variables in its content.
     *
     * @test
     *
     * @return void
     */
    public function a_pagina_can_replace_variables_in_its_content(): void
    {
        app(KampjaarFactory::class)->isActief()->create(2019);
        $pagina = create(Pagina::class, ['tekst' => 'Hallo welkom bij $JAAR$']);

        $this->assertEquals('Hallo welkom bij $JAAR$', $pagina->tekst);
        $this->assertEquals('Hallo welkom bij 2019', $pagina->withVariableReplacements()->tekst);
    }
}
