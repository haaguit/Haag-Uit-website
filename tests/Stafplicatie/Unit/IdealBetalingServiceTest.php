<?php

namespace Tests\Stafplicatie\Unit;

use GuzzleHttp\Psr7\Response;
use HUplicatie\KampInschrijving;
use HUplicatie\Kampjaar;
use HUplicatie\Services\Betalingen\IdealBetalingService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\WorksWithMollie;

class IdealBetalingServiceTest extends TestCase
{
    use WorksWithMollie, RefreshDatabase;

    private $responseStack;

    protected function setUp(): void
    {
        parent::setUp();
        $this->responseStack = $this->mockMollie();
    }

    /**
     * Test if a betaling can be created.
     *
     * @test
     */
    public function a_betaling_can_be_created(): void
    {
        $id = 'tr_123456789';
        $description = 'Betaling Stichting Haag Uit 2018';
        $amount = '100.00';
        $paymentUrl = 'https://mollie.nl/betaling/1234';
        $this->responseStack->append(new Response(200, [], json_encode([
            'id'          => $id,
            'amount'      => ['currency' => 'EUR', 'value' => $amount],
            'description' => $description,
            '_links'      => [
                'checkout' => [
                    'href' => $paymentUrl,
                    'type' => 'text/html',
                ],
            ],
        ])));

        $kampjaar = create(Kampjaar::class);
        $inschrijving = create(KampInschrijving::class, ['jaar' => $kampjaar->jaar]);
        $betalingsService = new IdealBetalingService($inschrijving);
        $betaling = $betalingsService->processBetaling($amount);

        $this->assertCount(1, $this->mollieRequestHistory);
        $this->assertEquals($id, $betaling->transactie_id);
        $this->assertEquals($amount, $betaling->bedrag);
        $this->assertEquals($paymentUrl, $betaling->paymentUrl);
    }
}
