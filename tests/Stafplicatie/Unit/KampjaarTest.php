<?php

namespace Tests\Stafplicatie\Unit;

use HUplicatie\Actiepunt;
use HUplicatie\KampInschrijving;
use HUplicatie\Kampjaar;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class KampjaarTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test that a kampjaar can have an inschrijving.
     *
     * @test
     */
    public function an_kampjaar_can_have_an_inschrijving(): void
    {
        $kampjaar = create(Kampjaar::class);
        create(KampInschrijving::class, ['jaar' => $kampjaar->jaar]);
        $this->assertInstanceOf(Collection::class, $kampjaar->kampInschrijvingen);
        $this->assertCount(1, $kampjaar->kampInschrijvingen);
    }

    /**
     * Test if an kampjaar can have actiepunten.
     *
     * @test
     */
    public function an_kampjaar_can_have_actiepunten(): void
    {
        $kampjaar = create(Kampjaar::class);
        create(Actiepunt::class, ['jaar' => $kampjaar->jaar], 30);
        $this->assertInstanceOf(Collection::class, $kampjaar->actiepunten);
        $this->assertCount(30, $kampjaar->actiepunten);
    }

    /**
     * Test if an active kampjaar can be found.
     *
     * @test
     */
    public function an_active_kampjaar_can_be_found(): void
    {
        $active = create(Kampjaar::class, ['jaar' => 2019, 'actief' => 1]);
        create(Kampjaar::class, ['jaar' => 2018, 'actief' => 0]);

        $result = Kampjaar::getActiveKampjaar();
        $this->assertEquals($active->jaar, $result->jaar);
    }

    /**
     * Test if an open kampjaar can be found.
     *
     * @test
     */
    public function an_open_kampjaar_can_be_found(): void
    {
        $open = create(Kampjaar::class, ['jaar' => 2013, 'actief' => 1, 'open' => 1]);
        $nonActiveOpen = create(Kampjaar::class, ['jaar' => 2014, 'actief' => 0, 'open' => 1]);
        $activeNotOpen = create(Kampjaar::class, ['jaar' => 2015, 'actief' => 1, 'open' => 0]);
        $nonActive = create(Kampjaar::class, ['jaar' => 2016, 'actief' => 0, 'open' => 0]);

        $result = Kampjaar::getOpenKampjaar();
        $this->assertEquals($open->jaar, $result->jaar);
        $this->assertNotEquals($nonActiveOpen->jaar, $result->jaar);
        $this->assertNotEquals($activeNotOpen->jaar, $result->jaar);
        $this->assertNotEquals($nonActive->jaar, $result->jaar);
    }

    /**
     * Test if the year can be retrieved for the open and active kampjaar.
     *
     * @test
     */
    public function the_year_can_be_retrieved_for_the_open_and_active_kampjaar(): void
    {
        $open = create(Kampjaar::class, ['jaar' => 2019, 'actief' => 1, 'open' => 1]);
        $nonActiveOpen = create(Kampjaar::class, ['jaar' => 2018, 'actief' => 0, 'open' => 1]);
        $activeNotOpen = create(Kampjaar::class, ['jaar' => 2017, 'actief' => 1, 'open' => 0]);
        $nonActive = create(Kampjaar::class, ['jaar' => 2016, 'actief' => 0, 'open' => 0]);

        $result = Kampjaar::getOpenKampjaarYear();
        $this->assertEquals($open->jaar, $result);
        $this->assertNotEquals($nonActiveOpen->jaar, $result);
        $this->assertNotEquals($activeNotOpen->jaar, $result);
        $this->assertNotEquals($nonActive->jaar, $result);
    }

    /**
     * Test if a kampjaar can return the amount including insurance.
     *
     * @test
     */
    public function a_kampjaar_can_return_the_amount_including_insurance(): void
    {
        $kampjaar = create(Kampjaar::class, ['inschrijfbedrag' => 100.00, 'verzekeringbedrag' => 5.00]);

        $this->assertIsFloat($kampjaar->inschrijfbedrag);
        $this->assertIsFloat($kampjaar->verzekeringbedrag);
        $this->assertIsString($kampjaar->getAmountWithInsurance());
        $this->assertEquals('105.00', $kampjaar->getAmountWithInsurance());
    }

    /**
     * Test if a kampjaar can return the amount excluding insurance.
     *
     * @test
     */
    public function a_kampjaar_can_return_the_amount_excluding_insurance(): void
    {
        $kampjaar = create(Kampjaar::class, ['inschrijfbedrag' => 100.00, 'verzekeringbedrag' => 5.00]);

        $this->assertIsFloat($kampjaar->inschrijfbedrag);
        $this->assertIsFloat($kampjaar->verzekeringbedrag);
        $this->assertIsString($kampjaar->getAmountWithoutInsurance());
        $this->assertSame('100.00', $kampjaar->getAmountWithoutInsurance());
    }

    /**
     * Test if a kampjaar can determine its logo path.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_determine_its_logo_path(): void
    {
        $kampjaar = create(Kampjaar::class);

        $this->assertEquals(asset('/assets/website/img/hu_logo_default.png'), $kampjaar->logo);

        $kampjaar->logo = 'logos/hu_2020_logo.png';

        $this->assertEquals(asset('/storage/logos/hu_2020_logo.png'), $kampjaar->logo);
    }

    /**
     * Test if a kampjaar can be activated.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_be_activated(): void
    {
        $kampjaar = create(Kampjaar::class, ['actief' => 0]);

        $this->assertFalse($kampjaar->actief);

        $kampjaar->activate();

        $this->assertTrue($kampjaar->actief);
    }

    /**
     * Test if a kampjaar can be opened.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_be_opened(): void
    {
        $kampjaar = create(Kampjaar::class, ['actief' => 1, 'open' => 0]);

        $this->assertFalse($kampjaar->open);

        $kampjaar->open();

        $this->assertTrue($kampjaar->open);
    }

    /**
     * Test if a kampjaar can be closed.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_be_closed(): void
    {
        $kampjaar = create(Kampjaar::class, ['actief' => 1, 'open' => 1]);

        $this->assertTrue($kampjaar->open);

        $kampjaar->close();

        $this->assertFalse($kampjaar->open);
    }

    /**
     * Test if a kampjaar cannot be opened when not active.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_can_not_be_opened_when_not_active(): void
    {
        $kampjaar = create(Kampjaar::class, ['actief' => 0, 'open' => 0]);

        $this->assertFalse($kampjaar->open);

        $kampjaar->open();

        $this->assertFalse($kampjaar->open);
    }

    /**
     * Test if a kampjaar gets closed when deactivated.
     *
     * @test
     *
     * @return void
     */
    public function a_kampjaar_will_be_closed_when_deactivated(): void
    {
        $kampjaar = create(Kampjaar::class, ['jaar' => 2000, 'actief' => 1, 'open' => 1]);

        $this->assertTrue($kampjaar->actief);
        $this->assertTrue($kampjaar->open);

        $newKampjaar = create(Kampjaar::class, ['jaar' => 1999, 'actief' => 0, 'open' => 0]);
        $newKampjaar->activate();

        $kampjaar->refresh();
        $this->assertFalse($kampjaar->actief);
        $this->assertFalse($kampjaar->open);
        $this->assertTrue($newKampjaar->actief);
    }
}
