<?php

namespace Tests\Stafplicatie\Unit;

use HUplicatie\Actiepunt;
use HUplicatie\Kampjaar;
use HUplicatie\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Tests\Setup\KampjaarFactory;
use Tests\TestCase;

class ActiepuntTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if an actiepunt belongs to a kampjaar.
     *
     * @test
     *
     * @return void
     */
    public function an_actiepunt_belongs_to_a_kampjaar(): void
    {
        $kampjaar = app(KampjaarFactory::class)->create(2019);
        $actiepunt = factory(Actiepunt::class)->create(['jaar' => $kampjaar->jaar]);
        $this->assertInstanceOf(Kampjaar::class, $actiepunt->kampjaar);
        $this->assertEquals($kampjaar->jaar, $actiepunt->kampjaar->jaar);
    }

    /**
     * Test if an actiepunt belongs to one or more users.
     *
     * @test
     *
     * @return void
     */
    public function an_actiepunt_belongs_to_one_or_more_users(): void
    {
        $voorzitter = create(User::class);
        $penningmeester = create(User::class);
        $kampjaar = app(KampjaarFactory::class)->create(2019);
        $actiepunt = create(Actiepunt::class, ['jaar' => $kampjaar->jaar]);

        $actiepunt->users()->sync([$voorzitter->id, $penningmeester->id]);

        $this->assertCount(2, $actiepunt->fresh()->users);

        $actiepunt->users()->sync($voorzitter);
        $this->assertCount(1, $actiepunt->fresh()->users);
    }

    /**
     * Test if all actiepunten can be retrieved for the active kampjaar.
     *
     * @test
     *
     * @return void
     */
    public function all_actiepunten_can_be_retrieved_for_active_kampjaar(): void
    {
        $actiefJaar = app(KampjaarFactory::class)->isActief()->create(2019);
        $inactiefJaar = app(KampjaarFactory::class)->create(2000);

        create(Actiepunt::class, ['jaar' => $actiefJaar->jaar], 20);
        create(Actiepunt::class, ['jaar' => $inactiefJaar->jaar], 10);

        $this->assertCount(20, Actiepunt::activeYear()->get());
        $this->assertCount(30, Actiepunt::all());
    }

    /**
     * Test if an actiepunt is loaded with an array of assigned users.
     *
     * @test
     *
     * @return void
     */
    public function an_actiepunt_can_be_loaded_with_an_array_of_assigned_users(): void
    {
        $voorzitter = create(User::class);
        $penningmeester = create(User::class);
        $kampjaar = app(KampjaarFactory::class)->create(2019);
        $actiepunt = create(Actiepunt::class, ['jaar' => $kampjaar->jaar]);

        $actiepunt->users()->sync([$voorzitter->id, $penningmeester->id]);

        $this->assertCount(2, $actiepunt->fresh()->users);

        $this->assertInstanceOf(Collection::class, $actiepunt->assignedUsers);
        $this->assertCount(2, $actiepunt->assignedUsers);
        $this->assertTrue($actiepunt->assignedUsers->contains($voorzitter));
        $this->assertTrue($actiepunt->assignedUsers->contains($penningmeester));
    }
}
