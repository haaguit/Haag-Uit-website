<?php

namespace Tests\Stafplicatie\Unit;

use function create;
use HUplicatie\Aanwezigheid;
use HUplicatie\Functie;
use HUplicatie\Medewerker;
use HUplicatie\Persoon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MedewerkerTest extends TestCase
{
    use RefreshDatabase;

    private $medewerker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->medewerker = create(Medewerker::class);
    }

    /**
     * Test if a medewerker has a Persoon.
     *
     * @test
     */
    public function a_medewerker_has_a_persoon(): void
    {
        $this->assertInstanceOf(Persoon::class, $this->medewerker->persoonsgegevens);
    }

    /**
     * Test if a medewerker has an Eerste Keus.
     *
     * @test
     */
    public function a_medewerker_has_an_eerste_keus(): void
    {
        $this->medewerker->eerste_keus()->attach(Functie::inRandomOrder()->first());
        $this->assertInstanceOf(Functie::class, $this->medewerker->eerste_keus()->first());
    }

    /**
     * Test if a medewerker has a Tweede Keus.
     *
     * @test
     */
    public function a_medewerker_has_a_tweede_keus(): void
    {
        $this->medewerker->tweede_keus()->attach(Functie::inRandomOrder()->first());
        $this->assertInstanceOf(Functie::class, $this->medewerker->tweede_keus()->first());
    }

    /**
     * Test if a medewerker has Aanwezigheid.
     *
     * @test
     */
    public function a_medewerker_has_aanwezigeheden(): void
    {
        $this->assertInstanceOf(Collection::class, $this->medewerker->aanwezigheid);
        $this->assertInstanceOf(Aanwezigheid::class, $this->medewerker->aanwezigheid()->first());
    }

    /**
     * Test if creating a medewerker creates Aanwezigheid.
     *
     * @test
     */
    public function creating_a_medewerker_creates_aanwezigheden(): void
    {
        $medewerker = create(Medewerker::class);
        $this->assertCount(5, $medewerker->aanwezigheid);
    }

    /**
     * Test if deleting a medewerker deletes everything.
     *
     * @test
     */
    public function deleting_a_medewerker_deletes_everything(): void
    {
        $medewerker = create(Medewerker::class);
        $medewerker->eerste_keus()->attach(Functie::inRandomOrder()->first());
        $medewerker->tweede_keus()->attach(Functie::inRandomOrder()->first());
        $this->assertCount(5, $medewerker->aanwezigheid);
        $this->assertDatabaseHas('personen', ['id' => $medewerker->persoonsgegevens->id]);
        $this->assertDatabaseHas('eerste_keus', ['medewerker_id' => $medewerker->id]);
        $this->assertDatabaseHas('tweede_keus', ['medewerker_id' => $medewerker->id]);

        $medewerker->delete();

        $this->assertDatabaseMissing('medewerkers', ['id' => $medewerker->id]);
        $this->assertDatabaseMissing('aanwezigheden', ['medewerker_id' => $medewerker->id]);
        $this->assertDatabaseMissing('personen', ['id' => $medewerker->persoonsgegevens->id]);
        $this->assertDatabaseMissing('eerste_keus', ['medewerker_id' => $medewerker->id]);
        $this->assertDatabaseMissing('tweede_keus', ['medewerker_id' => $medewerker->id]);
    }
}
