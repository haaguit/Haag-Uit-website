<?php

namespace Tests\Website\Feature;

use Carbon\Carbon;
use HUplicatie\Nieuwsbericht;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NieuwsControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if nieuwsberichten can be displayed with pagination.
     *
     * @test
     */
    public function news_articles_can_be_displayed_with_pagination(): void
    {
        $newest = create(Nieuwsbericht::class);
        $older = create(Nieuwsbericht::class, ['created_at' => Carbon::now()->subDay()], 5);

        $response = $this->get('/nieuws')
            ->assertStatus(200);

        $response->assertSee($newest->titel);
        $response->assertDontSee($older->last()->titel);
        $response->assertSee($newest->previewText());
    }

    /**
     * Test if a Nieuwsbericht can be read.
     *
     * @test
     */
    public function a_news_article_can_be_read(): void
    {
        $news = create(Nieuwsbericht::class);

        $response = $this->get("/nieuws/{$news->id}")
            ->assertStatus(200);

        $response->assertSee($news->titel);
    }
}
