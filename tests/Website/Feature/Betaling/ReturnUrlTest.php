<?php

namespace Tests\Website\Feature\Betaling;

use HUplicatie\Betaling;
use HUplicatie\KampInschrijving;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mollie\Api\Exceptions\IncompatiblePlatform;
use Mollie\Api\Types\PaymentStatus;
use Tests\TestCase;
use Tests\utilities\MollieMockResponseFactory;
use Tests\WorksWithMollie;

class ReturnUrlTest extends TestCase
{
    use RefreshDatabase, WorksWithMollie;

    private $inschrijving;

    protected function setUp(): void
    {
        parent::setUp();
        $this->inschrijving = create(KampInschrijving::class, ['id' => 1]);
    }

    /**
     * Test if a request without an id returns a 404.
     *
     * @test
     *
     * @return void
     */
    public function a_request_returns_404_without_an_id(): void
    {
        $this->get('/inschrijven/betaald')
            ->assertRedirect('/');
    }

    /**
     * Test if a request redirects for an incasso.
     *
     * @test
     *
     * @return void
     */
    public function a_request_redirects_for_an_incasso(): void
    {
        create(Betaling::class, [
            'type'                 => 'incasso',
            'transactie_id'        => '201902261938471',
            'status'               => 'open',
            'kamp_inschrijving_id' => $this->inschrijving,
        ]);

        $this->get(route('returnUrl', ['id' => 1]))
            ->assertRedirect('/');
    }

    /**
     * Test if the return page displays successfully after a correct payment.
     *
     * @test
     *
     * @return void
     */
    public function the_return_page_displays_successful_after_correct_payment(): void
    {
        create(Betaling::class, [
            'type'                 => 'ideal',
            'transactie_id'        => 'tr_987979',
            'status'               => PaymentStatus::STATUS_PAID,
            'kamp_inschrijving_id' => $this->inschrijving,
        ]);

        $this->get(route('returnUrl', ['id' => 1]))
            ->assertSeeText('Je betaling is goed gegaan en afgerond')
            ->assertStatus(200);
    }

    /**
     * Test if the return page displays retry after open payment.
     *
     * @test
     *
     * @return void
     *
     * @throws IncompatiblePlatform
     */
    public function the_return_page_displays_retry_after_open_payment(): void
    {
        $responseStack = $this->mockMollie();
        $response = (new MollieMockResponseFactory())
            ->transactieId($transactieId = 'tr_123131')
            ->paymentUrl($paymentUrl = 'https://lekkerbetalen.com')
            ->build();
        $responseStack->append($response);

        create(Betaling::class, [
            'type'                 => 'ideal',
            'transactie_id'        => $transactieId,
            'status'               => PaymentStatus::STATUS_OPEN,
            'kamp_inschrijving_id' => $this->inschrijving,
        ]);

        $this->get(route('returnUrl', ['id' => 1]))
            ->assertSeeText('De betaling is nog niet voltooid')
            ->assertSee($paymentUrl)
            ->assertStatus(200);
    }

    /**
     * Test if the return page displays retry after unsuccessful payment.
     *
     * @test
     *
     * @return void
     */
    public function the_return_page_displays_retry_payment_after_unsuccessful_payment(): void
    {
        $betaling = create(Betaling::class, [
            'type'                 => 'ideal',
            'transactie_id'        => 'tr_987979',
            'status'               => PaymentStatus::STATUS_FAILED,
            'kamp_inschrijving_id' => $this->inschrijving,
        ]);

        $this->get(route('returnUrl', ['id' => 1]))
            ->assertSeeText("(Status: {$betaling->status})")
            ->assertSee(route('retryPayment'))
            ->assertStatus(200);
    }
}
