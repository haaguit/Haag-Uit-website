<?php

namespace Tests\Website\Feature;

use HUplicatie\Sponsor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SponsorFeedControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if all active sponsors can be fetched.
     *
     * @test
     */
    public function all_the_active_sponsors_can_be_fetched(): void
    {
        $sponsors = create(Sponsor::class, [], 10);

        $response = $this->getJson('/api/sponsor/feed');
        $response->assertJsonCount(10);
        $response->assertJson($sponsors->toArray());
    }
}
