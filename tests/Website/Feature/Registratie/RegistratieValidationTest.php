<?php

namespace Tests\Website\Feature\Registratie;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Tests\Setup\KampjaarFactory;
use Tests\TestCase;
use Tests\WorksWithGoogleCaptcha;

class RegistratieValidationTest extends TestCase
{
    use RefreshDatabase, TestsRegistration, WorksWithGoogleCaptcha;

    protected function setUp(): void
    {
        parent::setUp();
        Mail::fake();
        app(KampjaarFactory::class)->isActief()->isOpen()->create(2019);
        $this->withExceptionHandling();
        $this->mockCaptchaVerify();
    }

    /**
     * Test if a registration requires to accept the AV.
     *
     * @test
     */
    public function a_registration_requires_an_accepted_av(): void
    {
        $this->submitRegistration(['av' => null])
            ->assertSessionHasErrors('av');

        $this->submitRegistration(['av' => '0'])
            ->assertSessionHasErrors('av');
    }

    /**
     * Test if a registration can have an insurance.
     *
     * @test
     */
    public function a_registration_can_have_an_insurance(): void
    {
        $this->submitRegistration(['annuleringsverzekering' => null])
            ->assertSessionHasErrors('annuleringsverzekering');

        $this->submitRegistration(['annuleringsverzekering' => '0'])
            ->assertSessionHasErrors('annuleringsverzekering');
    }

    /**
     * Test if a registration requires a voornaam.
     *
     * @test
     */
    public function a_registration_requires_a_voornaam(): void
    {
        $this->mockCaptchaVerify();

        $this->submitRegistration(['voornaam' => null])
            ->assertSessionHasErrors('voornaam');

        $this->withDefaultMollieMockResponse()->submitRegistration(['voornaam' => 'Petertje'])
            ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['voornaam' => 'Petertje Metertje'])
            ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['voornaam' => 'Petertje-Jan Metertje'])
            ->assertSessionHasNoErrors();
    }

    /**
     * Test if a registration requires a roepnaam.
     *
     * @test
     */
    public function a_registration_requires_a_roepnaam(): void
    {
        $this->mockCaptchaVerify();

        $this->submitRegistration(['roepnaam' => null])
            ->assertSessionHasErrors('roepnaam');

        $this->withDefaultMollieMockResponse()->submitRegistration(['roepnaam' => 'Petertje'])
            ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['roepnaam' => 'Petertje Mëtertje'])
            ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['roepnaam' => 'Petertje-Jan Metertje'])
            ->assertSessionHasNoErrors();
    }

    /**
     * Test if a registration requires a voorletters.
     *
     * @test
     */
    public function a_registration_requires_voorletters(): void
    {
        $this->submitRegistration(['voorletters' => null])
            ->assertSessionHasErrors('voorletters');
    }

    /**
     * Test if a registration requires a achternaam.
     *
     * @test
     */
    public function a_registration_requires_a_achternaam(): void
    {
        $this->mockCaptchaVerify();

        $this->submitRegistration(['achternaam' => null])
            ->assertSessionHasErrors('achternaam');

        $this->withDefaultMollieMockResponse()->submitRegistration(['achternaam' => 'Metertje'])
            ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['achternaam' => 'van Metertje'])
            ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['achternaam' => 'van Metertje-Läuwers Willen 3e'])
            ->assertSessionHasNoErrors();
    }

    /**
     * Test if a registration requires a geboortedatum.
     *
     * @test
     */
    public function a_registration_requires_a_geboortedatum(): void
    {
        $this->submitRegistration(['geboortedatum' => null])
            ->assertSessionHasErrors('geboortedatum');

        $this->submitRegistration(['geboortedatum' => 'dit is geen datum vriend'])
            ->assertSessionHasErrors('geboortedatum');
    }

    /**
     * Test if a registration requires a geslacht.
     *
     * @test
     */
    public function a_registration_requires_a_geslacht(): void
    {
        $this->mockCaptchaVerify();
        $this->submitRegistration(['geslacht' => null])
            ->assertSessionHasErrors('geslacht');

        $this->submitRegistration(['geslacht' => 'genderneutraal'])
            ->assertSessionHasErrors('geslacht');

        $this->withDefaultMollieMockResponse()->submitRegistration(['geslacht' => 'm'])
            ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['geslacht' => 'v'])
            ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['geslacht' => 'nb'])
            ->assertSessionHasNoErrors();
    }

    /**
     * Test if a registration requires a opleiding.
     *
     * @test
     */
    public function a_registration_requires_a_opleiding(): void
    {
        $this->mockCaptchaVerify();
        $this->submitRegistration(['studie_id' => null])
            ->assertSessionHasErrors('studie_id');

        $this->submitRegistration(['studie_id' => '123123123123'])
            ->assertSessionHasErrors('studie_id');

        $this->withDefaultMollieMockResponse()->submitRegistration(['studie_id' => '1'])
            ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['studie_id' => '2'])
            ->assertSessionHasNoErrors();
    }

    /**
     * Test if a registration requires a kledingmaat.
     *
     * @test
     */
    public function a_registration_requires_a_kledingmaat(): void
    {
        $this->mockCaptchaVerify();
        $this->submitRegistration(['kledingmaat' => null])
           ->assertSessionHasErrors('kledingmaat');

        $this->submitRegistration(['kledingmaat' => 'xxxxxxxxxl'])
           ->assertSessionHasErrors('kledingmaat');

        $this->withDefaultMollieMockResponse()->submitRegistration(['kledingmaat' => 'xs'])
           ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['kledingmaat' => 's'])
           ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['kledingmaat' => 'm'])
           ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['kledingmaat' => 'l'])
           ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['kledingmaat' => 'xl'])
           ->assertSessionHasNoErrors();
    }

    /**
     * Test if a registration requires a opmerking to be less than 750 characters.
     *
     * @test
     */
    public function a_registration_requires_a_opmerking_to_be_less_than_750_long(): void
    {
        $this->mockCaptchaVerify();
        $this->submitRegistration(['opmerking' => Str::random(751)])
            ->assertSessionHasErrors('opmerking');

        $this->withDefaultMollieMockResponse()->submitRegistration(['opmerking' => Str::random(750)])
            ->assertSessionHasNoErrors();
    }

    /**
     * Test if a registration requires a straat.
     *
     * @test
     */
    public function a_registration_requires_a_straat(): void
    {
        $this->mockCaptchaVerify();

        $this->submitRegistration(['straat' => null])
            ->assertSessionHasErrors('straat');

        $this->withDefaultMollieMockResponse()->submitRegistration(['straat' => 'Van derde Barönstraat'])
            ->assertSessionHasNoErrors();
    }

    /**
     * Test if a registration requires a huisnummer.
     *
     * @test
     */
    public function a_registration_requires_a_huisnummer(): void
    {
        $this->submitRegistration(['huisnummer' => null])
            ->assertSessionHasErrors('huisnummer');
    }

    /**
     * Test if a registration requires a postcode.
     *
     * @test
     */
    public function a_registration_requires_a_postcode(): void
    {
        $this->mockCaptchaVerify();
        $this->submitRegistration(['postcode' => null])
            ->assertSessionHasErrors('postcode');

        $this->submitRegistration(['postcode' => 'AR1721'])
            ->assertSessionHasErrors('postcode');

        $this->withDefaultMollieMockResponse()->submitRegistration(['postcode' => '1234AB'])
            ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['postcode' => '1234 AB'])
            ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['postcode' => '1234aB'])
            ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['postcode' => '1234Ab'])
            ->assertSessionHasNoErrors();

        $this->withDefaultMollieMockResponse()->submitRegistration(['postcode' => '1234 ab'])
            ->assertSessionHasNoErrors();
    }

    /**
     * Test if a registration requires a woonplaats.
     *
     * @test
     */
    public function a_registration_requires_a_woonplaats(): void
    {
        $this->submitRegistration(['woonplaats' => null])
            ->assertSessionHasErrors('woonplaats');
    }

    /**
     * Test if a registration requires an email.
     *
     * @test
     */
    public function a_registration_requires_a_email(): void
    {
        $this->submitRegistration(['email' => null])
            ->assertSessionHasErrors('email');

        $this->submitRegistration(['email' => 'echtgeenemail.nl'])
            ->assertSessionHasErrors('email');
    }

    /**
     * Test if a registration requires a mobiel.
     *
     * @test
     */
    public function a_registration_requires_a_mobiel(): void
    {
        $this->submitRegistration(['mobiel' => null])
            ->assertSessionHasErrors('mobiel');

        $this->submitRegistration(['mobiel' => 'geen mobiel nummer vriend'])
            ->assertSessionHasErrors('mobiel');
    }

    /**
     * Test if a registration requires a telefoon nood.
     *
     * @test
     */
    public function a_registration_requires_a_telefoon_nood(): void
    {
        $this->submitRegistration(['telefoon_nood' => null])
           ->assertSessionHasErrors('telefoon_nood');

        $this->submitRegistration(['telefoon_nood' => 'geen mobiel nummer vriend'])
           ->assertSessionHasErrors('telefoon_nood');
    }

    /**
     * Test if a registration requires a payment type.
     *
     * @test
     */
    public function a_registration_requires_a_payment_type(): void
    {
        $this->submitRegistration(['type' => null])
            ->assertSessionHasErrors('type');

        $this->submitRegistration(['type' => 'Credit card'])
            ->assertSessionHasErrors('type');
    }

    /**
     * Test if a registration requires incasso data when its of incasso type.
     *
     * @test
     */
    public function a_registration_requires_incasso_details_with_incasso_type(): void
    {
        $this->submitRegistration(['type' => 'incasso'])
            ->assertSessionHasErrors('rekeninghouder_naam')
            ->assertSessionHasErrors('rekeninghouder_nummer')
            ->assertSessionHasErrors('rekeninghouder_woonplaats');
    }
}
