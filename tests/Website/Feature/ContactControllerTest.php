<?php

namespace Tests\Website\Feature;

use HUplicatie\Kampjaar;
use HUplicatie\Mail\Website\ContactRequestMailable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Tests\WorksWithGoogleCaptcha;

class ContactControllerTest extends TestCase
{
    use RefreshDatabase, WorksWithGoogleCaptcha;

    protected function setUp(): void
    {
        parent::setUp();
        create(Kampjaar::class);
        Mail::fake();
    }

    /**
     * Test if the contact page can be viewed.
     *
     * @test
     */
    public function the_contact_page_can_be_viewed(): void
    {
        $response = $this->json('GET', '/contact')
            ->assertStatus(200);

        $response->assertSee('Neem contact op met Haag Uit');
    }

    /**
     * Test if the contact form can be submitted.
     *
     * @test
     */
    public function a_contact_form_can_be_submitted(): void
    {
        $this->mockCaptchaVerify();

        $details = [
            'naam'  => 'Baccanardo',
            'email' => 'baccanardo@example.com',
            'tekst' => 'Hoeveel bier mag ik maximaal drinken?',
        ];

        $response = $this->post('/contact', $details);
        Mail::assertQueued(ContactRequestMailable::class, function ($mail) use ($details) {
            return $details['naam'] === $mail->name &&
                $details['email'] === $mail->email &&
                $details['tekst'] === $mail->text &&
                $mail->hasTo('staf@haaguit.com') &&
                $mail->hasFrom($details['email']);
        });

        $response->assertStatus(200);
        $response->assertSee('Beste '.$details['naam']);
    }

    /**
     * Test if validation is enforced in the contact request.
     *
     * @test
     */
    public function validation_is_enforced_for_a_contact_request(): void
    {
        $this->withExceptionHandling();
        $response = $this->post('/contact', []);
        Mail::assertNothingSent();
        $response->assertStatus(302)
            ->assertSessionHasErrors(['naam', 'email', 'tekst']);
    }
}
