<?php

namespace Tests\Website\Unit;

use HUplicatie\Kampjaar;
use HUplicatie\Transformer\ActiveKampjaarVariable;
use HUplicatie\Transformer\VariableTransformer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use InvalidArgumentException;
use Tests\TestCase;

class VariableTransformerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if variables can be replaced in text.
     *
     * @test
     *
     * @return void
     */
    public function variables_can_be_replaced_in_text(): void
    {
        $kampjaar = create(Kampjaar::class, ['actief' => 1]);
        $transformer = new VariableTransformer('String with variable $JAAR$');
        $output = $transformer->addSubstitute(new ActiveKampjaarVariable())->replace();
        $this->assertStringContainsString((string) $kampjaar->jaar, $output);
        $this->assertStringNotContainsString('$JAAR$', $output);
    }

    /**
     * Test if an exception is thrown when no variables are provided.
     *
     * @test
     */
    public function an_exception_is_thrown_when_no_variables_provided(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $transformer = new VariableTransformer('Some random text with variables $JAAR$');
        $transformer->replace();
    }
}
