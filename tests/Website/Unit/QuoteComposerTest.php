<?php

namespace Tests\Website\Unit;

use HUplicatie\Http\ViewComposers\QuoteComposer;
use HUplicatie\Quote;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\View\View;
use Mockery;
use Tests\TestCase;

class QuoteComposerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if a random quote can be retrieved.
     *
     * @test
     *
     * @return void
     */
    public function a_random_quote_can_be_retrieved(): void
    {
        create(Quote::class);

        $quoteComposer = new QuoteComposer();
        $view = Mockery::mock(View::class);
        $view->shouldReceive('with')->with('quote', Mockery::type(Quote::class));
        $quoteComposer->compose($view);
    }

    /**
     * Test if null is returned when no quote exists.
     *
     * @test
     */
    public function if_no_quote_exists_null_is_returned(): void
    {
        $quoteComposer = new QuoteComposer();
        $view = Mockery::mock(View::class);
        $view->shouldReceive('with')->with('quote', null);
        $quoteComposer->compose($view);
    }
}
