<?php

namespace Tests\Website\Unit\Service;

use HUplicatie\Exceptions\InschrijvingCreationException;
use HUplicatie\Http\Requests\Website\RegistratieRequest;
use HUplicatie\Kampjaar;
use HUplicatie\Services\InschrijvingService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class InschrijvingServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if an inschrijving can be processed.
     *
     * @test
     *
     * @return void
     *
     * @throws InschrijvingCreationException
     */
    public function an_inschrijving_can_be_processed(): void
    {
        $requestBag = [
            'roepnaam'      => 'Clemens',
            'voornaam'      => '',
            'voorletters'   => '',
            'achternaam'    => '',
            'geboortedatum' => '',
            'geslacht'      => '',
            'studie_id'     => '',
            'straat'        => '',
            'huisnummer'    => '',
            'postcode'      => '',
            'woonplaats'    => '',
            'email'         => '',
            'mobiel'        => '',
            'kledingmaat'   => 'L',
            'opmerking'     => '',
            'telefoon_nood' => '0612312318',
        ];
        $request = RegistratieRequest::create('', 'POST', $requestBag);
        $service = new InschrijvingService(create(Kampjaar::class, ['jaar' => 2019, 'actief' => true]));
        $inschrijving = $service->processInschrijving($request);
        $this->assertDatabaseHas('personen', [
            'roepnaam'         => 'Clemens',
        ]);
        $this->assertDatabaseHas('kamp_inschrijvingen', ['jaar' => '2019']);
    }
}
