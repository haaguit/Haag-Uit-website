<?php

namespace Tests;

use Exception;
use HUplicatie\User;
use Illuminate\Container\Container;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Exceptions\Handler;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function setUp(): void
    {
        parent::setUp();
        $this->disableExceptionHandling();
    }

    protected function signIn($user = null): TestCase
    {
        $user = $user ?: create(User::class);

        $this->actingAs($user);

        return $this;
    }

    protected function signInAsRole(string $role, $user = null): TestCase
    {
        $user = $user ?: create(User::class);

        $user->assignRole($role);

        $this->actingAs($user);

        return $this;
    }

    protected function signInWithPermissions($permission, $user = null): TestCase
    {
        $user = $user ?: create(User::class);

        $user->givePermissionTo($permission);

        $this->actingAs($user);

        return $this;
    }

    protected function disableExceptionHandling(): void
    {
        $this->oldExceptionHandler = $this->app->make(ExceptionHandler::class);
        $this->app->instance(ExceptionHandler::class, new class() extends Handler
        {
            public function __construct()
            {
                parent::__construct(new Container());
            }

            public function report(Exception $e)
            {
            }

            public function render($request, Exception $e)
            {
                throw $e;
            }
        });
    }

    protected function withExceptionHandling(): TestCase
    {
        $this->app->instance(ExceptionHandler::class, $this->oldExceptionHandler);

        return $this;
    }
}
