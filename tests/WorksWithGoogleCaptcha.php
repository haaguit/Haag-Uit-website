<?php

namespace Tests;

use TimeHunter\LaravelGoogleReCaptchaV3\Core\GoogleReCaptchaV3Response;
use TimeHunter\LaravelGoogleReCaptchaV3\Facades\GoogleReCaptchaV3;

trait WorksWithGoogleCaptcha
{
    public function mockCaptchaVerify(): void
    {
        GoogleReCaptchaV3::shouldReceive('init');
        GoogleReCaptchaV3::shouldReceive('setAction')->andReturnSelf();
        GoogleReCaptchaV3::shouldReceive('verifyResponse')
            ->andReturn(new GoogleReCaptchaV3Response(['success' => true], '127.0.0.1'));
    }
}
