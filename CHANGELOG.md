# Changelog

HUplicatie changelog. Proberen lekker alle wijzigingen bij te houden hiero.

Deze applicatie is geschreven met veel liefde en bier door de Huis ICT'er.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
Check out the issues here: [HUplicatie Issue Board](https://gitlab.com/haaguit/Haag-Uit-website/-/boards)

## [Unreleased]

- N/A

## [1.12.1] - 2022-07-08

### Changed

- Jan naar Mees voor DTF statistiek.

## [1.12.0] - 2022-05-16

### Added

- Nieuwe opleiding ADS & AI.

### Changed

- Inschrijvingen weer terug naar normaal.

## [1.11.0] - 2022-02-15

### Added

- Issuenummer #65: Verwijderen medewerker inschrijving.

## [1.10.2] - 2021-08-07

### Bugfix

- Bugfix voor HBO-ICT lekkerbellen studie verhoudingen die vergeten was vorige bugfix.

## [1.10.1] - 2021-08-07

### Bugfix

- Bugfix voor lekkerbellen studie verhoudingen na migratie.

## [1.10.0] - 2021-07-11

## Dit is een Release met tijdelijke functies voor inschrijvingen 2021!

- Issuenummer #79: Hele cel is nu klikbaar voor aanwezigheid.

## [1.9.1] - 2021-05-19

### Changed

- Issuenummer #84: Aanwezigheid is nu standaard niets en kan ook op niets gezet worden.

### Fixed

- Issuenummer #80: Actiepunten kunnen weer gewijzigd worden zonder deadline nu.
- Issuenummer #81: Afgemaakte actiepunten kunnen nu ook gefilterd worden op staffer.

## [1.9.0] - 2021-03-31

### Added

- Issuenummer #63: Persoon overzicht toegevoegd.

### Changed

- Issuenummer #85: Sorteren en filteren bij medewerkers inschrijvingen overzicht en aanwezigheid.

### Fixed

- Issuenummer #82: Collapse werkt weer voor afgemaakte APtjes.

## [1.8.0] - 2021-01-18

### Added

- Issuenummer #55: Bijhouden van medewerkers aanwezigheid en overzicht daarvan.
- Issuenummer #69: Static code analysis toegevoegd aan pipeline voor code kwaliteit.

### Changed

- Issuenummer #72: NPM wordt niet meer gebouwd op de server zelf maar in CI.
  Dit maakt het deploy proces hopelijk sneller, logischer en minder foutgevoelig.
- CoreUI update naar versie 3.

## [1.7.0] - 2020-12-13

### Added

- Wijzigen van medewerker inschrijvingen.

### Changed

- Verwijder knop uitgeschakeld vanwege ontbrekende functionaliteit.

## [1.6.0]

### Added

- Toevoegen van medewerker inschrijvingen.

## [1.5.2-REVERT]

### Added

- Basic Readme beschrijving om het project te draaien.

### Changed

- Revert van 1.5.1-BACKUP.

## [1.5.2-BACKUP]

### Changed

- Inschrijfformulier aangepast voor COVID-19 perikelen. Gaat gerevert worden na HU 2020.

## [1.5.2]

### Changed

- Update voor front end dependencies

## [1.5.1]

### Fixed

- Quick bugfix: Opruimen van oude websocket statistieken.

## [1.5.0]

- Issuenummer #41: Dashboard laat nu wat nuttige statistieken zien.
- Laravel framework update naar 5.8.
- Kleine bugfix in actiepunten table door "Event bubbling" te stoppen.
  Het detail scherm wordt niet meer laten zien zodra er op de "Afgemaakt" button wordt geklikt.

## [1.4.0]

### Changed

- Issue #45: Emails versturen is naar de queue verplaatst.

## [1.3.0]

### Added

- Issue #31: Een excel export functie gemaakt voor alle niet afgemelde inschrijvingen.
- Issue #36: Logout button toegevoegd aan 403 scherm.

### Changed

- Issue #48: Verstuur knop gedisabled in inschrijf formulier voor ongeduldige mensen.
- Issue #49: Inschrijvingen worden nu direct doorgestuurd naar ideal betaling.

## [1.2.3]

### Added

- Issue #46: Retry betaling link toegevoegd bij alle iDeal betalingen die niet betaald zijn (met bonus kopieer knop!).

### Changed

- Telefoon nummer gewijzigd in 088 nummer op verzoek van Toni.

### Removed

- Static code analysis uit pipeline gehaald. Werkte toch voor geen meter.

## [1.2.2]

### Changed

- Issue #43: Deadline is vanaf nu optioneel bij actiepunt.

## [1.2.1]

### Added

- Issue #43: Bij inschrijving niet gespecificeerd geslacht kunnen kiezen onder de term 'Non-Binair'.

## [1.2.0]

### Added

- Issue #38: Extra shirtmaten toegevoegd.

### Changed

- Issue #39: Registratie formulier laat nu relatieve datum zien voor incasso.

### Fixed

- Issue #40: Validatie aangepast voor registratie formulier zodat geldige data nu ook als geldig wordt gezien.
- Issue #39: Juiste foutmelding voor Captcha fout in registratie formulier.

## [1.1.0]

### Added

- Issue #34: AP filter voor staffer toegevoegd.

### Changed

- Security improvements voor stag/prod server.

## [1.0.0]

### Added

- Issue #37: Captcha check ingebouwd op contact en registratie formulier.
- Issue #28: LEKKER BELLENNNNNNNN

### Fixed

- Issue #1: Password reset van stafplicatie verwijst niet meer naar website maar terug naar stafplicatie.
- Issue #32: Variabelen lijstje toegevoegd naast pagina editor.
- Issue #35: Nieuwsbericht "lees meer" link verwijst nu naar juiste nieuwsbericht.

## [1.0.0-BETA-2]

### Fixed

- 500 error pagina wordt niet weergegeven. Blade directive if vervangen door isset in 500 error pagina.
- Sponsor component voor front end genereerde veel fouten.

## [1.0.0-BETA]

### Added

- Issue #24: Sponsor management toegevoegd.
- Issue #35: Front end component voor het laten zien van sponsoren.
- Link naar sponsoren vergeten in menu, nu toegevoegd.

### Fixed

- Dashboard in menu was altijd "actief". CSS attribute weggehaald die dat veroorzaakte.
- Issue #33: Delete fix voor sponsoren.

## [0.4.0]

### Added

- Issue #8: Actiepunten beheer geïmplementeerd.

## [0.3.0]

### Added

- Issue #13: Nieuwsberichten beheer geïmplementeerd.
- Issue #27: Promo filmpjes pagina content toegevoegd.

### Fixed

- Website front end was broken door dependency upgrades uit 0.2.1.
- Notatie voor aanmaakdatum van nieuwsbericht gefixt bij het bekijken van een nieuwsbericht.
- Link van homepagina naar nieuws artikel gefixt.
- Issue #26: Dashboard verplaatst naar `/dashboard` en de root url verwijst daar nu ook naar.
- Link fixes in user scherm.

## [0.2.1]

### Changed

- Update van front end dependencies. Vue, Laravel Mix etc.

## [0.2.0]

### Added

- Laten we eens normale versie nummering doen. IPV patches gewoon minors bij nieuwe functies voor de beta?
- Issue #11: Variable replacer toegevoegd voor front end paginas.
- Issue #10: Pagina's bewerken toegevoegd. Helaas alleen wel via normale text area. Moeten we nog wat op verzinnen.

### Changed

- Related to Issue #11: Variable replacement toegevoegd als extra methode ipv standaard replaced. Methode
  `withVariableReplacements()` vervangt alle variabelen in de tekst.

## [0.1.6]

### Added

- Issue #23: Vertaling voor inschrijfformulier validatie fouten toegevoegd.
- Issue #6: Inschrijvingen beheer toegevoegd.

## [0.1.5]

### Added

- Issue #20: Betalingsproces gefixt en verbeterd.
- Juiste jaartal in footer van website op basis van huidige jaar

### Fixed

- Payment url correct in inschrijf email.

### Changed

- Vendors uit front end package extracted. Aparte vendor.js nu.

## [0.1.4]

### Added

- Issue #5: Kampjaar beheer toegevoegd.
- Issue #2: Emails bij registratie naar inschrijving en staf.

## [0.1.3]

### Added

- Sentry support!

### Changed

- Issue #21: Veel changes op pipeline. Betere CI en hopelijk CD.
- Refactoring op basis van Code Sniffer standards.

## [0.1.2]

### Added

- Websockets demo toegevoegd aan stafplicatie.

### Fixed

- Issue #19: Bug fix voor middleware exclusion op verkeerde methode.

## [0.1.1] - 2019-02-06

### Changed

- Issue #3: Vergeten commits bijgevoegd van Beheer van gebruikers.

### Fixed

- Random falende test op unique constraint fail.

## [0.1.0] - 2019-02-06

### Added

- Issue #3: Beheer van inschrijvingen.
- Flash component voor het laten zien van meldingen in de front end.
- Dit changelog bestand
