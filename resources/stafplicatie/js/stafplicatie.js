/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import DashboardPage from "./pages/DashboardPage";
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import Clipboard from 'v-clipboard'

import Flash from './components/Flash.vue';
import NewInschrijving from './components/NewInschrijving.vue';
import PaymentStatusBadge from './components/PaymentStatusBadge.vue';
import PresentStatusBadge from './components/PresentStatusBadge.vue';
import ImageUpload from './components/ImageUpload.vue';
import PaginaEditor from './components/PaginaEditor.vue';
import Inschrijvingen from './components/InschrijvingTable.vue';
import NieuwsberichtEditor from './components/NieuwsberichtEditor';
import MarkdownPreview from './components/MarkdownPreview';

import EntitiesTable from './pages/EntitiesTable.vue';
import InschrijvingPage from './pages/InschrijvingPage.vue';
import ActiepuntPage from './pages/ActiepuntPage';
import AanwezigheidPage from "./pages/aanwezigheid/AanwezigheidPage";
import PersoonPage from "./pages/PersoonPage";
import MedewerkerIndexPage from "./pages/medewerker/MedewerkerIndexPage";

Vue.component('flash', Flash);
Vue.component('new-inschrijving', NewInschrijving);

Vue.component('payment-status-badge', PaymentStatusBadge);
Vue.component('present-status-badge', PresentStatusBadge);

Vue.component('image-upload', ImageUpload);
Vue.component('pagina-editor', PaginaEditor);
Vue.component('inschrijvingen', Inschrijvingen);
Vue.component('nieuwsbericht-editor', NieuwsberichtEditor);
Vue.component('markdown-preview', MarkdownPreview);

Vue.component('entities-table', EntitiesTable);
Vue.component('inschrijving-page', InschrijvingPage);
Vue.component('persoon-page', PersoonPage);
Vue.component('actiepunt-page', ActiepuntPage);
Vue.component('dashboard-page', DashboardPage);
Vue.component('aanwezigheid-page', AanwezigheidPage);
Vue.component('medewerker-index-page', MedewerkerIndexPage);

Vue.use(Clipboard);

const stafplicatie = new Vue({
    el: '#app',

    created() {
        Echo.private('dashboard')
            .listen('InternetConnection.Heartbeat', (e) => {
                console.log('Heartbeat received!');
            });
    },

});
