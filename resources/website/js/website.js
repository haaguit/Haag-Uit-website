/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue';
window.Vue = require('vue');

import InschrijfWiggleComponent from './components/InschrijfWiggleComponent';
import HeaderAnimationsComponent from './components/HeaderAnimationsComponent';
import QuoteBoxComponent from './components/QuoteBoxComponent';
import LatestNewsComponent from './components/LatestNewsComponent';
import BirthdatePickerComponent from './components/BirthdatePickerComponent';
import ErrorReportingComponent from './components/ErrorReportingComponent';
import SponsorBoxComponent from './components/SponsorBoxComponent';
import InschrijfFormulierPage from './pages/InschrijfFormulierPage';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('inschrijf-wiggle-component', InschrijfWiggleComponent);
Vue.component('header-animations-component', HeaderAnimationsComponent);
Vue.component('quote-box-component', QuoteBoxComponent);
Vue.component('latest-news-component', LatestNewsComponent);
Vue.component('birthdate-picker-component', BirthdatePickerComponent);
Vue.component('error-reporting-component', ErrorReportingComponent);
Vue.component('sponsor-box-component', SponsorBoxComponent);
Vue.component('inschrijf-formulier-page', InschrijfFormulierPage);

const website = new Vue({
    el: '#website',
});
