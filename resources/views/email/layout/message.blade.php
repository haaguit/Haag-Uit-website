@component('email.layout.layout')
    @slot('header')
        @component('email.layout.header', ['kampjaar' => HUplicatie\Kampjaar::active()->first()])
        @endcomponent
    @endslot

    {{ $slot }}

    @slot('footer')
        @component('email.layout.footer')
        @endcomponent
    @endslot
@endcomponent