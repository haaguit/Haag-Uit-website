<tr>
    <td style="box-sizing: border-box;">
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0"
               style="margin: 0 auto; padding: 0; text-align: center; width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px;">
            <tr>
                <td class="content-cell" align="center" style="padding: 35px;">
                    <p style="line-height: 1.5em; margin-top: 0; color: #aeaeae; font-size: 12px; text-align: center;">
                        &copy; {{ \Carbon\Carbon::now()->year }} Stichting Haag Uit. Alle rechten voorbehouden.</p>
                </td>
            </tr>
        </table>
    </td>
</tr>