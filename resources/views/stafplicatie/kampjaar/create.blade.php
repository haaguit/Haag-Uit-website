@extends('stafplicatie.layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Kampjaar aanmaken</h5>
                    <form action="/stafplicatie/kampjaar" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="jaar">Jaar</label>
                            <input type="number" class="form-control {{ $errors->has('jaar') ? 'is-invalid' : '' }}"
                                   id="jaar" name="jaar" value="{{ old('jaar') }}"
                                   placeholder="2020">
                            @if ($errors->has('jaar'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('jaar') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="start">Start</label>
                            <input type="date" class="form-control  {{ $errors->has('start') ? 'is-invalid' : '' }}"
                                   id="start" name="start" value="{{ old('start') }}"
                                   placeholder="YYYY-MM-DD">
                            @if ($errors->has('start'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('start') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="eind">Eind</label>
                            <input type="date" class="form-control  {{ $errors->has('eind') ? 'is-invalid' : '' }}"
                                   id="eind" name="eind" value="{{ old('eind') }}"
                                   placeholder="YYYY-MM-DD">
                            @if ($errors->has('eind'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('eind') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="inschrijfbedrag">Inschrijfbedrag</label>
                            <input type="number"
                                   class="form-control  {{ $errors->has('inschrijfbedrag') ? 'is-invalid' : '' }}"
                                   id="inschrijfbedrag" name="inschrijfbedrag" value="{{ old('inschrijfbedrag') }}"
                                   placeholder="100,00">
                            @if ($errors->has('inschrijfbedrag'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('inschrijfbedrag') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="verzekeringbedrag">Verzekeringbedrag</label>
                            <input type="number"
                                   class="form-control {{ $errors->has('verzekeringbedrag') ? 'is-invalid' : '' }}"
                                   id="verzekeringbedrag" name="verzekeringbedrag"
                                   value="{{ old('verzekeringbedrag') }}"
                                   placeholder="100,00">
                            @if ($errors->has('verzekeringbedrag'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('verzekeringbedrag') }}
                                </div>
                            @endif
                        </div>
                        <image-upload error="{{ $errors->first('logo') }}"></image-upload>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Aanmaken</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection