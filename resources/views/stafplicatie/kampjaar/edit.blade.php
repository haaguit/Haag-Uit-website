@extends('stafplicatie.layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Kampjaar wijzigen</h5>
                    <form action="/stafplicatie/kampjaar/{{ $kampjaar->jaar }}" method="post"
                          enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="start">Start</label>
                            <input type="date" class="form-control  {{ $errors->has('start') ? 'is-invalid' : '' }}"
                                   id="start" name="start" value="{{ old('start', $kampjaar->start) }}"
                                   placeholder="dd-mm-yyyy">
                            @if ($errors->has('start'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('start') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="eind">Eind</label>
                            <input type="date" class="form-control  {{ $errors->has('eind') ? 'is-invalid' : '' }}"
                                   id="eind" name="eind"
                                   value="{{ old('eind', $kampjaar->eind) }}" placeholder="dd-mm-yyyy">
                            @if ($errors->has('eind'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('eind') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="inschrijfbedrag">Inschrijf bedrag</label>
                            <input type="number"
                                   class="form-control  {{ $errors->has('inschrijfbedrag') ? 'is-invalid' : '' }}"
                                   id="inschrijfbedrag" name="inschrijfbedrag"
                                   value="{{ old('inschrijfbedrag', $kampjaar->inschrijfbedrag) }}"
                                   placeholder="100,00">
                            @if ($errors->has('inschrijfbedrag'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('inschrijfbedrag') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="verzekeringbedrag">Verzekeringbedrag</label>
                            <input type="number"
                                   class="form-control  {{ $errors->has('verzekeringbedrag') ? 'is-invalid' : '' }}"
                                   id="verzekeringbedrag"
                                   value="{{ old('verzekeringbedrag', $kampjaar->verzekeringbedrag) }}"
                                   name="verzekeringbedrag" placeholder="5,00">
                            @if ($errors->has('verzekeringbedrag'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('verzekeringbedrag') }}
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Huidige logo</h5>
                                        <img src="{{$kampjaar->logo}}" alt="HU logo {{ $kampjaar->jaar }}" style="max-height:270px; max-width: 270px" width="100%" height="100%">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <image-upload error="{{ $errors->first('logo') }}"></image-upload>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Wijzigen</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection