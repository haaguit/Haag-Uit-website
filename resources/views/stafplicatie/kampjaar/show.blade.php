@extends('stafplicatie.layouts.app')

@section('content')
    {{ $kampjaar->jaar }} {{ $kampjaar->start }} {{ $kampjaar->eind }} {{ $kampjaar->inschrijfbedrag }} {{ $kampjaar->verzekeringbedrag }} {{ $kampjaar->actief ? 'Actief' : 'Inactief' }} {{ $kampjaar->open ? 'Open' : 'Gesloten' }}<br/>
    <img src="{{ $kampjaar->logo }}" alt="HU Logo {{ $kampjaar->jaar }}" height="150px" width="150px">
@endsection