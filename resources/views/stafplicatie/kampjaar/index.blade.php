@extends('stafplicatie.layouts.app')

@section('content')
    <entities-table :entities="{{ json_encode($kampjaren) }}" inline-template>
        <div class="row">
            <div class="col-md-12 ">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex">
                                    <div class="p-2">
                                        <h5 class="card-title">Kampjaren</h5>
                                    </div>
                                    <div class="ml-auto p-2">
                                        <a role="button" class="btn btn-success" href="/stafplicatie/kampjaar/create">
                                            <i class="fas fa-calendar-alt"></i> Aanmaken
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="d-table-cell" scope="col">Jaar</th>
                                        <th class="d-none d-md-table-cell" scope="col">Start</th>
                                        <th class="d-none d-md-table-cell" scope="col">Eind</th>
                                        <th class="d-none d-lg-table-cell" scope="col">Actief</th>
                                        <th class="d-none d-lg-table-cell" scope="col">Open</th>
                                        <th class="d-none d-lg-table-cell" scope="col">Logo</th>
                                        <th class="d-table-cell" scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr is="kampjaar-table-row" v-for="(kampjaar, index) in items" :key="kampjaar.jaar"
                                                            :jaar="kampjaar"
                                                            @deleted="remove(index)">
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </entities-table>
@endsection