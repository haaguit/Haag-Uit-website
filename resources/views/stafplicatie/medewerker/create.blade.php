@extends('stafplicatie.layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            <form action="/stafplicatie/medewerker" method="post">
                @csrf
                <h3>Medewerker inschrijving aanmaken</h3>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Persoonsgegevens</h5>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="voornaam">Voornaam</label>
                                            <input type="text"
                                                   class="form-control {{ $errors->has('voornaam') ? 'is-invalid' : '' }}"
                                                   id="voornaam" name="voornaam" value="{{ old('voornaam') }}"
                                                   placeholder="Voornaam">
                                            @if ($errors->has('voornaam'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('voornaam') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="achternaam">Achternaam</label>
                                            <input type="text"
                                                   class="form-control {{ $errors->has('achternaam') ? 'is-invalid' : '' }}"
                                                   id="achternaam" name="achternaam" value="{{ old('achternaam') }}"
                                                   placeholder="Achternaam">
                                            @if ($errors->has('achternaam'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('achternaam') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="voorletters">Voorletters</label>
                                            <input type="text"
                                                   class="form-control {{ $errors->has('voorletters') ? 'is-invalid' : '' }}"
                                                   id="voorletters" name="voorletters" value="{{ old('voorletters') }}"
                                                   placeholder="Voorletters">
                                            @if ($errors->has('voorletters'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('voorletters') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="roepnaam">Roepnaam</label>
                                            <input type="text"
                                                   class="form-control {{ $errors->has('roepnaam') ? 'is-invalid' : '' }}"
                                                   id="roepnaam" name="roepnaam" value="{{ old('roepnaam') }}"
                                                   placeholder="Roepnaam">
                                            @if ($errors->has('roepnaam'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('roepnaam') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-8">
                                        <div class="form-group">
                                            <label for="straat">Straat</label>
                                            <input type="text"
                                                   class="form-control {{ $errors->has('straat') ? 'is-invalid' : '' }}"
                                                   id="straat" name="straat" value="{{ old('straat') }}"
                                                   placeholder="Straat">
                                            @if ($errors->has('straat'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('straat') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="huisnummer">Huisnummer</label>
                                            <input type="text"
                                                   class="form-control {{ $errors->has('huisnummer') ? 'is-invalid' : '' }}"
                                                   id="huisnummer" name="huisnummer" value="{{ old('huisnummer') }}"
                                                   placeholder="Nummer">
                                            @if ($errors->has('huisnummer'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('huisnummer') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="postcode">Postcode</label>
                                            <input type="text"
                                                   class="form-control {{ $errors->has('postcode') ? 'is-invalid' : '' }}"
                                                   id="postcode" name="postcode" value="{{ old('postcode') }}"
                                                   placeholder="Postcode">
                                            @if ($errors->has('postcode'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('postcode') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="form-group">
                                            <label for="woonplaats">Woonplaats</label>
                                            <input type="text"
                                                   class="form-control {{ $errors->has('woonplaats') ? 'is-invalid' : '' }}"
                                                   id="woonplaats" name="woonplaats" value="{{ old('woonplaats') }}"
                                                   placeholder="Woonplaats">
                                            @if ($errors->has('woonplaats'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('woonplaats') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="mobiel">Mobiel</label>
                                            <input type="text"
                                                   class="form-control {{ $errors->has('mobiel') ? 'is-invalid' : '' }}"
                                                   id="mobiel" name="mobiel" value="{{ old('mobiel') }}"
                                                   placeholder="Mobiel">
                                            @if ($errors->has('mobiel'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('mobiel') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="telefoon_nood">Telefoon nood</label>
                                            <input type="text"
                                                   class="form-control {{ $errors->has('telefoon_nood') ? 'is-invalid' : '' }}"
                                                   id="telefoon_nood" name="telefoon_nood"
                                                   value="{{ old('telefoon_nood') }}"
                                                   placeholder="Telefoon nood">
                                            @if ($errors->has('telefoon_nood'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('telefoon_nood') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Overige gegevens</h5>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="studentnummmer">Studentnummer</label>
                                            <input type="text"
                                                   class="form-control {{ $errors->has('studentnummer') ? 'is-invalid' : '' }}"
                                                   id="studentnummer" name="studentnummer"
                                                   value="{{ old('studentnummer') }}"
                                                   placeholder="Studentnummer">
                                            @if ($errors->has('studentnummer'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('studentnummer') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="geboortedatum">Geboortedatum</label>
                                            <input type="date"
                                                   class="form-control {{ $errors->has('geboortedatum') ? 'is-invalid' : '' }}"
                                                   id="geboortedatum" name="geboortedatum"
                                                   value="{{ old('geboortedatum') }}"
                                                   placeholder="YYYY-MM-DD">
                                            @if ($errors->has('geboortedatum'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('geboortedatum') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text"
                                                   class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                                                   id="email" name="email" value="{{ old('email') }}"
                                                   placeholder="Email">
                                            @if ($errors->has('email'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('email') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="geslacht">Geslacht</label>
                                            <select id="geslacht" name="geslacht"
                                                    class="form-control {{ $errors->has('geslacht') ? 'is-invalid' : '' }}">
                                                <option disabled {{ (!old('geslacht')) ? 'selected' : '' }}>Geslacht</option>
                                                <option value="m" {{ old('geslacht') === 'm' ? 'selected' : '' }}>Man</option>
                                                <option value="v" {{ old('geslacht') === 'v' ? 'selected' : '' }}>Vrouw</option>
                                                <option value="o" {{ old('geslacht') === 'o' ? 'selected' : '' }}>Non-Binair</option>
                                            </select>
                                            @if ($errors->has('geslacht'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('geslacht') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="rijbewijs">Rijbewijs</label>
                                            <select id="rijbewijs" name="rijbewijs"
                                                    class="form-control {{ $errors->has('rijbewijs') ? 'is-invalid' : '' }}">
                                                <option disabled {{ (!old('rijbewijs')) ? 'selected' : '' }}>Rijbewijs</option>
                                                <option value="Geen" {{ old('rijbewijs') === 'Geen' ? 'selected' : '' }}>Geen</option>
                                                <option value="B" {{ old('rijbewijs') === 'B' ? 'selected' : '' }}>B</option>
                                                <option value="BE {{ old('rijbewijs') === 'BE' ? 'selected' : '' }}">BE</option>
                                            </select>
                                            @if ($errors->has('rijbewijs'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('rijbewijs') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="ehbo">EHBO diploma</label>
                                            <select id="ehbo" name="ehbo"
                                                    class="form-control {{ $errors->has('ehbo') ? 'is-invalid' : '' }}">
                                                <option disabled {{ (!old('ehbo')) ? 'selected' : '' }}>EHBO diploma</option>
                                                <option value="Geen" {{ old('ehbo') === 'Geen' ? 'selected' : '' }}>Geen</option>
                                                <option value="EHBO" {{ old('ehbo') === 'EHBO' ? 'selected' : '' }}>EHBO</option>
                                                <option value="BHV" {{ old('ehbo') === 'BHV' ? 'selected' : '' }}>BHV</option>
                                            </select>
                                            @if ($errors->has('ehbo'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('ehbo') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group">
                                            <label for="studie">Studie</label>
                                            <select id="studie_id" name="studie_id"
                                                    class="form-control {{ $errors->has('studie_id') ? 'is-invalid' : '' }}">
                                                <option disabled {{ (!old('studie_id')) ? 'selected' : '' }}>Studie</option>
                                                @foreach($studies as $studie)
                                                    <option value="{{ $studie->id }}" {{ old('studie_id') === $studie->id ? 'selected' : '' }}>{{ $studie->naam }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('ehbo'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('ehbo') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Functie keuzes</h5>
                                <div class="row">
                                    <div class="col-12 col-md-6 mb-3 mb-md-0">
                                        <div class="row border-bottom mb-2">
                                            <div class="col-6"><strong>Functie</strong></div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <strong><span
                                                            class="{{ $errors->has('eerste_keus') ? 'text-danger' : '' }}">1e Keus</span></strong>
                                            </div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <strong>2e Keus</strong>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">BAR</div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" {{ in_array('BAR', old('eerste_keus') ?? []) ? 'checked' : '' }}
                                                       value="BAR" id="eerste_keus_BAR"
                                                       name="eerste_keus[]">
                                            </div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" value="BAR" id="tweede_keus_BAR"
                                                       {{ in_array('BAR', old('tweede_keus') ?? []) ? 'checked' : '' }} name="tweede_keus[]">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">EHBO</div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" {{ in_array('EHBO', old('eerste_keus') ?? []) ? 'checked' : '' }}
                                                       value="EHBO" id="eerste_keus_EHBO"
                                                       name="eerste_keus[]">
                                            </div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" value="EHBO" id="tweede_keus_EHBO"
                                                       {{ in_array('EHBO', old('tweede_keus') ?? []) ? 'checked' : '' }} name="tweede_keus[]">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">Foto</div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" {{ in_array('Foto', old('eerste_keus') ?? []) ? 'checked' : '' }}
                                                       value="Foto" id="eerste_keus_Foto"
                                                       name="eerste_keus[]">
                                            </div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" value="Foto" id="tweede_keus_Foto"
                                                       {{ in_array('Foto', old('tweede_keus') ?? []) ? 'checked' : '' }} name="tweede_keus[]">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">Keuken</div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" {{ in_array('Keuken', old('eerste_keus') ?? []) ? 'checked' : '' }}
                                                       value="Keuken" id="eerste_keus_Keuken"
                                                       name="eerste_keus[]">
                                            </div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" value="Keuken" id="tweede_keus_Keuken"
                                                       {{ in_array('Keuken', old('tweede_keus') ?? []) ? 'checked' : '' }} name="tweede_keus[]">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="row border-bottom mb-2">
                                            <div class="col-6"><strong>Functie</strong></div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <strong><span
                                                            class="{{ $errors->has('eerste_keus') ? 'text-danger' : '' }}">1e Keus</span></strong>
                                            </div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <strong>2e Keus</strong>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">Leider</div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" {{ in_array('Leider', old('eerste_keus') ?? []) ? 'checked' : '' }}
                                                       value="Leider" id="eerste_keus_Leider"
                                                       name="eerste_keus[]">
                                            </div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" value="Leider" id="tweede_keus_Leider"
                                                       {{ in_array('Leider', old('tweede_keus') ?? []) ? 'checked' : '' }} name="tweede_keus[]">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">Logisitek</div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" {{ in_array('Logistiek', old('eerste_keus') ?? []) ? 'checked' : '' }}
                                                       value="Logistiek" id="eerste_keus_Logistiek"
                                                       name="eerste_keus[]">
                                            </div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" {{ in_array('Logistiek', old('tweede_keus') ?? []) ? 'checked' : '' }}
                                                       value="Logistiek" id="tweede_keus_Logistiek"
                                                       name="tweede_keus[]">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">Techniek</div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" {{ in_array('Techniek', old('eerste_keus') ?? []) ? 'checked' : '' }}
                                                       value="Techniek" id="eerste_keus_Techniek"
                                                       name="eerste_keus[]">
                                            </div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" value="Techniek" id="tweede_keus_Techniek"
                                                       {{ in_array('Techniek', old('tweede_keus') ?? []) ? 'checked' : '' }} name="tweede_keus[]">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">Video</div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" {{ in_array('Video', old('eerste_keus') ?? []) ? 'checked' : '' }}
                                                       value="Video" id="eerste_keus_Video"
                                                       name="eerste_keus[]">
                                            </div>
                                            <div class="col-3 d-flex justify-content-center">
                                                <input type="checkbox" value="Video" id="tweede_keus_Video"
                                                       {{ in_array('Video', old('tweede_keus') ?? []) ? 'checked' : '' }} name="tweede_keus[]">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Overig</h5>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="ervaring">Ervaring</label>
                                            <textarea id="ervaring" name="ervaring" rows="3"
                                                      class="form-control {{ $errors->has('ervaring') ? 'is-invalid' : '' }}"
                                                      placeholder="Ervaring">{{ old('ervaring') }}</textarea>
                                            @if ($errors->has('ervaring'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('ervaring') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="motivatie">Motivatie</label>
                                            <textarea id="motivatie" name="motivatie" rows="3"
                                                      class="form-control {{ $errors->has('motivatie') ? 'is-invalid' : '' }}"
                                                      placeholder="Motivatie">{{ old('motivatie') }}</textarea>
                                            @if ($errors->has('motivatie'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('motivatie') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="eigenschappen">Eigenschappen</label>
                                            <textarea id="eigenschappen" name="eigenschappen" rows="3"
                                                      class="form-control {{ $errors->has('eigenschappen') ? 'is-invalid' : '' }}"
                                                      placeholder="Eigenschappen">{{ old('eigenschappen') }}</textarea>
                                            @if ($errors->has('eigenschappen'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('eigenschappen') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row pb-4">
                    <div class="col-12 d-flex justify-content-end">
                        <button type="submit" name="submit" value="Opslaan" class="btn btn-success">Opslaan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection