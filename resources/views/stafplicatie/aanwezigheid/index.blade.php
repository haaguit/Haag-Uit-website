@extends('stafplicatie.layouts.app')

@section('content')
    <aanwezigheid-page :medewerkers="{{ json_encode($medewerkers) }}" inline-template>
        <div>
            <div class="row">
                <div class="col-md-12 ">
                    <div class="d-flex justify-content-between">
                        <h3 class="card-title">Aanwezigheid</h3>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="mb1-tab" data-toggle="tab" href="#mb1"
                                       role="tab" aria-controls="mb1" aria-selected="true">MB1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="mb2-tab" data-toggle="tab" href="#mb2" role="tab"
                                       aria-controls="mb2" aria-selected="false">MB2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="gesprekjes-tab" data-toggle="tab" href="#gesprekjes"
                                       role="tab" aria-controls="gesprekjes"
                                       aria-selected="false">Gesprekjes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="mv3-tab" data-toggle="tab" href="#mv3" role="tab"
                                       aria-controls="mv3" aria-selected="false">MV3</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="mv4-tab" data-toggle="tab" href="#mv4" role="tab"
                                       aria-controls="mv4" aria-selected="false">MV4</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="overview-tab" data-toggle="tab" href="#overview"
                                       role="tab"
                                       aria-controls="overview" aria-selected="false">Overzicht</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="mb1" role="tabpanel"
                                     aria-labelledby="mb1-tab">
                                    <div class="mt-2">
                                        <div class="row mx-1">
                                            <div class="col-sm border-bottom py-1 text-center text-sm-left">
                                                <div class="font-weight-bold">Naam</div>
                                            </div>
                                            <div class="col-sm col-lg-4 text-center d-flex flex-row mt-1 mt-sm-0 py-1 border-bottom">
                                                <div style="width: 25%">N/B</div>
                                                <div style="width: 25%">Aanwezig</div>
                                                <div style="width: 25%">Afgemeld</div>
                                                <div style="width: 25%">Afwezig</div>
                                            </div>
                                        </div>
                                        <aanwezigheid-table-row v-for="(m, index) in meetings[0]"
                                                                :medewerker="m" :id="`${m.id}-${index}`"
                                                                :key="`${m.id}-${index}`"></aanwezigheid-table-row>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="mb2" role="tabpanel" aria-labelledby="mb2-tab">
                                    <div class="mt-2">
                                        <div class="row mx-1">
                                            <div class="col-sm border-bottom py-1 text-center text-sm-left">
                                                <div class="font-weight-bold">Naam</div>
                                            </div>
                                            <div class="col-sm col-lg-4 text-center d-flex flex-row mt-1 mt-sm-0 py-1 border-bottom">
                                                <div style="width: 25%">N/B</div>
                                                <div style="width: 25%">Aanwezig</div>
                                                <div style="width: 25%">Afgemeld</div>
                                                <div style="width: 25%">Afwezig</div>
                                            </div>
                                        </div>
                                        <aanwezigheid-table-row v-for="(m, index) in meetings[1]"
                                                                :medewerker="m" :id="`${m.id}-${index}`"
                                                                :key="`${m.id}-${index}`"></aanwezigheid-table-row>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="gesprekjes" role="tabpanel"
                                     aria-labelledby="gesprekjes-tab">
                                    <div class="mt-2">
                                        <div class="row mx-1">
                                            <div class="col-sm border-bottom py-1 text-center text-sm-left">
                                                <div class="font-weight-bold">Naam</div>
                                            </div>
                                            <div class="col-sm col-lg-4 text-center d-flex flex-row mt-1 mt-sm-0 py-1 border-bottom">
                                                <div style="width: 25%">N/B</div>
                                                <div style="width: 25%">Aanwezig</div>
                                                <div style="width: 25%">Afgemeld</div>
                                                <div style="width: 25%">Afwezig</div>
                                            </div>
                                        </div>
                                        <aanwezigheid-table-row v-for="(m, index) in meetings[2]"
                                                                :medewerker="m" :id="`${m.id}-${index}`"
                                                                :key="`${m.id}-${index}`"></aanwezigheid-table-row>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="mv3" role="tabpanel" aria-labelledby="mv3-tab">
                                    <div class="mt-2">
                                        <div class="row mx-1">
                                            <div class="col-sm border-bottom py-1 text-center text-sm-left">
                                                <div class="font-weight-bold">Naam</div>
                                            </div>
                                            <div class="col-sm col-lg-4 text-center d-flex flex-row mt-1 mt-sm-0 py-1 border-bottom">
                                                <div style="width: 25%">N/B</div>
                                                <div style="width: 25%">Aanwezig</div>
                                                <div style="width: 25%">Afgemeld</div>
                                                <div style="width: 25%">Afwezig</div>
                                            </div>
                                        </div>
                                        <aanwezigheid-table-row v-for="(m, index) in meetings[3]"
                                                                :medewerker="m" :id="`${m.id}-${index}`"
                                                                :key="`${m.id}-${index}`"></aanwezigheid-table-row>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="mv4" role="tabpanel" aria-labelledby="mv4-tab">
                                    <div class="mt-2">
                                        <div class="row mx-1">
                                            <div class="col-sm border-bottom py-1 text-center text-sm-left">
                                                <div class="font-weight-bold">Naam</div>
                                            </div>
                                            <div class="col-sm col-lg-4 text-center d-flex flex-row mt-1 mt-sm-0 py-1 border-bottom">
                                                <div style="width: 25%">Aanwezig</div>
                                                <div style="width: 25%">Afgemeld</div>
                                                <div style="width: 25%">Afwezig</div>
                                            </div>
                                        </div>
                                        <aanwezigheid-table-row v-for="(m, index) in meetings[4]"
                                                                :medewerker="m" :id="`${m.id}-${index}`"
                                                                :key="`${m.id}-${index}`"></aanwezigheid-table-row>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="overview" role="tabpanel"
                                     aria-labelledby="overview-tab">
                                    <div>
                                        <aanwezigheid-overview :medewerkers="data"
                                                               :views="views"></aanwezigheid-overview>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </aanwezigheid-page>
@endsection