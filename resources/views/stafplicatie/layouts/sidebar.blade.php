<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand d-lg-down-none">
        <img class="c-sidebar-brand-full" src="{{ asset('/assets/stafplicatie/img/logo_stafplicatie.png') }}"
             width="125"
             height="50" alt="Stafplicatie">
        <img class="c-sidebar-brand-minimized" src="{{ asset('/assets/website/img/hu_logo_default.png') }}" width="50"
             height="50" alt="HU">
    </div>
    <ul class="c-sidebar-nav ps">
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('dashboard') }}">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="/assets/stafplicatie/svg/free.svg#cil-speedometer"></use>
                </svg>
                Dashboard
            </a>
        </li>
        <li class="c-sidebar-nav-title">Kamp management</li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="/stafplicatie/actiepunt">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="/assets/stafplicatie/svg/free.svg#cil-pin"></use>
                </svg>
                Actiepunten
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="/stafplicatie/kampjaar">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="/assets/stafplicatie/svg/free.svg#cil-calendar"></use>
                </svg>
                Kampjaren
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="/stafplicatie/inschrijving">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="/assets/stafplicatie/svg/free.svg#cil-people"></use>
                </svg>
                Inschrijvingen
            </a>
        </li>
        <li class="c-sidebar-nav-title">Medewerkers</li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="/stafplicatie/medewerker">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="/assets/stafplicatie/svg/free.svg#cil-address-book"></use>
                </svg>
                Medewerker Inschrijvingen
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="/stafplicatie/aanwezigheid">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="/assets/stafplicatie/svg/free.svg#cil-calendar-check"></use>
                </svg>
                Aanwezigheid
            </a>
        </li>
        <li class="c-sidebar-nav-title">Website</li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="/stafplicatie/nieuwsbericht">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="/assets/stafplicatie/svg/free.svg#cil-newspaper"></use>
                </svg>
                Nieuwsberichten
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="/stafplicatie/pagina">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="/assets/stafplicatie/svg/free.svg#cil-notes"></use>
                </svg>
                Pagina's
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="/stafplicatie/sponsor">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="/assets/stafplicatie/svg/free.svg#cil-heart"></use>
                </svg>
                Sponsoren
            </a>
        </li>
        <li class="c-sidebar-nav-title">Algemeen</li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="/stafplicatie/persoon">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="/assets/stafplicatie/svg/free.svg#cil-group"></use>
                </svg>
                Personen
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="/stafplicatie/user">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="/assets/stafplicatie/svg/free.svg#cil-user"></use>
                </svg>
                Gebruikers
            </a>
        </li>
    </ul>
    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent"
            data-class="c-sidebar-minimized"></button>
</div>