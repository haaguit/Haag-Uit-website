@extends('stafplicatie.layouts.app')

@section('content')
    <entities-table :entities="{{ json_encode($sponsoren) }}" inline-template>
        <div class="row">
            <div class="col-md-12 ">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex">
                                    <div class="p-2">
                                        <h5 class="card-title">Sponsoren</h5>
                                    </div>
                                    <div class="ml-auto p-2">
                                        <a role="button" class="btn btn-success" href="/stafplicatie/sponsor/create">
                                            <i class="fas fa-hands-helping"></i> Aanmaken
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="d-table-cell" scope="col">Naam</th>
                                        <th class="d-none d-lg-table-cell" scope="col">Link</th>
                                        <th class="d-none d-md-table-cell" scope="col">Actief</th>
                                        <th class="d-none d-lg-table-cell" scope="col">Logo</th>
                                        <th class="d-table-cell" scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr is="sponsor-table-row" v-for="(sponsor, index) in items" :key="sponsor.id"
                                                            :data="sponsor"
                                                            @deleted="remove(index)">
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </entities-table>
@endsection