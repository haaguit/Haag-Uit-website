@extends('stafplicatie.layouts.app')

@section('content')
    <inschrijving-page inline-template :original-inschrijving="{{$inschrijving}}">
        <div class="py-4">
            <div class="row">
                <div class="col-12">
                    <h1 v-text="fullname"></h1>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Persoonsgegevens</h5>
                            <div class="row">
                                <div class="col-6">
                                    Naam:
                                </div>
                                <div class="col-6">
                                    <span v-text="fullname"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Voorletters:
                                </div>
                                <div class="col-6">
                                    <span v-text="inschrijving.persoon.voorletters"></span>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-6">
                                    Roepnaam:
                                </div>
                                <div class="col-6">
                                    <span v-text="inschrijving.persoon.roepnaam"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Geboortedatum:
                                </div>
                                <div class="col-6">
                                    <div class="row">
                                        <div class="col-12">
                                            <span v-text="dateOfBirth.format('L')"></span>
                                            (<span v-text="ageNow"></span> jaar)
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 text-muted">
                                            <small>Op HU: <span v-text="ageOnHu"></span> jaar
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Geslacht:
                                </div>
                                <div class="col-6">
                                    {{ ucfirst($inschrijving->persoon->geslacht) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Contact informatie</h5>
                            <div class="row mb-3">
                                <div class="col-6">
                                    Noodnummer:
                                </div>
                                <div class="col-6 text-danger">
                                    {{$inschrijving->telefoon_nood}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Email:
                                </div>
                                <div class="col-6">
                                    {{ $inschrijving->persoon->email }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Telefoon:
                                </div>
                                <div class="col-6">
                                    {{ $inschrijving->persoon->mobiel }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Straat:
                                </div>
                                <div class="col-6">
                                    {{ $inschrijving->persoon->straat }} {{ $inschrijving->persoon->huisnummer }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Postcode en Plaats:
                                </div>
                                <div class="col-6">
                                    {{ $inschrijving->persoon->postcode }} {{ $inschrijving->persoon->woonplaats }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Overig</h5>
                            <div class="row">
                                <div class="col-6">
                                    Presentie:
                                </div>
                                <div class="col-6">
                                    <present-status-badge
                                            :presentie="inschrijving.presentie"></present-status-badge>
                                    <present-status-update :presentie="inschrijving.presentie" :id="inschrijving.id"
                                                           @change="updatePresentStatus">
                                    </present-status-update>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Ingeschreven op:
                                </div>
                                <div class="col-6">
                                    {{ $inschrijving->created_at }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Kledingmaat:
                                </div>
                                <div class="col-6">
                                    <kledingmaat-edit :inschrijving-id="inschrijving.id" :kledingmaat="inschrijving.kledingmaat"></kledingmaat-edit>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <latest-payment-details :id="inschrijving.id" :latest-betaling="inschrijving.latest_betaling"
                                            :annuleringsverzekering="inschrijving.annuleringsverzekering"></latest-payment-details>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Opmerking</h5>
                            {{ $inschrijving->opmerking }}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </inschrijving-page>
@endsection