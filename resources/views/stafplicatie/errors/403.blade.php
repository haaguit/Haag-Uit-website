@extends('stafplicatie.errors.base')

@section('error')
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="clearfix">
                <h1 class="float-left display-3 mr-4">403</h1>
                <h4 class="pt-3">Staf, we have a problem!</h4>
                <p class="text-muted">Onbevoegden!</p>
            </div>
            <a href="{{ url()->previous() }}" class="btn btn-primary btn-lg btn-block" role="button"
               aria-disabled="true">Terug naar waar je vandaan kwam</a>
            <a class="btn btn-primary btn-lg btn-block" role="button" href="{{ route('logout') }}"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </div>
@endsection