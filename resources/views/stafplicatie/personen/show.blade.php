@extends('stafplicatie.layouts.app')

@section('content')
    <persoon-page inline-template :entity="{{$persoon}}">
        <div class="py-4">
            <div class="row">
                <div class="col-12">
                    <h1 v-text="fullName"></h1>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Persoonsgegevens</h5>
                            <div class="row">
                                <div class="col-6">
                                    Naam:
                                </div>
                                <div class="col-6">
                                    <span v-text="fullName"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Voorletters:
                                </div>
                                <div class="col-6">
                                    <span v-text="persoon.voorletters"></span>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-6">
                                    Roepnaam:
                                </div>
                                <div class="col-6">
                                    <span v-text="persoon.roepnaam"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Geboortedatum:
                                </div>
                                <div class="col-6">
                                    <div class="row">
                                        <div class="col-12">
                                            <span v-text="dateOfBirth.format('L')"></span>
                                            (<span v-text="ageNow"></span> jaar)
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Geslacht:
                                </div>
                                <div class="col-6">
                                    {{ ucfirst($persoon->geslacht) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Contact informatie</h5>
                            <div class="row mb-3">
                                <div class="col-6">
                                    Noodnummer:
                                </div>
                                <div class="col-6 text-danger">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Email:
                                </div>
                                <div class="col-6">
                                    {{ $persoon->email }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Telefoon:
                                </div>
                                <div class="col-6">
                                    {{ $persoon->mobiel }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Straat:
                                </div>
                                <div class="col-6">
                                    {{ $persoon->straat }} {{ $persoon->huisnummer }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    Postcode en Plaats:
                                </div>
                                <div class="col-6">
                                    {{ $persoon->postcode }} {{ $persoon->woonplaats }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Overig</h5>
                            <div class="row">
                                <div class="col-6">
                                    Aangemaakt op:
                                </div>
                                <div class="col-6">
                                    {{ $persoon->created_at }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Historie</h5>
                            <div class="row">
                                <div class="col-6">
                                    Aangemaakt op:
                                </div>
                                <div class="col-6">
                                    {{ $persoon->created_at }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="card">
                                <div class="card-body text-center">
                                    @if($persoon->kampInschrijving)
                                        <h2>Feut in:</h2>
                                        <h3>{{ $persoon->kampInschrijving->jaar }}</h3>
                                    @else
                                        <h2>Geen feut</h2>
                                    @endif
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body text-center">
                                    <h2>Inschrijvingen:</h2>
                                    <h3><span v-text="countMedewerkerInschrijvingen"></span></h3>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body text-center">
                                    <h2>Selecties:</h2>
                                    <h3><span v-text="countSelecties"></span></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-8">
                            <div class="card h-100">
                                <div class="card-body">
                                    <registrations-list
                                            :registrations="persoon.medewerker_inschrijvingen"></registrations-list>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </persoon-page>
@endsection