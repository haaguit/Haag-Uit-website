@extends('stafplicatie.layouts.app')

@section('content')
    <div class="row">
        <div class="col-xl-9">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Pagina wijzigen</h5>
                    <pagina-editor :page-id="'{{ $pagina->id }}'"></pagina-editor>
                </div>
            </div>
        </div>
        <div class="col-xl-3">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Variabelen</h5>
                    <div class="row">
                        <div class="col-12">
                            <table class=" table table-borderless">
                                <tbody>
                                <tr>
                                    <td><b>$JAAR$</b></td>
                                    <td>Huidige kampjaar jaar</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection