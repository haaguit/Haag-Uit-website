@extends('stafplicatie.layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Gebruiker aanmaken</h5>
                    <form action="/stafplicatie/user" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">Naam</label>
                            <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" id="name" name="name" value="{{ old('name') }}"
                                   placeholder="Henkie Kraggelwenk">
                            @if ($errors->has('name'))
                                <div class="invalid-feedback">
                                   {{ $errors->first('name') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control  {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}"
                                   placeholder="tasje@haaguit.com">
                            @if ($errors->has('email'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password">Wachtwoord</label>
                            <input type="password" class="form-control  {{ $errors->has('password') ? 'is-invalid' : '' }}" id="password" name="password"
                                   placeholder="IetsSuperVeiligs">
                            @if ($errors->has('password'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Wachtwoord bevestiging</label>
                            <input type="password" class="form-control  {{ $errors->has('password') ? 'is-invalid' : '' }}" id="password_confirmation"
                                   name="password_confirmation" placeholder="IetsSuperVeiligs">
                            @if ($errors->has('password'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="role">Rol</label>
                            <select class="form-control  {{ $errors->has('role') ? 'is-invalid' : '' }}" id="role" name="role">
                                @foreach($roles as $role)
                                    <option value="{{$role->name}}" {{  old('role') == $role->name ? 'selected' : '' }}>{{$role->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('role'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('role') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Aanmaken</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection