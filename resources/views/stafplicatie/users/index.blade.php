@extends('stafplicatie.layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 ">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="d-flex">
                                <div class="p-2">
                                    <h5 class="card-title">Gebruikers</h5>
                                </div>
                                <div class="ml-auto p-2">
                                    <a role="button" class="btn btn-success" href="/stafplicatie/user/create"><i class="fas fa-user"></i> Aanmaken</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="d-none d-lg-table-cell" scope="col">#</th>
                                    <th scope="col">Naam</th>
                                    <th class="d-none d-md-table-cell" scope="col">Email</th>
                                    <th class="d-none d-lg-table-cell" scope="col">Rol</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <th class="d-none d-lg-table-cell" scope="row">{{ $user->id }}</th>
                                        <td>{{ $user->name }}</td>
                                        <td class="d-none d-md-table-cell">{{ $user->email }}</td>
                                        <td class="d-none d-lg-table-cell">{{ $user->getRoleNames()->first() }}</td>
                                        <td>
                                            @if($user->id !== 1)
                                                <a role="button" class="btn btn-primary"
                                                   href="/stafplicatie/user/{{ $user->id }}/edit"><span
                                                            class="fas fa-edit"></span>
                                                </a>
                                                <a role="button" class="btn btn-danger"
                                                   href="/stafplicatie/user/{{ $user->id }}"
                                                   onclick="event.preventDefault(); document.getElementById('delete-button-{{ $user->id }}').submit();">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                                <form id="delete-button-{{ $user->id }}"
                                                      action="/stafplicatie/user/{{ $user->id }}" method="POST"
                                                      style="display: none;">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection