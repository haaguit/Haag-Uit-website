@extends('website.layouts.website')

@section('content')
    <div class="box text-box">
        <h2 class="box-title">Neem contact op met Haag Uit </h2>
        <div class="text">
            Wij proberen zo veel mogelijk informatie over Haag Uit te geven via de website.
            Mocht je nog vragen of opmerkingen hebben, is e-mail het handigste communicatiemiddel om contact met ons op
            te nemen. Hiervoor kan je gebruik maken van het contactformulier of onderstaand e-mailadres.
        </div>
        <br/>

        <div class="text">
            <h4>Bezoekadres*</h4> <br/>
            De Haagse Hogeschool, kamer SL-6.41 <br/>
            Johanna Westerdijkplein 75 <br/>
            2521 EN Den Haag<br/>
            <span class="subtle italic">*Graag van te voren een afspraak maken </span>
        </div>
        <br/>

        <div class="text">
            <h4>Postadres</h4> <br/>
            Stichting Haag Uit<br/>
            2501 EH Den Haag<br/>
        </div>
        <br/>

        <div class="text">
            Email: <a href="mailto:info@haaguit.com">info@haaguit.com</a><br/>
            Rekeningnummer: NL79 INGB 0006 0656 57 <br/>
            T.n.v. Stichting Haag Uit te 's-Gravenhage <br/>
            KvK-nummer: 27319424 <br/>
        </div>
        <br/>

        <div>
            <h4> Contactgegevens organisatie tijdens Haag Uit {{ $kampjaar->jaar }}</h4> <br/>
            <span class="italic">Telefoon: (088) 000 6058 | Mail: staf@haaguit.com</span>
        </div>
    </div>

    <div class="box text-box">
        <h3 class="box-sub-title">Contactformulier </h3>
        <form method="post" action="/contact">
            {{ csrf_field() }}
            <p>
                <label for="naam">Naam: </label>
                <input type="text" id="naam" name="naam" value="{{ old('naam') }}" required>
                @if($errors->has('naam'))
                    <span class="error">{{ $errors->first('naam') }}</span>
                @endif
            </p>
            <p>
                <label for="email">E-mailadres: </label>
                <input type="email" id="email" name="email" value="{{ old('email') }}" required>
                @if($errors->has('email'))
                    <span class="error">{{ $errors->first('email') }}</span>
                @endif
            </p>
            <p>
                <label for="tekst">Bericht: </label>
                <textarea name="tekst" id="tekst" cols="40" rows="10" style="resize: none" maxlength="600"
                          required>{{ old('tekst') }}</textarea>
                @if($errors->has('tekst'))
                    <span class="error">{{ $errors->first('tekst') }}</span>
                @endif
            </p>
            <div id="contact_id"></div>
            @if($errors->has('g-recaptcha-response'))
                <span class="error">{{ $errors->first('g-recaptcha-response') }}</span>
            @endif
            <p>
                <label for="verzenden"> </label>
                <input type="submit" id="verzenden" name="verzenden" value="Stuur bericht"/>
            </p>
        </form>
    </div>
    {!! GoogleReCaptchaV3::render(['contact_id' => 'contact']) !!}
@endsection
