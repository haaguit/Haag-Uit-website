@extends('website.layouts.website')

@section('content')
    <div class="box text-box">
        <h2 class="box-title">Bericht verstuurd </h2>
        <p>Beste {{ $name }}, je bericht is verstuurd naar de Staf van Haag Uit. We nemen zo snel mogelijk contact met je op!</p>
    </div>
@endsection
