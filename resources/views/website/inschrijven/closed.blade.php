@extends('website.layouts.website')

@section('content')
    <div class="box text-box">
        <h2 class="box-title">Inschrijvingen gesloten</h2>
        <p>
            Super tof dat je je wil inschrijven voor Haag Uit! Helaas is nschrijven voor Haag Uit op dit moment (nog) niet mogelijk.<br/>
            Mocht je vragen hebben, stuur dan een email naar de <a href="mailto:staf@haaguit.com">staf</a> of stuur een berichtje via de <a href="/contact">contact pagina</a>.<br/>
            <br/>
            Hopelijk tot ziens op Haag Uit!
        </p>
    </div>
@endsection