@extends('website.layouts.website')

@section('content')
    <div class="box text-box">
        <h2 class="box-title">Inschrijving gelukt!</h2>
        <p>
            Wat tof dat je mee gaat op Haag Uit. Jij gaat de beste start van je studie tegemoet!
            We zullen je verder op de hoogte houden via het door jou opgegeven e-mailadres, <b>{{ $email }}</b>.
        </p>
        @if($paymentUrl)
            <p>
                Je hebt er voor gekozen om te betalen middels iDeal.
                <a href="{{ $paymentUrl }}">Klik hier om te betalen!</a>
            </p>
        @else
            <p>
                Je hebt er voor gekozen om te betalen middels incasso. Deze incasso zal ongeveer <b>2 weken</b> voor Haag Uit plaats vinden, rond <b>{{ \Carbon\Carbon::parse($kampjaar->start)->subWeek(2)->format('d-m-Y') }}</b>.
            </p>
        @endif
        <p>
            Heb je nog vragen of andere dingen? Stuur dan een mailtje naar <a href="mailto:staf@haaguit.com">staf@haaguit.com</a>!
        </p>
        <p>
            We zien je op Haag Uit!
        </p>
    </div>
@endsection