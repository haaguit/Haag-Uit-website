@extends('website.layouts.website')

@section('content')
    <div class="box text-box">
        <h2 class="box-title">Nieuwsberichten</h2>
        <p>Hier vind je alle nieuwsberichten van Haag Uit.</p>
    </div>
    @foreach($newsMessages as $message)
        <div class="box text-box">
            <div class="level">
                <div class="flex">
                    <div style="color: #2384ae; font-size: 16px;">{{ $message->titel }}</div>
                </div>
                <div>
                    <div style="color: #afafaf;">geplaatst op {{ $message->created_at->format('Y-m-d H:i') }}</div>
                </div>
            </div>
            <div class="separator"></div>
            <div>
                {{ $message->previewText() }}
            </div>
            <br/>
            <div class="box-footer">
                <a href="{{ url('nieuws', [$message->id]) }}">Lees meer ></a>
            </div>
        </div>
    @endforeach

    <div class="box pagination-box">
        {{ $newsMessages->links() }}
    </div>
@endsection
