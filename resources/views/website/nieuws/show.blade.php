@extends('website.layouts.website')

@section('content')
    <div class="box text-box">
        <h2 class="box-title">{{ $nieuwsbericht->titel }}</h2>
        <div class="subtle italic">
            Datum: {{ $nieuwsbericht->created_at->format('Y-m-d H:i') }}
        </div>
        <p>
            {{ \Illuminate\Mail\Markdown::parse($nieuwsbericht->tekst) }}
        </p>
    </div>
@endsection
