<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="nl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="description"
          content="Haag Uit is de jaarlijkse introductieweek voor nieuwe studenten aan de Faculteit voor IT &amp;
          Design aan de Haagse Hogeschool, georganiseerd door en voor studenten."/>
    <meta http-equiv="keywords"
          content="den, haag, uit, haaguit, introductieweek, haagse hogeschool, hhs, ict, informatica, the hague university"/>
    <meta name="robots" content="index, follow"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ mix('assets/website/css/website.css') }}" rel="stylesheet">

    <title>Introductieweek Haag Uit</title>

</head>

<body>
<div id="website">
    <div class="container page-top full">
        @include('website.layouts.nav')
        <header-animations-component></header-animations-component>
    </div>

    <div class="container page-wrap xx-wide">

        <div class="container page-content">
            @yield('content')
        </div>

        <div class="container aside-right">

            <quote-box-component :data="{{ json_encode($quote) }}"></quote-box-component>

            <div class="separator"></div>
            <sponsor-box-component :jaar="{{ $kampjaar->jaar }}"></sponsor-box-component>

            <div class="separator"></div>

            <div class="box login-btn x-narrow">
                <span class="login-title">Medewerkers login </span>

                <form id="hu_loginformulier" name="hu_loginformulier" method="post" action="/inloggen">
                    <p>
                        <input type="text" id="loginEmail" name="email" placeholder="E-mailadres" value="" required/>
                        <input type="password" id="loginWachtwoord" name="wachtwoord" placeholder="Wachtwoord" value=""
                               required/>
                        <input type="submit" id="inloggen" name="inloggen" value="Inloggen"/>
                    </p>
                </form>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    @include('website.layouts.footer')
</div>

<!-- Scripts -->
<script src="{{ mix('/assets/stafplicatie/js/manifest.js') }}" defer></script>
<script src="{{ mix('/assets/stafplicatie/js/vendor.js') }}" defer></script>
<script src="{{ mix('assets/website/js/website.js') }}" defer></script>
{!!  GoogleReCaptchaV3::init() !!}
</body>
</html>
