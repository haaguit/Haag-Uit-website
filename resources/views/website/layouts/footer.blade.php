<div class="container page-footer xx-wide">
    <div class="separator"></div>
    Copyright Haag Uit {{ \Carbon\Carbon::now()->year }}
</div>
