<div class="box navigation xx-wide">
    <ul class="nav-list fleft">
        <li><a class="nav-link default" href="{{ url('/nieuws') }}">Nieuws</a></li>
        <li class="has-child">
            <a class="nav-link default" href="{{ url('/pagina/geschiedenis') }}">Over Haag Uit</a>
            <ul class="nav-list-drop">
                <li>
                    <a class="nav-link default" href="{{ url('/pagina/geschiedenis') }}">De geschiedenis</a>
                </li>
                <li>
                    <a class="nav-link default" href="{{ url('/pagina/organisatie') }}">De organisatie</a>
                </li>
                <li>
                    <a class="nav-link default" href="{{ url('/pagina/wat') }}">Wat, waar &amp; wanneer?</a>
                </li>
                <li>
                    <a class="nav-link default" href="{{ url('/pagina/programma') }}">Wat gaan we doen?</a>
                </li>
                <li>
                    <a class="nav-link default" href="{{ url('/pagina/waarom') }}">Waarom mee gaan?</a>
                </li>
                <li>
                    <a class="nav-link default" href="{{ url('/pagina/paklijst') }}">De paklijst</a>
                </li>
            </ul>
        </li>
        <li><a class="nav-link default" href="{{ url('/contact') }}">Contact</a>
    </ul>

    <div class="box logo">
        <a href="{{ url('/') }}"><img src="{{ $kampjaar->logo }}" alt="Haag Uit" class="logo"/></a>
    </div>

    <ul class="nav-list fright">
        <li>
            <a class="nav-link default right" href="{{ url('/pagina/sponsoren') }}">Sponsoren</a>
        </li>
        <li>
            <a class="nav-link default right" href="{{ url('/inschrijven') }}">Inschrijven</a>
        </li>
    </ul>

    <inschrijf-wiggle-component url="{{ url('/inschrijven') }}"></inschrijf-wiggle-component>
    <div class="clearfix"></div>

</div>
