@extends('website.layouts.website')
@section('content')
    <div class="box text-box">
        <h2 class="box-title">
            Pagina niet gevonden.
        </h2>
        <p>
            Deze pagina bestaat helaas niet (meer).
            <br/>
            <a href="{{ url('/') }}">Terug</a>
        </p>
    </div>
@endsection