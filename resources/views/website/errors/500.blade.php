@extends('website.layouts.website')
@section('content')
    <div class="box text-box">
        <h2 class="box-title">
            Oh oh...
        </h2>
        <p>
            Er is iets goed mis gegaan. <br/>
            Als dit probleem zich voor blijft doen, neem dan contact op met de Staf via
            <a href="mailto:staf@haaguit.com">staf@haaguit.com</a>
            <br/>
            <br/>
            @isset($message)
                <div>
                    <b>Toelichting: </b> {{ $message }}
                </div>
            @endisset
            <a href="{{ URL::previous() }}">Terug naar waar je vandaan komt</a>
        </p>
        @if(app()->bound('sentry') && !empty(Sentry::getLastEventID()))
            <error-reporting-component event-id="{{ Sentry::getLastEventID() }}"></error-reporting-component>
        @endif
    </div>
@endsection
